package ke.mwangaza.buy.mwangaza;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.widget.Button;
import android.widget.TextView;

import butterknife.BindView;
import butterknife.ButterKnife;
import ke.mwangaza.buy.mwangaza.ui.main.ui.DashboardActivity;

public class PinAirtimeSuccess extends AppCompatActivity {

    @BindView(R.id.details)
    TextView details;
    @BindView(R.id.amount)
    TextView amount;
    @BindView(R.id.serial)
    TextView serial;
    @BindView(R.id.toolbar)
    Toolbar toolbar;
    @BindView(R.id.done)
    Button done;
    @BindView(R.id.share_airtime)
    Button share;

    private String pin, value, serial_number;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_pin_airtime_success);
        ButterKnife.bind(this);

        toolbar.setTitle(R.string.buy_success);
        setSupportActionBar(toolbar);
        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setDisplayShowHomeEnabled(true);
        }

        Intent intent = getIntent();
        Bundle bundle = intent.getExtras();

        assert bundle != null;
        pin = bundle.getString("pin");
        value = bundle.getString("amount");
        serial_number = bundle.getString("serial_number");

        details.setText(String.format("Pin Number : %s", pin));
        amount.setText(String.format("Amount Paid : %s", value));
        serial.setText(String.format("Reference : %s", serial_number));

        share.setOnClickListener(v -> {
            Intent myIntent = new Intent(Intent.ACTION_SEND);
            myIntent.setType("text/plain");
            String shareBody = "MWANGAZA AIRTIME RECEIPT" + "\n" + "========================" + "\n" + "Pin #: " + pin + "\n" + "Amount: " + value + "\n" + "Transaction: " + serial_number + "\n"+ "\n"+"Download Mwangaza App from Playstore";
            myIntent.putExtra(Intent.EXTRA_TEXT, shareBody);
            startActivity(Intent.createChooser(myIntent, "Share via..."));
        });

        done.setOnClickListener(v -> {
            Intent dashIntent = new Intent(getApplicationContext(), DashboardActivity.class);
            startActivity(dashIntent);
        });

    }

    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }
}
