package ke.mwangaza.buy.mwangaza.data.retrofit.power;

public class PostPaidMeters {

    private String account_number;

    public PostPaidMeters(String account_number) {
        this.account_number = account_number;
    }

    public String getAccount_number() {
        return account_number;
    }

    public void setAccount_number(String account_number) {
        this.account_number = account_number;
    }
}


