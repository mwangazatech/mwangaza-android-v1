package ke.mwangaza.buy.mwangaza.data.retrofit.water;

public class WaterRequest {

    private String user_id;
    private String account_number;

    public WaterRequest(String user_id, String account_number) {
        this.user_id = user_id;
        this.account_number = account_number;
    }
}
