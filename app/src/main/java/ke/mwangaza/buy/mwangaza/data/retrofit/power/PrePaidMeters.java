package ke.mwangaza.buy.mwangaza.data.retrofit.power;

public class PrePaidMeters {

    private String meter_number;

    public PrePaidMeters(String meter_number) {
        this.meter_number = meter_number;
    }

    public String getAccount_number() {
        return meter_number;
    }

    public void setAccount_number(String meter_number) {
        this.meter_number = meter_number;
    }
}


