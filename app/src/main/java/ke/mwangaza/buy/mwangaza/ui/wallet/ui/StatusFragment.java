package ke.mwangaza.buy.mwangaza.ui.wallet.ui;

import android.annotation.SuppressLint;
import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.Toast;

import java.util.List;

import ke.mwangaza.buy.mwangaza.Mwangaza;
import ke.mwangaza.buy.mwangaza.R;
import ke.mwangaza.buy.mwangaza.data.retrofit.power.PrePaidVendResponse;
import ke.mwangaza.buy.mwangaza.network.APIClient;
import ke.mwangaza.buy.mwangaza.network.WalletInterface;
import ke.mwangaza.buy.mwangaza.ui.main.ui.DashboardActivity;
import ke.mwangaza.buy.mwangaza.ui.wallet.adapter.PrepaidTokensAdapter;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

@SuppressLint("ValidFragment")
public class StatusFragment extends Fragment {

    View view;
    Mwangaza app;
    WalletInterface walletInterface;
    ProgressDialog progressDialog;

    List<PrePaidVendResponse> prePaidVendResponseList;
    RecyclerView recyclerView;
    ImageView brokenImageLink;

    public StatusFragment(Mwangaza app) {
        this.app = app;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_status, container, false);

        walletInterface = APIClient.getClient(app.settings.getBearerToken()).create(WalletInterface.class);

        recyclerView = view.findViewById(R.id.recyclerView);
        brokenImageLink = view.findViewById(R.id.brokenImageLink);

        loadRecentTokens();

        return view;
    }

    private void loadRecentTokens() {

        progressDialog = new ProgressDialog(getActivity());
        progressDialog.setIndeterminate(true);
        progressDialog.setMessage("Fetching Recent Tokens...");
        progressDialog.setCancelable(false);
        progressDialog.show();

        Call<List<PrePaidVendResponse>> call = walletInterface.getTokenHistory();
        call.enqueue(new Callback<List<PrePaidVendResponse>>() {
            @Override
            public void onResponse(@NonNull Call<List<PrePaidVendResponse>> call, @NonNull Response<List<PrePaidVendResponse>> response) {
                progressDialog.dismiss();
                if (response.isSuccessful()) {
                    prePaidVendResponseList = response.body();
                    assert prePaidVendResponseList != null;

                    showTransactions(prePaidVendResponseList);
                }
            }

            @Override
            public void onFailure(@NonNull Call<List<PrePaidVendResponse>> call, @NonNull Throwable t) {
                progressDialog.dismiss();
                Toast.makeText(getActivity(), "Failed to load Token History...", Toast.LENGTH_SHORT).show();
                Intent intent = new Intent(getActivity(), DashboardActivity.class);
                startActivity(intent);
            }
        });

    }

    private void showTransactions(List<PrePaidVendResponse> prePaidVendResponses) {
        if (prePaidVendResponses == null || prePaidVendResponses.isEmpty()) {
            brokenImageLink.setVisibility(View.VISIBLE);
            recyclerView.setVisibility(View.INVISIBLE);
        } else {
            PrepaidTokensAdapter prepaidTokensAdapter = new PrepaidTokensAdapter(getActivity(), prePaidVendResponses);

            LinearLayoutManager mLayoutManager =
                    new LinearLayoutManager(getActivity(), LinearLayoutManager.VERTICAL, false);
            recyclerView.setLayoutManager(mLayoutManager);
            recyclerView.setAdapter(prepaidTokensAdapter);
        }
    }

}
