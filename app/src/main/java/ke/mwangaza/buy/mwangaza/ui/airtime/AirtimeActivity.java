package ke.mwangaza.buy.mwangaza.ui.airtime;

import android.Manifest;
import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.provider.ContactsContract;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Toast;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;

import butterknife.BindView;
import butterknife.ButterKnife;
import ke.mwangaza.buy.mwangaza.Mwangaza;
import ke.mwangaza.buy.mwangaza.R;
import ke.mwangaza.buy.mwangaza.data.retrofit.airtime.AirtimeRequest;
import ke.mwangaza.buy.mwangaza.data.retrofit.airtime.AirtimeResponse;
import ke.mwangaza.buy.mwangaza.network.APIClient;
import ke.mwangaza.buy.mwangaza.network.AirtimeInterface;
import ke.mwangaza.buy.mwangaza.ui.main.ui.DashboardActivity;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class AirtimeActivity extends AppCompatActivity {

    @BindView(R.id.toolbar)
    Toolbar toolbar;
    @BindView(R.id.buyAirtime)
    Button buyAirtime;
    @BindView(R.id.airtimePhoneNumber)
    EditText airtimePhoneNumber;
    @BindView(R.id.airtimeAmount)
    EditText airtimeAmount;
    @BindView(R.id.button_contacts)
    ImageView phoneContacts;
    @BindView(R.id.mynumber)
    RadioButton mynumber;
    @BindView(R.id.othernumber)
    RadioButton othernumber;
    @BindView(R.id.Cancel)
    Button Cancel;
    @BindView(R.id.radio_guide)
    RadioGroup radio_guide;

    AirtimeInterface airtimeInterface;
    ProgressDialog progressDialog;

    Mwangaza app;
    String operator, amount, phone_number, fetched_phone;
    private final static int PICK_CONTACT = 1001;
    private static final int MY_PERMISSIONS_REQUEST_READ_CONTACTS = 99;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_airtime);
        ButterKnife.bind(this);

        toolbar.setTitle(R.string.purchase_airtime);
        setSupportActionBar(toolbar);
        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setDisplayShowHomeEnabled(true);
        }

        app = (Mwangaza) getApplicationContext();
        airtimeInterface = APIClient.getClient(app.settings.getBearerToken()).create(AirtimeInterface.class);

        Intent intent = getIntent();
        Bundle bundle = intent.getExtras();

        if (bundle != null) {
            fetched_phone = bundle.getString("phone_number");
            airtimePhoneNumber.setText(fetched_phone);
        }

        mynumber.setOnCheckedChangeListener(new Radio_check());
        othernumber.setOnCheckedChangeListener(new Radio_check());

        Cancel.setOnClickListener(v -> {
            Intent cancel = new Intent(getApplicationContext(), DashboardActivity.class);
            startActivity(cancel);
        });

        buyAirtime.setOnClickListener(v -> verifyAirtimeDetails());
        phoneContacts.setOnClickListener(v -> pickContact());
        checkContactPermissions();
    }

    private void checkContactPermissions() {
        if (ContextCompat.checkSelfPermission(this, Manifest.permission.READ_CONTACTS) != PackageManager.PERMISSION_GRANTED) {
            Toast.makeText(getApplicationContext(),
                    "We need to read your contacts to find the phone number.",
                    Toast.LENGTH_SHORT).show();
            ActivityCompat.requestPermissions(AirtimeActivity.this,
                    new String[]{android.Manifest.permission.READ_CONTACTS}, MY_PERMISSIONS_REQUEST_READ_CONTACTS);
        } else {
            Toast.makeText(getApplicationContext(),
                    "Contacts permission granted.",
                    Toast.LENGTH_SHORT).show();
        }
    }

    private void pickContact() {
        Intent intent = new Intent(Intent.ACTION_PICK, ContactsContract.Contacts.CONTENT_URI);
        startActivityForResult(intent, PICK_CONTACT);
    }

    @Override
    public void onActivityResult(int reqCode, int resultCode, Intent data) {
        super.onActivityResult(reqCode, resultCode, data);

        switch (reqCode) {
            case (PICK_CONTACT):
                if (resultCode == Activity.RESULT_OK) {
                    Uri contactData = data.getData();
                    Cursor c = getContentResolver().query(contactData, null, null, null, null);
                    if (c.moveToFirst()) {
                        String contactId = c.getString(c.getColumnIndex(ContactsContract.Contacts._ID));
                        String hasNumber = c.getString(c.getColumnIndex(ContactsContract.Contacts.HAS_PHONE_NUMBER));
                        String num = "";
                        if (Integer.valueOf(hasNumber) == 1) {
                            Cursor numbers = getContentResolver().query(ContactsContract.CommonDataKinds.Phone.CONTENT_URI, null, ContactsContract.CommonDataKinds.Phone.CONTACT_ID + " = " + contactId, null, null);
                            while (numbers.moveToNext()) {
                                num = numbers.getString(numbers.getColumnIndex(ContactsContract.CommonDataKinds.Phone.NUMBER));
                                String contact_name =numbers.getString(numbers.getColumnIndex(ContactsContract.CommonDataKinds.Phone.DISPLAY_NAME));
                                Toast.makeText(AirtimeActivity.this, "You’ve selected: " + contact_name, Toast.LENGTH_LONG).show();
                                //phone_number = num;
                                String numNew = num.replaceAll("\\s+", "");
                                airtimePhoneNumber.setText(numNew);
                            }
                        }
                    }
                    break;
                }
        }
    }


    class Radio_check implements CompoundButton.OnCheckedChangeListener {
        @Override
        public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
            if (mynumber.isChecked()) {
                phone_number = app.settings.getPhoneNumber();
                airtimePhoneNumber.setText(phone_number);
            } else if (othernumber.isChecked()) {
                airtimePhoneNumber.setText(null);
            }
        }
    }

    private void verifyAirtimeDetails() {

        phone_number = airtimePhoneNumber.getText().toString().trim();
        amount = airtimeAmount.getText().toString().trim();
        operator = "safaricom";

        if (TextUtils.isEmpty(phone_number)) {
            airtimePhoneNumber.setError("Kindly Enter a Phone Number");
            return;
        }

        if (TextUtils.isEmpty(amount)) {
            airtimeAmount.setError("Kindly Enter Airtime Amount");
            return;
        }

        purchaseAirtime();
    }

    private void purchaseAirtime() {

        progressDialog = new ProgressDialog(this);
        progressDialog.setIndeterminate(true);
        progressDialog.setMessage("Purchasing Airtime...");
        progressDialog.setCancelable(false);
        progressDialog.show();

        AirtimeRequest airtimeRequest = new AirtimeRequest(phone_number, Integer.parseInt(amount), operator);

        Call<AirtimeResponse> call = airtimeInterface.buyAirtimePinless(airtimeRequest);
        call.enqueue(new Callback<AirtimeResponse>() {
            @Override
            public void onResponse(@NonNull Call<AirtimeResponse> call, @NonNull Response<AirtimeResponse> response) {
                if (response.isSuccessful()) {
                    progressDialog.dismiss();

                    AirtimeResponse airtimeResponse = response.body();
                    assert airtimeResponse != null;

                    Intent intent = new Intent(getApplicationContext(), AirtimeSuccessActivity.class);
                    Bundle bundle = new Bundle();
                    bundle.putString("receipt_number", airtimeResponse.getReceipt_number());
                    bundle.putString("amount", String.valueOf(airtimeResponse.getAmount()));
                    bundle.putString("phone_number", String.valueOf(airtimeResponse.getPhone_number()));
                    intent.putExtras(bundle);
                    startActivity(intent);

                } else {
                    progressDialog.dismiss();
                    assert response.body() != null;
                    JSONObject jsonObject = null;
                    try {
                        assert response.errorBody() != null;
                        jsonObject = new JSONObject(response.errorBody().string());

                        Intent intent = new Intent(getApplicationContext(), AirtimeFailureActivity.class);
                        Bundle bundle = new Bundle();
                        bundle.putString("message", jsonObject.getString("message"));
                        intent.putExtras(bundle);
                        startActivity(intent);

                    } catch (JSONException e) {
                        e.printStackTrace();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
            }

            @Override
            public void onFailure(@NonNull Call<AirtimeResponse> call, @NonNull Throwable t) {
                progressDialog.dismiss();
                Intent intent = new Intent(getApplicationContext(), AirtimeFailureActivity.class);
                Bundle bundle = new Bundle();
                bundle.putString("message", "Please try your request again...");
                intent.putExtras(intent);
                startActivity(intent);
            }
        });
    }

    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }
}

