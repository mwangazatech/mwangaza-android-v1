package ke.mwangaza.buy.mwangaza.ui.wallet.ui;

import android.annotation.SuppressLint;
import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.Toast;

import java.util.List;

import ke.mwangaza.buy.mwangaza.Mwangaza;
import ke.mwangaza.buy.mwangaza.R;
import ke.mwangaza.buy.mwangaza.data.retrofit.wallet.WalletDeposits;
import ke.mwangaza.buy.mwangaza.network.APIClient;
import ke.mwangaza.buy.mwangaza.network.WalletInterface;
import ke.mwangaza.buy.mwangaza.ui.main.ui.DashboardActivity;
import ke.mwangaza.buy.mwangaza.ui.wallet.adapter.WalletDepositsAdapter;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

@SuppressLint("ValidFragment")
public class AllFragment extends Fragment {

    View view;
    Mwangaza app;
    WalletInterface walletInterface;
    ProgressDialog progressDialog;

    List<WalletDeposits> walletDeposits;
    RecyclerView recyclerView;
    ImageView brokenImageLink;

    public AllFragment(Mwangaza app) {
        this.app = app;
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_all, container, false);

        walletInterface = APIClient.getClient(app.settings.getBearerToken()).create(WalletInterface.class);

        recyclerView = view.findViewById(R.id.recyclerView);
        brokenImageLink = view.findViewById(R.id.brokenImageLink);

        loadWalletDeposits();

        return view;
    }

    private void loadWalletDeposits() {

        progressDialog = new ProgressDialog(getActivity());
        progressDialog.setIndeterminate(true);
        progressDialog.setMessage("Fetching Wallet Deposits...");
        progressDialog.setCancelable(false);
        progressDialog.show();

        Call<List<WalletDeposits>> call = walletInterface.getWalletDeposits();
        call.enqueue(new Callback<List<WalletDeposits>>() {
            @Override
            public void onResponse(@NonNull Call<List<WalletDeposits>> call, @NonNull Response<List<WalletDeposits>> response) {
                progressDialog.dismiss();
                if (response.isSuccessful()) {
                    walletDeposits = response.body();
                    assert walletDeposits != null;

                    showTransactions(walletDeposits);
                }
            }

            @Override
            public void onFailure(@NonNull Call<List<WalletDeposits>> call, @NonNull Throwable t) {
                progressDialog.dismiss();
                Toast.makeText(getActivity(), "Failed to load wallet transactions...", Toast.LENGTH_SHORT).show();
                Intent intent = new Intent(getActivity(), DashboardActivity.class);
                startActivity(intent);
            }
        });

    }

    private void showTransactions(List<WalletDeposits> walletDeposits) {
        if (walletDeposits == null || walletDeposits.isEmpty()) {
            brokenImageLink.setVisibility(View.VISIBLE);
            recyclerView.setVisibility(View.INVISIBLE);
        } else {
            WalletDepositsAdapter walletDepositsAdapter = new WalletDepositsAdapter(getActivity(), walletDeposits);

            LinearLayoutManager mLayoutManager =
                    new LinearLayoutManager(getActivity(), LinearLayoutManager.VERTICAL, false);
            recyclerView.setLayoutManager(mLayoutManager);
            recyclerView.setAdapter(walletDepositsAdapter);
        }
    }

}


