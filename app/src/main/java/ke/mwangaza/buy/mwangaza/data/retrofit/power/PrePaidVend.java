package ke.mwangaza.buy.mwangaza.data.retrofit.power;

public class PrePaidVend {

   private String pre_paid_id;
   private String meter_number;
   private String customer_name;
   private int amount;

    public PrePaidVend(String pre_paid_id, String meter_number, String customer_name, int amount) {
        this.pre_paid_id = pre_paid_id;
        this.meter_number = meter_number;
        this.customer_name = customer_name;
        this.amount = amount;
    }
}
