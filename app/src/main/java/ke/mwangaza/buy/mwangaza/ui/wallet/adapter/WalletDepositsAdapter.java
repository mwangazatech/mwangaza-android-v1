package ke.mwangaza.buy.mwangaza.ui.wallet.adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import ke.mwangaza.buy.mwangaza.R;
import ke.mwangaza.buy.mwangaza.data.retrofit.wallet.WalletDeposits;

public class WalletDepositsAdapter extends RecyclerView.Adapter<WalletDepositsAdapter.WalletDepositsViewHolder> {

    Context context;
    private List<WalletDeposits> walletDeposits;

    public WalletDepositsAdapter(Context context, List<WalletDeposits> walletDeposits) {
        this.context = context;
        this.walletDeposits = walletDeposits;
    }

    @NonNull
    @Override
    public WalletDepositsAdapter.WalletDepositsViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.recycler_view_transactions, parent, false);
        final WalletDepositsAdapter.WalletDepositsViewHolder mViewHolder = new WalletDepositsAdapter.WalletDepositsViewHolder(itemView);
        return mViewHolder;
    }

    @Override
    public void onBindViewHolder(@NonNull final WalletDepositsAdapter.WalletDepositsViewHolder holder, final int position) {
        final WalletDeposits transactions = walletDeposits.get(position);

        holder.textViewTransID.setText(transactions.getTrans_id());
        holder.textViewName.setText(transactions.getMpesa_transaction_id());
        holder.textViewAmount.setText(String.valueOf(transactions.getTrans_amount()));
    }

    @Override
    public int getItemCount() {
        return walletDeposits.size();
    }

    class WalletDepositsViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.textViewTransID)
        TextView textViewTransID;
        @BindView(R.id.textViewName)
        TextView textViewName;
        @BindView(R.id.textViewAmount)
        TextView textViewAmount;

        WalletDepositsViewHolder(View view) {
            super(view);
            ButterKnife.bind(this, view);
        }
    }

}