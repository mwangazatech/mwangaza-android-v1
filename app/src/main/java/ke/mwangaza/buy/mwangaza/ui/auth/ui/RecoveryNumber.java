package ke.mwangaza.buy.mwangaza.ui.auth.ui;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import butterknife.BindView;
import butterknife.ButterKnife;
import ke.mwangaza.buy.mwangaza.Mwangaza;
import ke.mwangaza.buy.mwangaza.R;
import ke.mwangaza.buy.mwangaza.data.retrofit.user.ResetOTP;
import ke.mwangaza.buy.mwangaza.data.retrofit.user.ResetOTPRes;
import ke.mwangaza.buy.mwangaza.network.APIClient;
import ke.mwangaza.buy.mwangaza.network.AuthInterface;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class RecoveryNumber extends AppCompatActivity {

    @BindView(R.id.editTextPhoneNumber)
    EditText editTextPhoneNumber;
    @BindView(R.id.buttonReset)
    Button buttonReset;

    Mwangaza app;
    AuthInterface authInterface;
    ProgressDialog progressDialog;

    String phone_number;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_recovery_number);
        ButterKnife.bind(this);

        app = (Mwangaza) getApplicationContext();
        authInterface = APIClient.getClient(app.settings.getBearerToken()).create(AuthInterface.class);

        editTextPhoneNumber.setText(app.settings.getPhoneNumber());

        buttonReset.setOnClickListener(v -> verifyUserPhone());
    }

    private void verifyUserPhone() {

        phone_number = editTextPhoneNumber.getText().toString().trim();

        if (TextUtils.isEmpty(phone_number)) {
            editTextPhoneNumber.setError("Kindly Enter Phone Number");
            return;
        }

        resetPassword();
    }

    private void resetPassword() {

        progressDialog = new ProgressDialog(this);
        progressDialog.setIndeterminate(true);
        progressDialog.setMessage("Requesting SMS OTP...");
        progressDialog.setCancelable(false);
        progressDialog.show();

        ResetOTP resetOTP = new ResetOTP(phone_number);

        Call<ResetOTPRes> call = authInterface.requestForOTP(resetOTP);
        call.enqueue(new Callback<ResetOTPRes>() {
            @Override
            public void onResponse(@NonNull Call<ResetOTPRes> call, @NonNull Response<ResetOTPRes> response) {
                progressDialog.dismiss();
                if (response.isSuccessful()) {
                    ResetOTPRes resetOTPRes = response.body();
                    assert resetOTPRes != null;

                    Intent intent = new Intent(getApplicationContext(), VerifyOtp.class);
                    Bundle bundle = new Bundle();
                    bundle.putString("user_id", resetOTPRes.getUser_id());
                    bundle.putString("phone_number", resetOTPRes.getPhone_number());
                    intent.putExtras(bundle);
                    startActivity(intent);

                } else {
                    Toast.makeText(RecoveryNumber.this, "User Not Found", Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(@NonNull Call<ResetOTPRes> call, @NonNull Throwable t) {
                progressDialog.dismiss();

                Toast.makeText(RecoveryNumber.this, "Ooops...an error occured", Toast.LENGTH_SHORT).show();

                Intent intent = new Intent(getApplicationContext(), LoginActivity.class);
                startActivity(intent);
            }
        });

    }
}
