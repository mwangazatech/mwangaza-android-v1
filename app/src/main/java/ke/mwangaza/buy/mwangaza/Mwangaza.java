package ke.mwangaza.buy.mwangaza;

import android.app.Application;

import com.facebook.stetho.Stetho;

import ke.mwangaza.buy.mwangaza.settings.Settings;

public class Mwangaza extends Application {

    public Settings settings;
    public static Mwangaza instance;

    public Mwangaza() {
        super();
    }

    public static Mwangaza getInstance() {
        return instance;
    }

    @Override
    public void onCreate() {
        super.onCreate();

        settings = new Settings(getApplicationContext());

        if (BuildConfig.DEBUG) {

            Stetho.initializeWithDefaults(this);
        }

    }
}
