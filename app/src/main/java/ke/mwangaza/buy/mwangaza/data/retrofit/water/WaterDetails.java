package ke.mwangaza.buy.mwangaza.data.retrofit.water;

public class WaterDetails {

   private String water_id;
   private String user_id;
   private String account_number;
   private String customer_name;
   private String due_date;
   private String amount;

    public WaterDetails(String water_id, String user_id, String account_number, String customer_name, String due_date, String amount) {
        this.water_id = water_id;
        this.user_id = user_id;
        this.account_number = account_number;
        this.customer_name = customer_name;
        this.due_date = due_date;
        this.amount = amount;
    }

    public String getWater_id() {
        return water_id;
    }

    public void setWater_id(String water_id) {
        this.water_id = water_id;
    }

    public String getUser_id() {
        return user_id;
    }

    public void setUser_id(String user_id) {
        this.user_id = user_id;
    }

    public String getAccount_number() {
        return account_number;
    }

    public void setAccount_number(String account_number) {
        this.account_number = account_number;
    }

    public String getCustomer_name() {
        return customer_name;
    }

    public void setCustomer_name(String customer_name) {
        this.customer_name = customer_name;
    }

    public String getDue_date() {
        return due_date;
    }

    public void setDue_date(String due_date) {
        this.due_date = due_date;
    }

    public String getAmount() {
        return amount;
    }

    public void setAmount(String amount) {
        this.amount = amount;
    }
}
