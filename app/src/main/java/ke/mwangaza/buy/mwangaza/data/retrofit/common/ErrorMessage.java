package ke.mwangaza.buy.mwangaza.data.retrofit.common;

public class ErrorMessage {

    private String message;
    private String trans_ref;

    public ErrorMessage(String message, String trans_ref) {
        this.message = message;
        this.trans_ref = trans_ref;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getTrans_ref() {
        return trans_ref;
    }

    public void setTrans_ref(String trans_ref) {
        this.trans_ref = trans_ref;
    }
}
