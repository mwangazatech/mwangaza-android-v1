package ke.mwangaza.buy.mwangaza.ui.tv;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.CardView;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;

import butterknife.BindView;
import butterknife.ButterKnife;
import ke.mwangaza.buy.mwangaza.Mwangaza;
import ke.mwangaza.buy.mwangaza.R;
import ke.mwangaza.buy.mwangaza.data.retrofit.common.ErrorMessage;
import ke.mwangaza.buy.mwangaza.data.retrofit.wallet.PayBill;
import ke.mwangaza.buy.mwangaza.data.retrofit.wallet.Utilities;
import ke.mwangaza.buy.mwangaza.data.retrofit.wallet.UtilitiesResponse;
import ke.mwangaza.buy.mwangaza.network.APIClient;
import ke.mwangaza.buy.mwangaza.network.WalletInterface;
import ke.mwangaza.buy.mwangaza.ui.main.ui.DashboardActivity;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class TvInternetActivity extends AppCompatActivity {

    @BindView(R.id.toolbar)
    Toolbar toolbar;
    @BindView(R.id.cancel)
    Button cancel;
    @BindView(R.id.dstv)
    CardView dstv;
    @BindView(R.id.zuku)
    CardView zuku;
    @BindView(R.id.startimes)
    CardView startimes;
    @BindView(R.id.gotv)
    CardView gotv;
    @BindView(R.id.kwese)
    CardView kwese;
    @BindView(R.id.faiba)
    CardView faiba;
    @BindView(R.id.pay)
    TextView pay;
    @BindView(R.id.paybilltext)
    EditText paybilltext;
    @BindView(R.id.accountreference)
    EditText accountreference;
    @BindView(R.id.amountext)
    EditText amountext;
    @BindView(R.id.choice_tokens)
    Button choice_tokens;
    @BindView(R.id.cost)
    TextView cost;

    String paybill, account, amount;
    String biller_name = "";
    int calculatableAmount;

    Mwangaza app;
    WalletInterface walletInterface;
    ProgressDialog progressDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_tv_internet);
        ButterKnife.bind(this);

        toolbar.setTitle(R.string.titleTV);
        setSupportActionBar(toolbar);
        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setDisplayShowHomeEnabled(true);
        }

        amountext.addTextChangedListener(new TextWatcher() {
            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

                // TODO Auto-generated method stub
            }

            @Override
            public void afterTextChanged(Editable s) {
                if (!TextUtils.isEmpty(s)) {
                    calculatableAmount = Integer.valueOf(amountext.getText().toString().trim());
                    int i = calculateB2BTransactionCost(calculatableAmount);
                    cost.setText(String.format("Transaction Cost Is: %s", i));
                }
            }
        });

        app = (Mwangaza) getApplicationContext();
        walletInterface = APIClient.getClient(app.settings.getBearerToken()).create(WalletInterface.class);

        choice_tokens.setOnClickListener(v -> {
            if (biller_name.equals("DSTV") || biller_name.equals("GOTV")) {
                cost.setVisibility(View.GONE);
                verifyUtilityDetails();
            } else if (biller_name.equals("NA")) {
                verifyDetails();
            } else {
                Toast.makeText(this, "Kindly Select One Option above", Toast.LENGTH_SHORT).show();
            }
        });

        dstv.setOnClickListener(v -> {
            biller_name = "DSTV";
            pay.setText("Pay DSTV");

        });

        zuku.setOnClickListener(v -> {
            biller_name = "NA";
            paybilltext.setText("320320");
            pay.setText("Pay ZUKU Fibre");
        });

        startimes.setOnClickListener(v -> {
            biller_name = "NA";
            paybilltext.setText("585858");
            pay.setText("Pay StarTimes");

        });

        gotv.setOnClickListener(v -> {
            biller_name = "GOTV";
            pay.setText("Pay GOtv");

        });

        kwese.setOnClickListener(v -> {
            biller_name = "NA";
            paybilltext.setText("733444");
            pay.setText("Pay KWESE");
        });

        faiba.setOnClickListener(v -> {
            biller_name = "NA";
            paybilltext.setText("776611");
            pay.setText("Pay Faiba 4G");

        });

        cancel.setOnClickListener(v -> {
            Intent intents = new Intent(getApplicationContext(), DashboardActivity.class);
            startActivity(intents);
        });

    }

    private void verifyUtilityDetails() {

        account = accountreference.getText().toString().trim();
        amount = amountext.getText().toString().trim();

        if (TextUtils.isEmpty(biller_name)) {
            paybilltext.setError("Kindly Select One Option");
            Toast.makeText(this, "Kindly Select One Option", Toast.LENGTH_SHORT).show();
            return;
        }

        if (TextUtils.isEmpty(account)) {
            accountreference.setError("Kindly Enter Account Reference");
            return;
        }

        if (TextUtils.isEmpty(amount)) {
            amountext.setError("Kindly Enter Amount");
            return;
        }

        if (Integer.valueOf(amount) < 500) {
            amountext.setError("Minimum Amount is KES 500");
            return;
        }

        sendUtility(biller_name);
    }

    private void verifyDetails() {

        paybill = paybilltext.getText().toString().trim();
        account = accountreference.getText().toString().trim();
        amount = amountext.getText().toString().trim();

        if (TextUtils.isEmpty(paybill)) {
            paybilltext.setError("Kindly Enter Paybill Number");
            return;
        }

        if (TextUtils.isEmpty(account)) {
            accountreference.setError("Kindly Enter Account Reference");
            return;
        }

        if (TextUtils.isEmpty(amount)) {
            amountext.setError("Kindly Enter Amount");
            return;
        }

        sendMoney();
    }

    private void sendUtility(String biller_name) {

        progressDialog = new ProgressDialog(this);
        progressDialog.setIndeterminate(true);
        progressDialog.setMessage("Paying Utility...");
        progressDialog.setCancelable(false);
        progressDialog.show();

        Utilities utilities = new Utilities(account, Integer.parseInt(amount), biller_name, biller_name, app.settings.getPhoneNumber());

        Call<UtilitiesResponse> call = walletInterface.payUtility(utilities);
        call.enqueue(new Callback<UtilitiesResponse>() {
            @Override
            public void onResponse(@NonNull Call<UtilitiesResponse> call, @NonNull Response<UtilitiesResponse> response) {
                if (response.isSuccessful()) {
                    progressDialog.dismiss();

                    UtilitiesResponse utilitiesResponse = response.body();
                    assert utilitiesResponse != null;

                    if (utilitiesResponse.isStatus()) {

                        Intent intent = new Intent(getApplicationContext(), TvSuccessActivity.class);
                        Bundle bundle = new Bundle();
                        bundle.putString("message", "`Transaction processed successfully");
                        bundle.putString("trans_ref", utilitiesResponse.getUtility_id());
                        intent.putExtras(bundle);
                        startActivity(intent);
                    } else {
                        Intent intent = new Intent(getApplicationContext(), TvFailureActivity.class);
                        Bundle bundle = new Bundle();
                        bundle.putString("message", "Transaction was not successful");
                        intent.putExtras(bundle);
                        startActivity(intent);
                    }

                } else {
                    progressDialog.dismiss();
                    assert response.body() != null;
                    JSONObject jsonObject = null;
                    try {
                        assert response.errorBody() != null;
                        jsonObject = new JSONObject(response.errorBody().string());

                        Intent intent = new Intent(getApplicationContext(), TvFailureActivity.class);
                        Bundle bundle = new Bundle();
                        bundle.putString("message", jsonObject.getString("message"));
                        intent.putExtras(bundle);
                        startActivity(intent);

                    } catch (JSONException e) {
                        e.printStackTrace();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
            }

            @Override
            public void onFailure(Call<UtilitiesResponse> call, Throwable t) {
                Intent intent = new Intent(getApplicationContext(), TvFailureActivity.class);
                Bundle bundle = new Bundle();
                bundle.putString("message", "Please try your request again...");
                intent.putExtras(intent);
                startActivity(intent);
            }
        });

    }

    private void sendMoney() {

        progressDialog = new ProgressDialog(this);
        progressDialog.setIndeterminate(true);
        progressDialog.setMessage("Paying Bill...");
        progressDialog.setCancelable(false);
        progressDialog.show();

        PayBill payBill = new PayBill(app.settings.getUserId(), Integer.parseInt(amount), account, paybill);

        Call<ErrorMessage> call = walletInterface.payBill(payBill);
        call.enqueue(new Callback<ErrorMessage>() {
            @Override
            public void onResponse(@NonNull Call<ErrorMessage> call, @NonNull Response<ErrorMessage> response) {
                if (response.isSuccessful()) {
                    progressDialog.dismiss();

                    ErrorMessage errorMessage = response.body();
                    assert errorMessage != null;

                    Intent intent = new Intent(getApplicationContext(), TvSuccessActivity.class);
                    Bundle bundle = new Bundle();
                    bundle.putString("message", errorMessage.getMessage());
                    bundle.putString("trans_ref", errorMessage.getTrans_ref());
                    intent.putExtras(bundle);
                    startActivity(intent);

                } else {
                    progressDialog.dismiss();
                    assert response.body() != null;
                    JSONObject jsonObject = null;
                    try {
                        assert response.errorBody() != null;
                        jsonObject = new JSONObject(response.errorBody().string());

                        Intent intent = new Intent(getApplicationContext(), TvFailureActivity.class);
                        Bundle bundle = new Bundle();
                        bundle.putString("message", jsonObject.getString("message"));
                        intent.putExtras(bundle);
                        startActivity(intent);

                    } catch (JSONException e) {
                        e.printStackTrace();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
            }

            @Override
            public void onFailure(@NonNull Call<ErrorMessage> call, @NonNull Throwable t) {
                progressDialog.dismiss();
                Intent intent = new Intent(getApplicationContext(), TvFailureActivity.class);
                Bundle bundle = new Bundle();
                bundle.putString("message", "Please try your request again...");
                intent.putExtras(intent);
                startActivity(intent);
            }
        });
    }

    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }

    private int calculateB2BTransactionCost(int amount) {
        if (amount <= 100) {
            return 10;
        } else if (amount >= 101 && amount <= 500) {
            return 15;
        } else if (amount >= 501 && amount <= 1000) {
            return 20;
        } else if (amount >= 1001 && amount <= 1500) {
            return 25;
        } else if (amount >= 1501 && amount <= 2500) {
            return 30;
        } else if (amount >= 2501 && amount <= 3500) {
            return 35;
        } else if (amount >= 3501 && amount <= 5000) {
            return 50;
        } else if (amount >= 5001 && amount <= 7500) {
            return 60;
        } else if (amount >= 7501 && amount <= 10000) {
            return 60;
        } else if (amount >= 10001 && amount <= 15000) {
            return 85;
        } else if (amount >= 15001 && amount <= 35000) {
            return 100;
        } else if (amount >= 35001 && amount <= 50000) {
            return 190;
        }
        return 190;
    }
}