package ke.mwangaza.buy.mwangaza.ui.power;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.widget.Button;
import android.widget.EditText;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;

import butterknife.BindView;
import butterknife.ButterKnife;
import ke.mwangaza.buy.mwangaza.Mwangaza;
import ke.mwangaza.buy.mwangaza.R;
import ke.mwangaza.buy.mwangaza.data.retrofit.power.PostPaidDetails;
import ke.mwangaza.buy.mwangaza.data.retrofit.power.PostPaidRequest;
import ke.mwangaza.buy.mwangaza.network.APIClient;
import ke.mwangaza.buy.mwangaza.network.PowerInterface;
import ke.mwangaza.buy.mwangaza.ui.main.ui.DashboardActivity;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class PostPaidMeterActivity extends AppCompatActivity {

    @BindView(R.id.toolbar)
    Toolbar toolbar;
    @BindView(R.id.cancel)
    Button cancel;
    @BindView(R.id.postPayAccountNumber)
    EditText postPayAccountNumber;
    @BindView(R.id.verifyAccountNumber)
    Button verifyAccountNumber;

    Mwangaza app;
    PowerInterface powerInterface;
    ProgressDialog progressDialog;

    String account_number, user_id;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_post_paid_meter);
        ButterKnife.bind(this);

        toolbar.setTitle(R.string.button_postpaid);
        setSupportActionBar(toolbar);
        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setDisplayShowHomeEnabled(true);
        }

        app = (Mwangaza) getApplicationContext();
        powerInterface = APIClient.getClient(app.settings.getBearerToken()).create(PowerInterface.class);

        cancel.setOnClickListener(v -> {
            Intent intent = new Intent(getApplicationContext(), DashboardActivity.class);
            startActivity(intent);
        });

        verifyAccountNumber.setOnClickListener(v -> {
            verifyAccNumber();
        });


    }

    private void verifyAccNumber() {

        account_number = postPayAccountNumber.getText().toString().trim();
        user_id = app.settings.getUserId();

        if (TextUtils.isEmpty(account_number)) {
            postPayAccountNumber.setError("Kindly Enter Your Account Number");
            return;
        }

        confirmMeterDetails();
    }

    private void confirmMeterDetails() {

        progressDialog = new ProgressDialog(this);
        progressDialog.setIndeterminate(true);
        progressDialog.setMessage("Verifying Account Number...");
        progressDialog.setCancelable(false);
        progressDialog.show();

        PostPaidRequest postPaidRequest = new PostPaidRequest(user_id, account_number);

        Call<PostPaidDetails> call = powerInterface.verifyPostpay(postPaidRequest);
        call.enqueue(new Callback<PostPaidDetails>() {
            @Override
            public void onResponse(@NonNull Call<PostPaidDetails> call, @NonNull Response<PostPaidDetails> response) {
                if (response.isSuccessful()) {
                    progressDialog.dismiss();

                    PostPaidDetails postPaidDetails = response.body();
                    assert postPaidDetails != null;

                    Intent intent = new Intent(getApplicationContext(), PostPayActivity.class);
                    Bundle bundle = new Bundle();
                    bundle.putString("post_paid_id", postPaidDetails.getPost_paid_id());
                    bundle.putString("account_number", postPaidDetails.getAccount_number());
                    bundle.putString("customer_name", postPaidDetails.getCustomer_name());
                    bundle.putString("customer_balance", postPaidDetails.getCustomer_balance());
                    intent.putExtras(bundle);
                    startActivity(intent);

                } else {
                    progressDialog.dismiss();
                    assert response.body() != null;
                    JSONObject jsonObject = null;
                    try {
                        assert response.errorBody() != null;
                        jsonObject = new JSONObject(response.errorBody().string());

                        Intent intent = new Intent(getApplicationContext(), PostFailureActivity.class);
                        Bundle bundle = new Bundle();
                        bundle.putString("message", jsonObject.getString("message"));
                        intent.putExtras(bundle);
                        startActivity(intent);

                    } catch (JSONException e) {
                        e.printStackTrace();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
            }

            @Override
            public void onFailure(@NonNull Call<PostPaidDetails> call, @NonNull Throwable t) {
                progressDialog.dismiss();
                Intent intent = new Intent(getApplicationContext(), PostFailureActivity.class);
                Bundle bundle = new Bundle();
                bundle.putString("message", "Please try your request again...");
                intent.putExtras(intent);
                startActivity(intent);
            }
        });
    }

    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }
}


