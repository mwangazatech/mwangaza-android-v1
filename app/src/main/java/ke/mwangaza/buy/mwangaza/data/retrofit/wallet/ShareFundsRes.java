package ke.mwangaza.buy.mwangaza.data.retrofit.wallet;

public class ShareFundsRes {

    private String name;
    private String phone_number;
    private int amount;
    private int balance;

    public ShareFundsRes(String name, String phone_number, int amount, int balance) {
        this.name = name;
        this.phone_number = phone_number;
        this.amount = amount;
        this.balance = balance;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPhone_number() {
        return phone_number;
    }

    public void setPhone_number(String phone_number) {
        this.phone_number = phone_number;
    }

    public int getAmount() {
        return amount;
    }

    public void setAmount(int amount) {
        this.amount = amount;
    }

    public int getBalance() {
        return balance;
    }

    public void setBalance(int balance) {
        this.balance = balance;
    }
}
