package ke.mwangaza.buy.mwangaza.data.retrofit.user;

public class LoginUser {

    private String phone_number;
    private String pin;

    public LoginUser(String phone_number, String pin) {
        this.phone_number = phone_number;
        this.pin = pin;
    }
}
