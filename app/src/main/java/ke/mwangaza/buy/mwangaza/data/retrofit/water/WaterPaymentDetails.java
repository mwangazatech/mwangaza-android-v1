package ke.mwangaza.buy.mwangaza.data.retrofit.water;

public class WaterPaymentDetails {

   private String water_id;
   private String account_number;
   private String amount;
   private String paid;

    public WaterPaymentDetails(String water_id, String account_number, String amount, String paid) {
        this.water_id = water_id;
        this.account_number = account_number;
        this.amount = amount;
        this.paid = paid;
    }
}