package ke.mwangaza.buy.mwangaza.data.retrofit.power;

public class PostPaidRequest {

    private String user_id;
    private String account_number;

    public PostPaidRequest(String user_id, String account_number) {
        this.user_id = user_id;
        this.account_number = account_number;
    }
}
