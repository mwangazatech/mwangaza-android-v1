package ke.mwangaza.buy.mwangaza.data.retrofit.water;

public class PayWaterRequest {

    private String water_id;
    private String amount;

    public PayWaterRequest(String water_id, String amount) {
        this.water_id = water_id;
        this.amount = amount;
    }
}
