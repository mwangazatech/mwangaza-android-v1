package ke.mwangaza.buy.mwangaza.ui.wallet.adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import ke.mwangaza.buy.mwangaza.R;
import ke.mwangaza.buy.mwangaza.data.retrofit.power.PrePaidVendResponse;

public class PrepaidTokensAdapter extends RecyclerView.Adapter<PrepaidTokensAdapter.PrePaidVendResponseViewHolder> {

    Context context;
    private List<PrePaidVendResponse> prePaidVendResponseList;

    public PrepaidTokensAdapter(Context context, List<PrePaidVendResponse> prePaidVendResponseList) {
        this.context = context;
        this.prePaidVendResponseList = prePaidVendResponseList;
    }

    @NonNull
    @Override
    public PrepaidTokensAdapter.PrePaidVendResponseViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.recycler_view_tokens, parent, false);
        final PrepaidTokensAdapter.PrePaidVendResponseViewHolder mViewHolder = new PrepaidTokensAdapter.PrePaidVendResponseViewHolder(itemView);
        return mViewHolder;
    }

    @Override
    public void onBindViewHolder(@NonNull final PrepaidTokensAdapter.PrePaidVendResponseViewHolder holder, final int position) {
        final PrePaidVendResponse transactions = prePaidVendResponseList.get(position);

        holder.textViewMeterNumber.setText(transactions.getMeter_number());
        holder.textViewToken.setText(String.valueOf(transactions.getToken()));
        holder.textViewCustomerName.setText(transactions.getCustomer_name());
        holder.textViewUnits.setText(String.valueOf(transactions.getUnits()));
    }

    @Override
    public int getItemCount() {
        return prePaidVendResponseList.size();
    }

    class PrePaidVendResponseViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.textViewMeterNumber)
        TextView textViewMeterNumber;
        @BindView(R.id.textViewCustomerName)
        TextView textViewCustomerName;
        @BindView(R.id.textViewToken)
        TextView textViewToken;
        @BindView(R.id.textViewUnits)
        TextView textViewUnits;

        PrePaidVendResponseViewHolder(View view) {
            super(view);
            ButterKnife.bind(this, view);
        }
    }

}