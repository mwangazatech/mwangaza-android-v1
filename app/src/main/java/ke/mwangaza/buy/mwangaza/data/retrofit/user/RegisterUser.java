package ke.mwangaza.buy.mwangaza.data.retrofit.user;

public class RegisterUser {

   private String name;
   private String phone_number;
   private String facebook_token;
   private String pin;

    public RegisterUser(String name, String phone_number, String pin, String facebook_token) {
        this.name = name;
        this.phone_number = phone_number;
        this.facebook_token = facebook_token;
        this.pin = pin;
    }
}
