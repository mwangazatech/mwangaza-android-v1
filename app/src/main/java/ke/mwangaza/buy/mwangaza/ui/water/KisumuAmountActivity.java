package ke.mwangaza.buy.mwangaza.ui.water;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;

import butterknife.BindView;
import butterknife.ButterKnife;
import ke.mwangaza.buy.mwangaza.Mwangaza;
import ke.mwangaza.buy.mwangaza.R;
import ke.mwangaza.buy.mwangaza.data.retrofit.common.ErrorMessage;
import ke.mwangaza.buy.mwangaza.data.retrofit.wallet.PayBill;
import ke.mwangaza.buy.mwangaza.network.APIClient;
import ke.mwangaza.buy.mwangaza.network.WalletInterface;
import ke.mwangaza.buy.mwangaza.network.WaterInterface;
import ke.mwangaza.buy.mwangaza.ui.main.ui.DashboardActivity;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class KisumuAmountActivity extends AppCompatActivity {

    @BindView(R.id.toolbar)
    Toolbar toolbar;
    @BindView(R.id.cancel)
    Button cancel;

    @BindView(R.id.name)
    EditText editTextAmount;
    @BindView(R.id.paybill)
    Button payBill;

    @BindView(R.id.customer)
    TextView customer;
    @BindView(R.id.meter)
    TextView meter;
    @BindView(R.id.amount)
    TextView amt;

    Mwangaza app;
    WaterInterface waterInterface;
    WalletInterface walletInterface;
    ProgressDialog progressDialog;

    String user_id, water_id, account_number, customer_name, due_date, amount, amount_pay;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_kisumu_amount);
        ButterKnife.bind(this);

        toolbar.setTitle(R.string.water_kisumu);
        setSupportActionBar(toolbar);
        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setDisplayShowHomeEnabled(true);
        }

        app = (Mwangaza) getApplicationContext();
        waterInterface = APIClient.getClient(app.settings.getBearerToken()).create(WaterInterface.class);
        walletInterface = APIClient.getClient(app.settings.getBearerToken()).create(WalletInterface.class);

        Intent intent = getIntent();
        Bundle bundle = intent.getExtras();

        assert bundle != null;
        water_id = bundle.getString("water_id");
        user_id = bundle.getString("user_id");
        account_number = bundle.getString("account_number");
        customer_name = bundle.getString("customer_name");
        due_date = bundle.getString("due_date");
        amount = bundle.getString("amount");

        customer.setText(String.format("Customer Name: %s", customer_name));
        meter.setText(String.format("Account Number: %s", account_number));
        amt.setText(String.format("Amount Due: %s", amount));

        payBill.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                verifyDetails();
            }
        });

        cancel.setOnClickListener(v -> {
            Intent cancel = new Intent(getApplicationContext(), DashboardActivity.class);
            startActivity(cancel);
        });
    }

    private void verifyDetails() {

        amount_pay = editTextAmount.getText().toString().trim();

        if (TextUtils.isEmpty(amount_pay)) {
            editTextAmount.setError("Kindly Enter Amount");
            return;
        }

        sendMoney();

    }

    private void sendMoney() {

        progressDialog = new ProgressDialog(this);
        progressDialog.setIndeterminate(true);
        progressDialog.setMessage("Paying Kisumu Water...");
        progressDialog.setCancelable(false);
        progressDialog.show();

        PayBill payBill = new PayBill(app.settings.getUserId(), Integer.parseInt(amount_pay), account_number, "517000");

        Call<ErrorMessage> call = walletInterface.payBill(payBill);
        call.enqueue(new Callback<ErrorMessage>() {
            @Override
            public void onResponse(@NonNull Call<ErrorMessage> call, @NonNull Response<ErrorMessage> response) {
                if (response.isSuccessful()) {
                    progressDialog.dismiss();

                    ErrorMessage errorMessage = response.body();
                    assert errorMessage != null;

                    Intent intent = new Intent(getApplicationContext(), KisumuSuccessActivity.class);
                    Bundle bundle = new Bundle();
                    bundle.putString("message", errorMessage.getMessage());
                    bundle.putString("trans_ref", errorMessage.getTrans_ref());
                    intent.putExtras(bundle);
                    startActivity(intent);

                } else {
                    progressDialog.dismiss();
                    assert response.body() != null;
                    JSONObject jsonObject = null;
                    try {
                        assert response.errorBody() != null;
                        jsonObject = new JSONObject(response.errorBody().string());

                        Intent intent = new Intent(getApplicationContext(), KisumuFailActivity.class);
                        Bundle bundle = new Bundle();
                        bundle.putString("message", jsonObject.getString("message"));
                        intent.putExtras(bundle);
                        startActivity(intent);

                    } catch (JSONException e) {
                        e.printStackTrace();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
            }

            @Override
            public void onFailure(@NonNull Call<ErrorMessage> call, @NonNull Throwable t) {
                progressDialog.dismiss();
                Intent intent = new Intent(getApplicationContext(), KisumuFailActivity.class);
                Bundle bundle = new Bundle();
                bundle.putString("message", "Please try your request again...");
                intent.putExtras(intent);
                startActivity(intent);
            }
        });
    }

    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }
}
