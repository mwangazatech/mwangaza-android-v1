package ke.mwangaza.buy.mwangaza.ui.funds;

import android.Manifest;
import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.provider.ContactsContract;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Toast;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;

import butterknife.BindView;
import butterknife.ButterKnife;
import ke.mwangaza.buy.mwangaza.Mwangaza;
import ke.mwangaza.buy.mwangaza.R;
import ke.mwangaza.buy.mwangaza.data.retrofit.wallet.ShareFunds;
import ke.mwangaza.buy.mwangaza.data.retrofit.wallet.ShareFundsRes;
import ke.mwangaza.buy.mwangaza.network.APIClient;
import ke.mwangaza.buy.mwangaza.network.WalletInterface;
import ke.mwangaza.buy.mwangaza.ui.main.ui.DashboardActivity;
import retrofit2.Callback;
import retrofit2.Response;

public class ShareFundsActivity extends AppCompatActivity {

    @BindView(R.id.toolbar)
    Toolbar toolbar;
    @BindView(R.id.cancel)
    Button cancel;
    @BindView(R.id.button_contacts)
    ImageView phoneContacts;

    @BindView(R.id.accountnumber)
    EditText etAccountNumber;
    @BindView(R.id.amount_funds)
    EditText etAmount;
    @BindView(R.id.share_funds)
    Button shareFunds;

    String accountNumber, amount;
    ProgressDialog progressDialog;
    Mwangaza app;
    WalletInterface walletInterface;

    private final static int PICK_CONTACT = 0;
    private static final int MY_PERMISSIONS_REQUEST_READ_CONTACTS = 99;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_share_funds);
        ButterKnife.bind(this);

        toolbar.setTitle(R.string.share_funds);
        setSupportActionBar(toolbar);
        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setDisplayShowHomeEnabled(true);
        }

        app = (Mwangaza) getApplicationContext();
        walletInterface = APIClient.getClient(app.settings.getBearerToken()).create(WalletInterface.class);

        cancel.setOnClickListener(v -> {
            Intent intent = new Intent(getApplicationContext(), DashboardActivity.class);
            startActivity(intent);
        });

        shareFunds.setOnClickListener(view -> verifyDetails());
        phoneContacts.setOnClickListener(v -> pickContact());
        checkContactPermissions();
    }

    private void verifyDetails() {

        accountNumber = etAccountNumber.getText().toString().trim();
        amount = etAmount.getText().toString().trim();

        if (TextUtils.isEmpty(accountNumber)) {
            etAccountNumber.setError("Kindly Enter Phone Number");
            return;
        }

        if (TextUtils.isEmpty(amount)) {
            etAmount.setError("Kindly Enter Amount");
            return;
        }

        shareFundsWallet();
    }

    private void shareFundsWallet() {

        progressDialog = new ProgressDialog(this);
        progressDialog.setIndeterminate(true);
        progressDialog.setMessage("Processing Request...");
        progressDialog.setCancelable(false);
        progressDialog.show();

        ShareFunds shareFunds = new ShareFunds(accountNumber, Integer.parseInt(amount));

        retrofit2.Call<ShareFundsRes> call = walletInterface.shareFunds(shareFunds);
        call.enqueue(new Callback<ShareFundsRes>() {
            @Override
            public void onResponse(@NonNull retrofit2.Call<ShareFundsRes> call, @NonNull Response<ShareFundsRes> response) {
                if (response.isSuccessful()) {
                    progressDialog.dismiss();
                    ShareFundsRes shareFundsRes = response.body();
                    assert shareFundsRes != null;

                    Intent intent = new Intent(getApplicationContext(), SuccessShareFundsActivity.class);
                    Bundle bundle = new Bundle();
                    bundle.putString("name", shareFundsRes.getName());
                    bundle.putString("phone_number", shareFundsRes.getPhone_number());
                    bundle.putString("amount", String.valueOf(shareFundsRes.getAmount()));
                    bundle.putString("balance", String.valueOf(shareFundsRes.getBalance()));
                    intent.putExtras(bundle);
                    startActivity(intent);
                } else {
                    progressDialog.dismiss();
                    assert response.body() != null;
                    JSONObject jsonObject = null;
                    try {
                        assert response.errorBody() != null;
                        jsonObject = new JSONObject(response.errorBody().string());

                        Intent intent = new Intent(getApplicationContext(), BankFailureActivity.class);
                        Bundle bundle = new Bundle();
                        bundle.putString("message", jsonObject.getString("message"));
                        intent.putExtras(bundle);
                        startActivity(intent);

                    } catch (JSONException e) {
                        e.printStackTrace();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
            }

            @Override
            public void onFailure(@NonNull retrofit2.Call<ShareFundsRes> call, @NonNull Throwable t) {
                progressDialog.dismiss();
                Intent intent = new Intent(getApplicationContext(), BankFailureActivity.class);
                Bundle bundle = new Bundle();
                bundle.putString("message", "Please try your request again...");
                intent.putExtras(intent);
                startActivity(intent);
            }
        });
    }
    private void checkContactPermissions() {
        if (ContextCompat.checkSelfPermission(this, Manifest.permission.READ_CONTACTS) != PackageManager.PERMISSION_GRANTED) {
            Toast.makeText(getApplicationContext(),
                    "We need to read your contacts to find the phone number.",
                    Toast.LENGTH_SHORT).show();
            ActivityCompat.requestPermissions(ShareFundsActivity.this,
                    new String[]{android.Manifest.permission.READ_CONTACTS}, MY_PERMISSIONS_REQUEST_READ_CONTACTS);
        } else {
            Toast.makeText(getApplicationContext(),
                    "Contacts permission granted.",
                    Toast.LENGTH_SHORT).show();
        }
    }

    private void pickContact() {
        Intent intent = new Intent(Intent.ACTION_PICK, ContactsContract.Contacts.CONTENT_URI);
        startActivityForResult(intent, PICK_CONTACT);
    }

    @Override
    public void onActivityResult(int reqCode, int resultCode, Intent data) {
        super.onActivityResult(reqCode, resultCode, data);

        switch (reqCode) {
            case (PICK_CONTACT):
                if (resultCode == Activity.RESULT_OK) {
                    Uri contactData = data.getData();
                    Cursor c = getContentResolver().query(contactData, null, null, null, null);
                    if (c.moveToFirst()) {
                        String contactId = c.getString(c.getColumnIndex(ContactsContract.Contacts._ID));
                        String hasNumber = c.getString(c.getColumnIndex(ContactsContract.Contacts.HAS_PHONE_NUMBER));
                        String num = "";
                        if (Integer.valueOf(hasNumber) == 1) {
                            Cursor numbers = getContentResolver().query(ContactsContract.CommonDataKinds.Phone.CONTENT_URI, null, ContactsContract.CommonDataKinds.Phone.CONTACT_ID + " = " + contactId, null, null);
                            while (numbers.moveToNext()) {
                                num = numbers.getString(numbers.getColumnIndex(ContactsContract.CommonDataKinds.Phone.NUMBER));
                                Toast.makeText(ShareFundsActivity.this, "Number=" + num, Toast.LENGTH_LONG).show();
                                etAccountNumber.setText(num);
                            }
                        }
                    }
                    break;
                }
        }
    }


    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }
}
