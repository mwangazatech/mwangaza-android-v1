package ke.mwangaza.buy.mwangaza.ui.airtime;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.widget.Button;
import android.widget.TextView;

import butterknife.BindView;
import butterknife.ButterKnife;
import ke.mwangaza.buy.mwangaza.R;
import ke.mwangaza.buy.mwangaza.ui.main.ui.DashboardActivity;

public class AirtimeSuccessActivity extends AppCompatActivity {

    @BindView(R.id.details)
    TextView details;
    @BindView(R.id.amount)
    TextView amount;
    @BindView(R.id.toolbar)
    Toolbar toolbar;
    @BindView(R.id.done)
    Button done;
    @BindView(R.id.share_airtime)
    Button share;

    String receipt_number, total_amount, phone_number, transaction_id, date;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_airtime_success);
        ButterKnife.bind(this);

        toolbar.setTitle(R.string.buy_success);
        setSupportActionBar(toolbar);
        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setDisplayShowHomeEnabled(true);
        }

        Intent intent = getIntent();
        Bundle bundle = intent.getExtras();

        assert bundle != null;
        receipt_number = bundle.getString("receipt_number");
        total_amount = bundle.getString("amount");
        phone_number = bundle.getString("phone_number");

        details.setText(String.format("Receipt Number: %s", receipt_number));
        amount.setText(String.format("Amount: %s", total_amount));

        share.setOnClickListener(v -> {
            Intent myIntent = new Intent(Intent.ACTION_SEND);
            myIntent.setType("text/plain");
            String shareBody = "MWANGAZA AIRTIME RECEIPT" + "\n" + "========================" + "\n" + "Mobile: " + phone_number + "\n" + "Amount: " + total_amount + "\n" + "Transaction: " + receipt_number;
            myIntent.putExtra(Intent.EXTRA_TEXT, shareBody);
            startActivity(Intent.createChooser(myIntent, "Share via..."));
        });

        done.setOnClickListener(v -> {
            Intent dashIntent = new Intent(getApplicationContext(), DashboardActivity.class);
            startActivity(dashIntent);
        });

    }

    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }
}
