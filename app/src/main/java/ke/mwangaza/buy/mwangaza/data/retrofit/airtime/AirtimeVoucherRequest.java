package ke.mwangaza.buy.mwangaza.data.retrofit.airtime;

public class AirtimeVoucherRequest {

    private int amount;
    private String operator;

    public AirtimeVoucherRequest(int amount, String operator) {
        this.amount = amount;
        this.operator = operator;
    }
}
