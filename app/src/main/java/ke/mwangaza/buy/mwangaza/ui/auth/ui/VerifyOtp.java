package ke.mwangaza.buy.mwangaza.ui.auth.ui;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import butterknife.BindView;
import butterknife.ButterKnife;
import ke.mwangaza.buy.mwangaza.Mwangaza;
import ke.mwangaza.buy.mwangaza.R;
import ke.mwangaza.buy.mwangaza.data.retrofit.common.ErrorMessage;
import ke.mwangaza.buy.mwangaza.data.retrofit.user.ResetOTPRes;
import ke.mwangaza.buy.mwangaza.data.retrofit.user.VerifyOTP;
import ke.mwangaza.buy.mwangaza.network.APIClient;
import ke.mwangaza.buy.mwangaza.network.AuthInterface;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class VerifyOtp extends AppCompatActivity {

    @BindView(R.id.buttonReset)
    Button buttonReset;
    @BindView(R.id.ediTextOTP)
    EditText ediTextOTP;

    Mwangaza app;
    AuthInterface authInterface;
    ProgressDialog progressDialog;

    String otp_code, user_id, phone_number;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_verify_otp);
        ButterKnife.bind(this);

        app = (Mwangaza) getApplicationContext();
        authInterface = APIClient.getClient(app.settings.getBearerToken()).create(AuthInterface.class);

        Intent intent = getIntent();
        Bundle bundle = intent.getExtras();

        assert bundle != null;
        user_id = bundle.getString("user_id");
        phone_number = bundle.getString("phone_number");

        buttonReset.setOnClickListener(v -> verifyDetails());
    }

    private void verifyDetails() {

        otp_code = ediTextOTP.getText().toString().trim();

        if (TextUtils.isEmpty(otp_code)) {
            ediTextOTP.setError("Kindly enter the OTP");
            return;
        }

        resetPassword();
    }

    private void resetPassword() {

        progressDialog = new ProgressDialog(this);
        progressDialog.setIndeterminate(true);
        progressDialog.setMessage("Verifying OTP Code...");
        progressDialog.setCancelable(false);
        progressDialog.show();

        VerifyOTP verifyOtp = new VerifyOTP(user_id, otp_code);

        Call<ErrorMessage> call = authInterface.verifyOTP(verifyOtp);
        call.enqueue(new Callback<ErrorMessage>() {
            @Override
            public void onResponse(@NonNull Call<ErrorMessage> call, @NonNull Response<ErrorMessage> response) {
                progressDialog.dismiss();
                if (response.isSuccessful()) {
                    ErrorMessage errorMessage = response.body();
                    assert errorMessage != null;

                    Intent intent = new Intent(getApplicationContext(), ForgotPassActivity.class);
                    Bundle bundle = new Bundle();
                    bundle.putString("user_id", user_id);
                    intent.putExtras(bundle);
                    startActivity(intent);

                } else {
                    Toast.makeText(VerifyOtp.this, "OTP Code is Wrong!", Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(@NonNull Call<ErrorMessage> call, @NonNull Throwable t) {
                progressDialog.dismiss();

                Toast.makeText(VerifyOtp.this, "Ooops...an error occured", Toast.LENGTH_SHORT).show();

                Intent intent = new Intent(getApplicationContext(), LoginActivity.class);
                startActivity(intent);
            }
        });
    }
}
