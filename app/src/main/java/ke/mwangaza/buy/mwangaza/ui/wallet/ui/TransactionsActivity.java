package ke.mwangaza.buy.mwangaza.ui.wallet.ui;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.AppBarLayout;
import android.support.design.widget.TabLayout;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.widget.TextView;

import butterknife.BindView;
import butterknife.ButterKnife;
import ke.mwangaza.buy.mwangaza.Mwangaza;
import ke.mwangaza.buy.mwangaza.R;
import ke.mwangaza.buy.mwangaza.data.retrofit.common.ErrorMessage;
import ke.mwangaza.buy.mwangaza.data.retrofit.user.Firebase;
import ke.mwangaza.buy.mwangaza.data.retrofit.wallet.WalletBalance;
import ke.mwangaza.buy.mwangaza.network.APIClient;
import ke.mwangaza.buy.mwangaza.network.AuthInterface;
import ke.mwangaza.buy.mwangaza.network.WalletInterface;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class TransactionsActivity extends AppCompatActivity {

    @BindView(R.id.toolbar)
    Toolbar toolbar;
    @BindView(R.id.balance)
    TextView balance;
    @BindView(R.id.walletbalance)
    TextView walletbalance;
    WalletInterface walletInterface;
    AuthInterface authInterface;
    Mwangaza app;
    private TabLayout tablayout;
    private AppBarLayout appBarlayout;
    private ViewPager viewPager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_transactions);
        ButterKnife.bind(this);

        toolbar.setTitle(R.string.my_transactions);
        setSupportActionBar(toolbar);
        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setDisplayShowHomeEnabled(true);
        }

        app = (Mwangaza) getApplicationContext();
        walletInterface = APIClient.getClient(app.settings.getBearerToken()).create(WalletInterface.class);
        authInterface = APIClient.getClient(app.settings.getBearerToken()).create(AuthInterface.class);

        tablayout = findViewById(R.id.tablayout_id);
        appBarlayout = findViewById(R.id.appbarid);
        viewPager = findViewById(R.id.pager);
        PageAdapter adapter = new PageAdapter(getSupportFragmentManager());

        adapter.AddFragment(new StatusFragment(app), "Tokens");
        adapter.AddFragment(new RecentFragment(app), "Recent");

        viewPager.setAdapter(adapter);
        tablayout.setupWithViewPager(viewPager);

        refreshBalance();
        updateFirebase();
    }

    private void updateFirebase() {

        Firebase firebase = new Firebase(app.settings.getFirebaseToken());

        Call<ErrorMessage> call = authInterface.updateFCMToken(firebase);
        call.enqueue(new Callback<ErrorMessage>() {
            @Override
            public void onResponse(@NonNull Call<ErrorMessage> call, @NonNull Response<ErrorMessage> response) {

            }

            @Override
            public void onFailure(@NonNull Call<ErrorMessage> call, @NonNull Throwable t) {

            }
        });
    }

    private void refreshBalance() {
        Call<WalletBalance> call = walletInterface.getWalletBalance();
        call.enqueue(new Callback<WalletBalance>() {
            @Override
            public void onResponse(@NonNull Call<WalletBalance> call, @NonNull Response<WalletBalance> response) {
                if (response.isSuccessful()) {
                    WalletBalance walletBalance = response.body();
                    assert walletBalance != null;

                    walletbalance.setText(String.format("KES %s", walletBalance.getBalance()));
                }
            }

            @Override
            public void onFailure(@NonNull Call<WalletBalance> call, @NonNull Throwable t) {

            }
        });
    }

    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }
}
