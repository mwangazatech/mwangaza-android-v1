package ke.mwangaza.buy.mwangaza.ui.info.ui;

import android.os.Bundle;
import android.support.design.widget.AppBarLayout;
import android.support.design.widget.TabLayout;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;

import butterknife.BindView;
import butterknife.ButterKnife;
import ke.mwangaza.buy.mwangaza.Mwangaza;
import ke.mwangaza.buy.mwangaza.R;

public class MyNumbers extends AppCompatActivity {

    @BindView(R.id.toolbar)
    Toolbar toolbar;
    Mwangaza app;

    private TabLayout tablayout;
    private AppBarLayout appBarlayout;
    private ViewPager viewPager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_my_numbers);
        ButterKnife.bind(this);

        toolbar.setTitle(R.string.my_transactions);
        setSupportActionBar(toolbar);
        if (getSupportActionBar() != null) {
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        }

        app = (Mwangaza) getApplicationContext();

        tablayout = findViewById(R.id.tablayout_id);
        appBarlayout = findViewById(R.id.appbarid);
        viewPager = findViewById(R.id.pager);
        InfoAdapter adapter = new InfoAdapter(getSupportFragmentManager());

        adapter.AddFragment(new KenyaPowerNumbers(app), "Kenya Power");
        adapter.AddFragment(new AirtimeNumbers(app), "Airtime");
        adapter.AddFragment(new WaterNumbers(app), "Water");

        viewPager.setAdapter(adapter);
        tablayout.setupWithViewPager(viewPager);
    }

    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }
}
