package ke.mwangaza.buy.mwangaza.ui.wallet.adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import ke.mwangaza.buy.mwangaza.R;
import ke.mwangaza.buy.mwangaza.data.retrofit.wallet.MyTransactions;

public class TransactionsAdapter extends RecyclerView.Adapter<TransactionsAdapter.MyTransactionsViewHolder> {

    Context context;
    private List<MyTransactions> transactionsList;

    public TransactionsAdapter(Context context, List<MyTransactions> transactionsList) {
        this.context = context;
        this.transactionsList = transactionsList;
    }

    @NonNull
    @Override
    public TransactionsAdapter.MyTransactionsViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.recycler_view_transactions, parent, false);
        final TransactionsAdapter.MyTransactionsViewHolder mViewHolder = new TransactionsAdapter.MyTransactionsViewHolder(itemView);
        return mViewHolder;
    }

    @Override
    public void onBindViewHolder(@NonNull final TransactionsAdapter.MyTransactionsViewHolder holder, final int position) {
        final MyTransactions transactions = transactionsList.get(position);

        holder.textViewName.setText(transactions.getName());
        holder.textViewAmount.setText(String.valueOf(transactions.getAmount()));
        holder.textViewTransID.setText(transactions.getTransaction_id());
    }

    @Override
    public int getItemCount() {
        return transactionsList.size();
    }

    class MyTransactionsViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.textViewName)
        TextView textViewName;
        @BindView(R.id.textViewAmount)
        TextView textViewAmount;
        @BindView(R.id.textViewTransID)
        TextView textViewTransID;

        MyTransactionsViewHolder(View view) {
            super(view);
            ButterKnife.bind(this, view);
        }
    }

}