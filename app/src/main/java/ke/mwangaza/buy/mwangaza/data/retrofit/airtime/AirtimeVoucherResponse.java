package ke.mwangaza.buy.mwangaza.data.retrofit.airtime;

public class AirtimeVoucherResponse {

    private String airtime_id;
    private String shop_id;
    private String ref;
    private int amount;
    private String pincode;
    private boolean transaction_status;

    public AirtimeVoucherResponse(String airtime_id, String shop_id, String ref, int amount, String pincode, boolean transaction_status) {
        this.airtime_id = airtime_id;
        this.shop_id = shop_id;
        this.ref = ref;
        this.amount = amount;
        this.pincode = pincode;
        this.transaction_status = transaction_status;
    }

    public String getAirtime_id() {
        return airtime_id;
    }

    public void setAirtime_id(String airtime_id) {
        this.airtime_id = airtime_id;
    }

    public String getShop_id() {
        return shop_id;
    }

    public void setShop_id(String shop_id) {
        this.shop_id = shop_id;
    }

    public String getRef() {
        return ref;
    }

    public void setRef(String ref) {
        this.ref = ref;
    }

    public int getAmount() {
        return amount;
    }

    public void setAmount(int amount) {
        this.amount = amount;
    }

    public String getPincode() {
        return pincode;
    }

    public void setPincode(String pincode) {
        this.pincode = pincode;
    }

    public boolean isTransaction_status() {
        return transaction_status;
    }

    public void setTransaction_status(boolean transaction_status) {
        this.transaction_status = transaction_status;
    }
}
