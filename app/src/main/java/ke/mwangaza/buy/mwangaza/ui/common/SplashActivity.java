package ke.mwangaza.buy.mwangaza.ui.common;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;


import ke.mwangaza.buy.mwangaza.R;
import ke.mwangaza.buy.mwangaza.ui.auth.ui.WelcomeActivity;

public class SplashActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);

        new Handler().postDelayed(() -> {

            startActivity(new Intent(SplashActivity.this, WelcomeActivity.class));
            finish();

        }, 3000);

    }

}
