package ke.mwangaza.buy.mwangaza.ui.power;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.widget.Button;
import android.widget.TextView;

import butterknife.BindView;
import butterknife.ButterKnife;
import ke.mwangaza.buy.mwangaza.R;
import ke.mwangaza.buy.mwangaza.ui.main.ui.DashboardActivity;

public class PrepaidFailureActivity extends AppCompatActivity {

    @BindView(R.id.toolbar)
    Toolbar toolbar;
    @BindView(R.id.cancel)
    Button buttonDashboard;
    @BindView(R.id.details)
    TextView details;

    String message;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_failure_kp);
        ButterKnife.bind(this);

        toolbar.setTitle(R.string.tokens_failure);
        setSupportActionBar(toolbar);
        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setDisplayShowHomeEnabled(true);
        }

        Intent intent = getIntent();
        Bundle bundle = intent.getExtras();

        assert bundle != null;
        message = bundle.getString("message");

        details.setText(String.format("Reason : %s", message));

        buttonDashboard.setOnClickListener(v -> {
            Intent dashboard = new Intent(getApplicationContext(), DashboardActivity.class);
            startActivity(dashboard);
        });
    }

    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }
}
