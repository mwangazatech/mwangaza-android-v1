package ke.mwangaza.buy.mwangaza.network;

import java.util.List;

import ke.mwangaza.buy.mwangaza.data.retrofit.power.PostPaidDetails;
import ke.mwangaza.buy.mwangaza.data.retrofit.power.PostPaidMeters;
import ke.mwangaza.buy.mwangaza.data.retrofit.power.PostPaidRequest;
import ke.mwangaza.buy.mwangaza.data.retrofit.power.PostPaidVend;
import ke.mwangaza.buy.mwangaza.data.retrofit.power.PostPaidVendResponse;
import ke.mwangaza.buy.mwangaza.data.retrofit.power.PrePaidDetails;
import ke.mwangaza.buy.mwangaza.data.retrofit.power.PrePaidMeters;
import ke.mwangaza.buy.mwangaza.data.retrofit.power.PrePaidRequest;
import ke.mwangaza.buy.mwangaza.data.retrofit.power.PrePaidVend;
import ke.mwangaza.buy.mwangaza.data.retrofit.power.PrePaidVendResponse;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.POST;

public interface PowerInterface {

    @POST("power/prepaid/verify")
    Call<PrePaidDetails> verifyPrepaid(@Body PrePaidRequest prePaidRequest);

    @POST("power/prepaid/purchase")
    Call<PrePaidVendResponse> vendPrepaid(@Body PrePaidVend prePaidVend);

    @POST("power/postpay/verify")
    Call<PostPaidDetails> verifyPostpay(@Body PostPaidRequest postPaidRequest);

    @POST("power/postpay/purchase")
    Call<PostPaidVendResponse> vendPostpaid(@Body PostPaidVend postPaidVend);

    @GET("config/prepaid/meters")
    Call<List<PrePaidMeters>> getRecentPrePaidMeters();

    @GET("config/postpaid/meters")
    Call<List<PostPaidMeters>> getRecentPostPaidMeters();

}
