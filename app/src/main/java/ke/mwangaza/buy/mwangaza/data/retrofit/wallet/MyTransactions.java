package ke.mwangaza.buy.mwangaza.data.retrofit.wallet;

public class MyTransactions {

    private String transaction_id;
    private String name;
    private int amount;

    public MyTransactions(String transaction_id, String name, int amount) {
        this.transaction_id = transaction_id;
        this.name = name;
        this.amount = amount;
    }

    public String getTransaction_id() {
        return transaction_id;
    }

    public void setTransaction_id(String transaction_id) {
        this.transaction_id = transaction_id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getAmount() {
        return amount;
    }

    public void setAmount(int amount) {
        this.amount = amount;
    }
}

