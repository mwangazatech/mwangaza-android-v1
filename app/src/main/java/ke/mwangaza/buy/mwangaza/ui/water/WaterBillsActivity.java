package ke.mwangaza.buy.mwangaza.ui.water;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.widget.Button;

import butterknife.BindView;
import butterknife.ButterKnife;
import ke.mwangaza.buy.mwangaza.R;
import ke.mwangaza.buy.mwangaza.ui.main.ui.DashboardActivity;

public class WaterBillsActivity extends AppCompatActivity {

    @BindView(R.id.toolbar)
    Toolbar toolbar;
    @BindView(R.id.cancel)
    Button cancel;
    @BindView(R.id.choice_kisumu)
    Button kisumu;
    @BindView(R.id.choice_nairobi)
    Button nairobi;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_water_bills);
        ButterKnife.bind(this);

        toolbar.setTitle(R.string.watertitle);
        setSupportActionBar(toolbar);
        if(getSupportActionBar() != null){
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        }

        cancel.setOnClickListener(v -> {
            Intent intent = new Intent(getApplicationContext(), DashboardActivity.class);
            startActivity(intent);
        });
        nairobi.setOnClickListener(v -> {
            Intent intent = new Intent(getApplicationContext(), NairobiMeterActivity.class);
            startActivity(intent);
        });
        kisumu.setOnClickListener(v -> {
            Intent intent = new Intent(getApplicationContext(), KisumuMeterActivity.class);
            startActivity(intent);
        });
    }
    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }
}
