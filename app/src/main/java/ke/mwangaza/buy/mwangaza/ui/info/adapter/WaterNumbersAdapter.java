package ke.mwangaza.buy.mwangaza.ui.info.adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import ke.mwangaza.buy.mwangaza.R;
import ke.mwangaza.buy.mwangaza.data.retrofit.water.WaterMeters;

public class WaterNumbersAdapter extends RecyclerView.Adapter<WaterNumbersAdapter.MyTransactionsViewHolder> {

    Context context;
    private List<WaterMeters> transactionsList;

    CustomItemClickListener listener;

    public WaterNumbersAdapter(Context context, List<WaterMeters> transactionsList, CustomItemClickListener listener) {
        this.context = context;
        this.transactionsList = transactionsList;
        this.listener = listener;
    }

    @NonNull
    @Override
    public WaterNumbersAdapter.MyTransactionsViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.recycler_view_waternumbers, parent, false);
        final WaterNumbersAdapter.MyTransactionsViewHolder mViewHolder = new WaterNumbersAdapter.MyTransactionsViewHolder(itemView);
        return mViewHolder;
    }

    @Override
    public void onBindViewHolder(@NonNull final WaterNumbersAdapter.MyTransactionsViewHolder holder, final int position) {
        final WaterMeters transactions = transactionsList.get(position);

        holder.textCustomerName.setText(transactions.getCustomer_name());
        holder.textMeterNumber.setText(transactions.getAccount_number());

        holder.buy.setOnClickListener(v -> listener.onItemClick(v, position));

    }

    @Override
    public int getItemCount() {
        return transactionsList.size();
    }

    class MyTransactionsViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.textCustomerName)
        TextView textCustomerName;
        @BindView(R.id.textMeterNumber)
        TextView textMeterNumber;
        @BindView(R.id.buy)
        Button buy;

        MyTransactionsViewHolder(View view) {
            super(view);
            ButterKnife.bind(this, view);
        }
    }

}