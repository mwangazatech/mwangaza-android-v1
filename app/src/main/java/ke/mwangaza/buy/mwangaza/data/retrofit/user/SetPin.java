package ke.mwangaza.buy.mwangaza.data.retrofit.user;

public class SetPin {

    private String user_id;
    private String pin;

    public SetPin(String user_id, String pin) {
        this.user_id = user_id;
        this.pin = pin;
    }
}
