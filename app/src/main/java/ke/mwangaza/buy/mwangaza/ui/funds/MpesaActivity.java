package ke.mwangaza.buy.mwangaza.ui.funds;

import android.Manifest;
import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.provider.ContactsContract;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;

import butterknife.BindView;
import butterknife.ButterKnife;
import ke.mwangaza.buy.mwangaza.Mwangaza;
import ke.mwangaza.buy.mwangaza.R;
import ke.mwangaza.buy.mwangaza.data.retrofit.common.ErrorMessage;
import ke.mwangaza.buy.mwangaza.data.retrofit.wallet.SendMoney;
import ke.mwangaza.buy.mwangaza.network.APIClient;
import ke.mwangaza.buy.mwangaza.network.WalletInterface;
import ke.mwangaza.buy.mwangaza.ui.main.ui.DashboardActivity;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class MpesaActivity extends AppCompatActivity {

    private final static int PICK_CONTACT = 0;
    private static final int MY_PERMISSIONS_REQUEST_READ_CONTACTS = 99;
    @BindView(R.id.toolbar)
    Toolbar toolbar;
    @BindView(R.id.cancel)
    Button cancel;
    @BindView(R.id.button_contacts)
    ImageView phoneContacts;
    @BindView(R.id.send_mpesa)
    Button sendMpesa;
    @BindView(R.id.amounts)
    EditText etAmount;
    @BindView(R.id.phone_number)
    EditText etPhoneNumber;
    @BindView(R.id.cost)
    TextView cost;
    WalletInterface walletInterface;
    ProgressDialog progressDialog;
    String accountNumber, amount;
    int calculatableAmount;
    Mwangaza app;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_mpesa);
        ButterKnife.bind(this);

        toolbar.setTitle(R.string.mpesa);
        setSupportActionBar(toolbar);
        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setDisplayShowHomeEnabled(true);
        }

        app = (Mwangaza) getApplicationContext();
        walletInterface = APIClient.getClient(app.settings.getBearerToken()).create(WalletInterface.class);

        etAmount.addTextChangedListener(new TextWatcher() {
            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                if (!TextUtils.isEmpty(s)) {
                    calculatableAmount = Integer.valueOf(etAmount.getText().toString().trim());
                    int i = calculateB2CTransactionCost(calculatableAmount);
                    cost.setText(String.format("Transaction Cost Is: %s", i));
                }
            }
        });

        cancel.setOnClickListener(v -> {
            Intent intent = new Intent(getApplicationContext(), DashboardActivity.class);
            startActivity(intent);
        });

        sendMpesa.setOnClickListener(view -> verifyDetails());

        phoneContacts.setOnClickListener(v -> pickContact());
        checkContactPermissions();

    }

    private void verifyDetails() {

        accountNumber = etPhoneNumber.getText().toString().trim();
        amount = etAmount.getText().toString().trim();

        if (TextUtils.isEmpty(accountNumber)) {
            etPhoneNumber.setError("Kindly Enter Phone Number");
            return;
        }

        if (TextUtils.isEmpty(amount)) {
            etAmount.setError("Kindly Enter Amount");
            return;
        }

        sendMoney();
    }

    private void sendMoney() {

        progressDialog = new ProgressDialog(this);
        progressDialog.setIndeterminate(true);
        progressDialog.setMessage("Processing Request...");
        progressDialog.setCancelable(false);
        progressDialog.show();

        SendMoney sendMoney = new SendMoney(app.settings.getUserId(), Integer.parseInt(amount), accountNumber);

        retrofit2.Call<ErrorMessage> call = walletInterface.sendMoney(sendMoney);
        call.enqueue(new Callback<ErrorMessage>() {
            @Override
            public void onResponse(@NonNull Call<ErrorMessage> call, @NonNull Response<ErrorMessage> response) {
                if (response.isSuccessful()) {
                    progressDialog.dismiss();
                    ErrorMessage errorMessage = response.body();
                    assert errorMessage != null;

                    Intent intent = new Intent(getApplicationContext(), MpesaSuccessActivity.class);
                    Bundle bundle = new Bundle();
                    bundle.putString("message", errorMessage.getMessage());
                    bundle.putString("trans_ref", errorMessage.getTrans_ref());

                    intent.putExtras(bundle);
                    startActivity(intent);
                } else {
                    progressDialog.dismiss();
                    assert response.body() != null;
                    JSONObject jsonObject = null;
                    try {
                        assert response.errorBody() != null;
                        jsonObject = new JSONObject(response.errorBody().string());

                        Intent intent = new Intent(getApplicationContext(), MpesaFailureActivity.class);
                        Bundle bundle = new Bundle();
                        bundle.putString("message", jsonObject.getString("message"));
                        intent.putExtras(bundle);
                        startActivity(intent);

                    } catch (JSONException e) {
                        e.printStackTrace();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
            }

            @Override
            public void onFailure(@NonNull Call<ErrorMessage> call, @NonNull Throwable t) {
                progressDialog.dismiss();

                Intent intent = new Intent(getApplicationContext(), MpesaFailureActivity.class);
                Bundle bundle = new Bundle();
                bundle.putString("message", "Please try your request again...");
                intent.putExtras(intent);
                startActivity(intent);
            }
        });
    }

    private void checkContactPermissions() {
        if (ContextCompat.checkSelfPermission(this, Manifest.permission.READ_CONTACTS) != PackageManager.PERMISSION_GRANTED) {
            Toast.makeText(getApplicationContext(),
                    "We need to read your contacts to find the phone number.",
                    Toast.LENGTH_SHORT).show();
            ActivityCompat.requestPermissions(MpesaActivity.this,
                    new String[]{android.Manifest.permission.READ_CONTACTS}, MY_PERMISSIONS_REQUEST_READ_CONTACTS);
        } else {
            Toast.makeText(getApplicationContext(),
                    "Contacts permission granted.",
                    Toast.LENGTH_SHORT).show();
        }
    }

    private void pickContact() {
        Intent intent = new Intent(Intent.ACTION_PICK, ContactsContract.Contacts.CONTENT_URI);
        startActivityForResult(intent, PICK_CONTACT);
    }

    @Override
    public void onActivityResult(int reqCode, int resultCode, Intent data) {
        super.onActivityResult(reqCode, resultCode, data);

        switch (reqCode) {
            case (PICK_CONTACT):
                if (resultCode == Activity.RESULT_OK) {
                    Uri contactData = data.getData();
                    Cursor c = getContentResolver().query(contactData, null, null, null, null);
                    if (c.moveToFirst()) {
                        String contactId = c.getString(c.getColumnIndex(ContactsContract.Contacts._ID));
                        String hasNumber = c.getString(c.getColumnIndex(ContactsContract.Contacts.HAS_PHONE_NUMBER));
                        String num = "";
                        if (Integer.valueOf(hasNumber) == 1) {
                            Cursor numbers = getContentResolver().query(ContactsContract.CommonDataKinds.Phone.CONTENT_URI, null, ContactsContract.CommonDataKinds.Phone.CONTACT_ID + " = " + contactId, null, null);
                            while (numbers.moveToNext()) {
                                num = numbers.getString(numbers.getColumnIndex(ContactsContract.CommonDataKinds.Phone.NUMBER));
                                String contact_name = numbers.getString(numbers.getColumnIndex(ContactsContract.CommonDataKinds.Phone.DISPLAY_NAME));
                                Toast.makeText(MpesaActivity.this, "You’ve selected " + contact_name, Toast.LENGTH_LONG).show();
                                String numNew = num.replaceAll("\\s+", "");
                                etPhoneNumber.setText(numNew);
                            }
                        }
                    }
                    break;
                }
        }
    }

    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }

    private int calculateB2CTransactionCost(int amount) {
        if (amount <= 100) {
            return 16;
        } else if (amount >= 101 && amount <= 499) {
            return 19;
        } else if (amount >= 500 && amount <= 1000) {
            return 20;
        } else if (amount >= 1000 && amount <= 1500) {
            return 30;
        } else if (amount >= 1500 && amount <= 2500) {
            return 33;
        } else if (amount >= 2500 && amount <= 3500) {
            return 44;
        } else if (amount >= 3501 && amount <= 5000) {
            return 52;
        } else if (amount >= 5001 && amount <= 7500) {
            return 65;
        } else if (amount >= 7501 && amount <= 10000) {
            return 78;
        } else if (amount >= 10001 && amount <= 15000) {
            return 103;
        } else if (amount >= 15001 && amount <= 20000) {
            return 103;
        } else if (amount >= 20001 && amount <= 70000) {
            return 103;
        }
        return 103;
    }
}
