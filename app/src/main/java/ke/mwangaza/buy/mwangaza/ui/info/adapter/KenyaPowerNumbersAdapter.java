package ke.mwangaza.buy.mwangaza.ui.info.adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import ke.mwangaza.buy.mwangaza.R;
import ke.mwangaza.buy.mwangaza.data.retrofit.power.PrePaidMeters;

public class KenyaPowerNumbersAdapter extends RecyclerView.Adapter<KenyaPowerNumbersAdapter.MyTransactionsViewHolder> {

    Context context;
    private List<PrePaidMeters> transactionsList;
    CustomItemClickListener listener;

    private List<PrePaidMeters> paidMeters;

    public KenyaPowerNumbersAdapter(Context context, List<PrePaidMeters> transactionsList, CustomItemClickListener listener) {
        this.context = context;
        this.transactionsList = transactionsList;
        this.listener = listener;
    }

    @NonNull
    @Override
    public KenyaPowerNumbersAdapter.MyTransactionsViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.recycler_view_prepaidnumbers, parent, false);
        final KenyaPowerNumbersAdapter.MyTransactionsViewHolder mViewHolder = new KenyaPowerNumbersAdapter.MyTransactionsViewHolder(itemView);
        return mViewHolder;
    }

    @Override
    public void onBindViewHolder(@NonNull final KenyaPowerNumbersAdapter.MyTransactionsViewHolder holder, final int position) {
        final PrePaidMeters transactions = transactionsList.get(position);

        holder.textMobile.setText(transactions.getAccount_number());
        holder.buy.setOnClickListener(v -> listener.onItemClick(v, position));

    }

    @Override
    public int getItemCount() {
        if (paidMeters != null) {
            return paidMeters.size();
        }
        return transactionsList.size();
    }

    public Object getItem(int location) {
        return paidMeters.get(location);
    }

    /**
     * Remove all elements from the list.
     */
    public void clear() {
        final int size = getItemCount();
        transactionsList.clear();
        paidMeters.clear();
        notifyItemRangeRemoved(0, size);
    }

    public void setItems(List<PrePaidMeters> items) {
        this.clear();
        this.transactionsList = items;
        this.paidMeters = items;
        this.notifyDataSetChanged();
    }

    class MyTransactionsViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.textMobile)
        TextView textMobile;
        @BindView(R.id.buy)
        Button buy;

        MyTransactionsViewHolder(View view) {
            super(view);
            ButterKnife.bind(this, view);
        }
    }

}