package ke.mwangaza.buy.mwangaza.data.model;

import android.arch.persistence.room.Entity;
import android.arch.persistence.room.PrimaryKey;
import android.support.annotation.NonNull;

@Entity
public class User {
    @PrimaryKey()
    @NonNull
    private String user_id;
    private String name;
    private String phone_number;
    private String facebook_token;
    private String device_id;
    private boolean active;
    private String token;

    public User(String user_id, String name, String phone_number, String facebook_token, String device_id, boolean active, String token) {
        this.user_id = user_id;
        this.name = name;
        this.phone_number = phone_number;
        this.facebook_token = facebook_token;
        this.device_id = device_id;
        this.active = active;
        this.token = token;
    }

    public String getUser_id() {
        return user_id;
    }

    public void setUser_id(String user_id) {
        this.user_id = user_id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPhone_number() {
        return phone_number;
    }

    public void setPhone_number(String phone_number) {
        this.phone_number = phone_number;
    }

    public String getDevice_id() {
        return device_id;
    }

    public void setDevice_id(String device_id) {
        this.device_id = device_id;
    }

    public boolean isActive() {
        return active;
    }

    public void setActive(boolean active) {
        this.active = active;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public String getFacebook_token() {
        return facebook_token;
    }

    public void setFacebook_token(String facebook_token) {
        this.facebook_token = facebook_token;
    }
}


