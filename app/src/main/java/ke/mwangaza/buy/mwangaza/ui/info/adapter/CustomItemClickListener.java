package ke.mwangaza.buy.mwangaza.ui.info.adapter;

import android.view.View;

public interface CustomItemClickListener {
    void onItemClick(View v, int position);
}
