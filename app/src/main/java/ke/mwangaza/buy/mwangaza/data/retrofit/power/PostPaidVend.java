package ke.mwangaza.buy.mwangaza.data.retrofit.power;

public class PostPaidVend {

   private String post_paid_id;
   private String account_number;
   private String customer_name;
   private int amount;

    public PostPaidVend(String post_paid_id, String account_number, String customer_name, int amount) {
        this.post_paid_id = post_paid_id;
        this.account_number = account_number;
        this.customer_name = customer_name;
        this.amount = amount;
    }
}
