package ke.mwangaza.buy.mwangaza.ui.power;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.widget.Button;
import android.widget.TextView;

import butterknife.BindView;
import butterknife.ButterKnife;
import ke.mwangaza.buy.mwangaza.R;
import ke.mwangaza.buy.mwangaza.ui.main.ui.DashboardActivity;

public class PostSuccessActivity extends AppCompatActivity {

    @BindView(R.id.toolbar)
    Toolbar toolbar;
    @BindView(R.id.cancel)
    Button cancel;
    @BindView(R.id.share)
    Button share;

    @BindView(R.id.customer)
    TextView customer;
    @BindView(R.id.meter)
    TextView meter;
    @BindView(R.id.receipt)
    TextView receipt;
    @BindView(R.id.amount)
    TextView amount;

    String customer_name, account_number, rct_num, ref, paid_amount;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_post_success);
        ButterKnife.bind(this);

        toolbar.setTitle(R.string.payment_success);
        setSupportActionBar(toolbar);
        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setDisplayShowHomeEnabled(true);
        }

        Intent intent = getIntent();
        Bundle bundle = intent.getExtras();

        assert bundle != null;
        customer_name = bundle.getString("customer_name");
        account_number = bundle.getString("account_number");
        paid_amount = bundle.getString("paid_amount");
        rct_num = bundle.getString("rct_num");
        ref = bundle.getString("ref");

        customer.setText(String.format("Customer Name : %s", customer_name));
        meter.setText(String.format("Account Number : %s", account_number));
        amount.setText(String.format("Amount : %s", paid_amount));
        receipt.setText(String.format("Receipt Number : %s", rct_num));

        cancel.setOnClickListener(v -> {
            Intent dashboard = new Intent(getApplicationContext(), DashboardActivity.class);
            startActivity(dashboard);
        });
        share.setOnClickListener(v -> {
            Intent myIntent = new Intent(Intent.ACTION_SEND);
            myIntent.setType("text/plain");
            String shareBody = "MWANGAZA KPLC POSTPAID" + "\n" + "==========================" + "\n" + "Customer Name:" + customer_name + "\n" + "Account Number: " + account_number + "\n" + "Amount Paid: " + paid_amount + "\n" + "Receipt Number: " + rct_num;
            myIntent.putExtra(Intent.EXTRA_TEXT, shareBody);
            startActivity(Intent.createChooser(myIntent, "Share via..."));
        });
    }

    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }
}
