package ke.mwangaza.buy.mwangaza.network;

import java.util.List;

import ke.mwangaza.buy.mwangaza.data.retrofit.airtime.AirtimePhoneNumbers;
import ke.mwangaza.buy.mwangaza.data.retrofit.airtime.AirtimeRequest;
import ke.mwangaza.buy.mwangaza.data.retrofit.airtime.AirtimeResponse;
import ke.mwangaza.buy.mwangaza.data.retrofit.airtime.AirtimeVoucherRequest;
import ke.mwangaza.buy.mwangaza.data.retrofit.airtime.AirtimeVoucherResponse;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.POST;

public interface AirtimeInterface {

    @POST("airtime/buy")
    Call<AirtimeResponse> buyAirtimePinless(@Body AirtimeRequest airtimeRequest);

    @GET("config/phones")
    Call<List<AirtimePhoneNumbers>> getRecentPhoneNumbers();

    @POST("voucher/user")
    Call<AirtimeVoucherResponse> buyAirtimeVoucher(@Body AirtimeVoucherRequest airtimeVoucherRequest);

}
