package ke.mwangaza.buy.mwangaza.ui.info.ui;

import android.annotation.SuppressLint;
import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.Toast;

import java.util.List;

import ke.mwangaza.buy.mwangaza.Mwangaza;
import ke.mwangaza.buy.mwangaza.R;
import ke.mwangaza.buy.mwangaza.data.retrofit.airtime.AirtimePhoneNumbers;
import ke.mwangaza.buy.mwangaza.network.APIClient;
import ke.mwangaza.buy.mwangaza.network.AirtimeInterface;
import ke.mwangaza.buy.mwangaza.ui.airtime.AirtimeActivity;
import ke.mwangaza.buy.mwangaza.ui.info.adapter.AirtimeNumbersAdapter;
import ke.mwangaza.buy.mwangaza.ui.info.adapter.CustomItemClickListener;
import ke.mwangaza.buy.mwangaza.ui.main.ui.DashboardActivity;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

@SuppressLint("ValidFragment")
public class AirtimeNumbers extends Fragment {

    View view;
    Mwangaza app;
    AirtimeInterface airtimeInterface;

    ProgressDialog progressDialog;

    List<AirtimePhoneNumbers> airtimePhoneNumbersList;
    RecyclerView recyclerView;
    ImageView brokenImageLink;

    public AirtimeNumbers(Mwangaza app) {
        this.app = app;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        view = inflater.inflate(R.layout.fragment_airtime_numbers, container, false);

        airtimeInterface = APIClient.getClient(app.settings.getBearerToken()).create(AirtimeInterface.class);

        recyclerView = view.findViewById(R.id.recyclerView);
        brokenImageLink = view.findViewById(R.id.brokenImageLink);

        loadPreviousNumbers();

        return view;
    }

    private void loadPreviousNumbers() {

        progressDialog = new ProgressDialog(getActivity());
        progressDialog.setIndeterminate(true);
        progressDialog.setMessage("Fetching Recent Phone Numbers...");
        progressDialog.setCancelable(false);
        progressDialog.show();

        Call<List<AirtimePhoneNumbers>> call = airtimeInterface.getRecentPhoneNumbers();
        call.enqueue(new Callback<List<AirtimePhoneNumbers>>() {
            @Override
            public void onResponse(@NonNull Call<List<AirtimePhoneNumbers>> call, @NonNull Response<List<AirtimePhoneNumbers>> response) {
                progressDialog.dismiss();
                if (response.isSuccessful()) {
                    airtimePhoneNumbersList = response.body();
                    assert airtimePhoneNumbersList != null;

                    showTransactions(airtimePhoneNumbersList);
                }
            }

            @Override
            public void onFailure(@NonNull Call<List<AirtimePhoneNumbers>> call, @NonNull Throwable t) {
                progressDialog.dismiss();
                Toast.makeText(getActivity(), "Failed to load recent numbers...", Toast.LENGTH_SHORT).show();
                Intent intent = new Intent(getActivity(), DashboardActivity.class);
                startActivity(intent);
            }
        });

    }

    private void showTransactions(List<AirtimePhoneNumbers> myTransactions) {

        if (myTransactions == null || myTransactions.isEmpty()) {
            brokenImageLink.setVisibility(View.VISIBLE);
            recyclerView.setVisibility(View.INVISIBLE);
        } else {

            AirtimeNumbersAdapter airtimeNumbersAdapter = new AirtimeNumbersAdapter(getActivity(), myTransactions, new CustomItemClickListener() {
                @Override
                public void onItemClick(View v, int position) {
                    Intent intent = new Intent(getActivity(), AirtimeActivity.class);
                    Bundle bundle = new Bundle();
                    bundle.putString("phone_number", myTransactions.get(position).getPhone_number());
                    intent.putExtras(bundle);
                    startActivity(intent);
                }
            });

            LinearLayoutManager mLayoutManager =
                    new LinearLayoutManager(getActivity(), LinearLayoutManager.VERTICAL, false);
            recyclerView.setLayoutManager(mLayoutManager);
            recyclerView.setAdapter(airtimeNumbersAdapter);
        }
    }
}
