package ke.mwangaza.buy.mwangaza.ui.funds;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.toptoche.searchablespinnerlibrary.SearchableSpinner;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import ke.mwangaza.buy.mwangaza.Mwangaza;
import ke.mwangaza.buy.mwangaza.R;
import ke.mwangaza.buy.mwangaza.data.retrofit.common.ErrorMessage;
import ke.mwangaza.buy.mwangaza.data.retrofit.wallet.Banks;
import ke.mwangaza.buy.mwangaza.data.retrofit.wallet.PayBill;
import ke.mwangaza.buy.mwangaza.network.APIClient;
import ke.mwangaza.buy.mwangaza.network.WalletInterface;
import ke.mwangaza.buy.mwangaza.ui.main.ui.DashboardActivity;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class BankPaymentActivity extends AppCompatActivity {

    @BindView(R.id.toolbar)
    Toolbar toolbar;
    @BindView(R.id.cancel)
    Button Cancel;
    @BindView(R.id.bank)
    Button payBank;
    @BindView(R.id.amountext)
    EditText amountext;
    @BindView(R.id.accountreference)
    EditText accountreference;
    @BindView(R.id.spinner)
    SearchableSpinner spinner;
    @BindView(R.id.cost)
    TextView cost;

    WalletInterface walletInterface;
    ProgressDialog progressDialog;
    String accountNumber, amount, paybill;
    int calculatableAmount;
    Mwangaza app;

    List<Banks> banks;
    private ArrayList<String> bankNameList;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_bank_payment);
        ButterKnife.bind(this);

        app = (Mwangaza) getApplicationContext();
        walletInterface = APIClient.getClient(app.settings.getBearerToken()).create(WalletInterface.class);

        getBanksFromServer();

        toolbar.setTitle(R.string.bank_activity);
        setSupportActionBar(toolbar);
        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setDisplayShowHomeEnabled(true);
        }

        amountext.addTextChangedListener(new TextWatcher() {
            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                if (!TextUtils.isEmpty(s)) {
                    calculatableAmount = Integer.valueOf(amountext.getText().toString().trim());
                    int i = calculateB2BTransactionCost(calculatableAmount);
                    cost.setText(String.format("Transaction Cost is: %s", i));
                }
            }
        });

        spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> arg0, View arg1, int position, long id) {
                paybill = banks.get(position).getPaybill();
            }

            @Override
            public void onNothingSelected(AdapterView<?> arg0) {
                // TODO Auto-generated method stub
            }
        });

        payBank.setOnClickListener(v -> verifyDetails());

        Cancel.setOnClickListener(v -> {
            Intent intent = new Intent(getApplicationContext(), DashboardActivity.class);
            startActivity(intent);
        });
    }

    private void getBanksFromServer() {

        progressDialog = new ProgressDialog(this);
        progressDialog.setIndeterminate(true);
        progressDialog.setMessage("Getting Banks...");
        progressDialog.setCancelable(false);
        progressDialog.show();

        Call<List<Banks>> call = walletInterface.getBanksFromServer();
        call.enqueue(new Callback<List<Banks>>() {
            @Override
            public void onResponse(@NonNull Call<List<Banks>> call, @NonNull Response<List<Banks>> response) {
                progressDialog.dismiss();
                if (response.isSuccessful()) {

                    banks = response.body();
                    assert banks != null;

                    setListBanks(banks);

                }
            }

            @Override
            public void onFailure(@NonNull Call<List<Banks>> call, @NonNull Throwable t) {
                progressDialog.dismiss();
                Intent intent = new Intent(getApplicationContext(), DashboardActivity.class);
                Toast.makeText(BankPaymentActivity.this, "Ooops, an error occured! Please try again", Toast.LENGTH_SHORT).show();
                startActivity(intent);
            }
        });
    }

    public void setListBanks(final List<Banks> banksList) {
        banks = banksList;
        bankNameList = new ArrayList<>();

        bankNameList = new ArrayList<>();
        for (Banks bank : banks) {
            bankNameList.add(bank.getBank_name());
        }

        ArrayAdapter<String> banksAdapter = new ArrayAdapter<String>(BankPaymentActivity.this,
                R.layout.spinner_item_rg, bankNameList);

        banksAdapter.setDropDownViewResource(R.layout.spinner_item_rg2);
        spinner.setAdapter(banksAdapter);
    }


    private void verifyDetails() {

        accountNumber = accountreference.getText().toString().trim();
        amount = amountext.getText().toString().trim();

        if (TextUtils.isEmpty(accountNumber)) {
            accountreference.setError("Kindly Enter Your Account Number");
            return;
        }
        if (TextUtils.isEmpty(amount)) {
            amountext.setError("Kindly Enter Amount");
            return;
        }
        if (TextUtils.isEmpty(paybill)) {
            Toast.makeText(this, "Kindly select a Bank from the dropdown", Toast.LENGTH_SHORT).show();
            return;
        }

        sendToBank();

    }

    private void sendToBank() {

        progressDialog = new ProgressDialog(this);
        progressDialog.setIndeterminate(true);
        progressDialog.setMessage("Processing Bank Payment...");
        progressDialog.setCancelable(false);
        progressDialog.show();

        PayBill payBill = new PayBill(app.settings.getUserId(), Integer.parseInt(amount), accountNumber, paybill);

        Call<ErrorMessage> call = walletInterface.payBill(payBill);
        call.enqueue(new Callback<ErrorMessage>() {
            @Override
            public void onResponse(@NonNull Call<ErrorMessage> call, @NonNull Response<ErrorMessage> response) {
                if (response.isSuccessful()) {
                    progressDialog.dismiss();

                    ErrorMessage errorMessage = response.body();
                    assert errorMessage != null;

                    Intent intent = new Intent(getApplicationContext(), BankSuccessActivity.class);
                    Bundle bundle = new Bundle();
                    bundle.putString("message", errorMessage.getMessage());
                    bundle.putString("trans_ref", errorMessage.getTrans_ref());
                    intent.putExtras(bundle);
                    startActivity(intent);

                } else {
                    progressDialog.dismiss();
                    assert response.body() != null;
                    JSONObject jsonObject = null;
                    try {
                        assert response.errorBody() != null;
                        jsonObject = new JSONObject(response.errorBody().string());

                        Intent intent = new Intent(getApplicationContext(), BankFailureActivity.class);
                        Bundle bundle = new Bundle();
                        bundle.putString("message", jsonObject.getString("message"));
                        intent.putExtras(bundle);
                        startActivity(intent);

                    } catch (JSONException e) {
                        e.printStackTrace();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
            }

            @Override
            public void onFailure(@NonNull Call<ErrorMessage> call, @NonNull Throwable t) {
                progressDialog.dismiss();
                Intent intent = new Intent(getApplicationContext(), BankFailureActivity.class);
                Bundle bundle = new Bundle();
                bundle.putString("message", "Please try your request again...");
                intent.putExtras(intent);
                startActivity(intent);
            }
        });
    }

    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }

    private int calculateB2BTransactionCost(int amount) {
        if (amount <= 100) {
            return 10;
        } else if (amount >= 101 && amount <= 500) {
            return 15;
        } else if (amount >= 501 && amount <= 1000) {
            return 20;
        } else if (amount >= 1001 && amount <= 1500) {
            return 25;
        } else if (amount >= 1501 && amount <= 2500) {
            return 30;
        } else if (amount >= 2501 && amount <= 3500) {
            return 35;
        } else if (amount >= 3501 && amount <= 5000) {
            return 50;
        } else if (amount >= 5001 && amount <= 7500) {
            return 60;
        } else if (amount >= 7501 && amount <= 10000) {
            return 60;
        } else if (amount >= 10001 && amount <= 15000) {
            return 85;
        } else if (amount >= 15001 && amount <= 35000) {
            return 100;
        } else if (amount >= 35001 && amount <= 50000) {
            return 190;
        }
        return 190;
    }

}
