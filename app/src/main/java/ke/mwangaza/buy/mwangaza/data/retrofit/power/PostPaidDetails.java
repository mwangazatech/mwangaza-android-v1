package ke.mwangaza.buy.mwangaza.data.retrofit.power;

public class PostPaidDetails {

    private String post_paid_id;
    private String user_id;
    private String account_number;
    private String customer_name;
    private String customer_balance;

    public PostPaidDetails(String post_paid_id, String user_id, String account_number, String customer_name, String customer_balance) {
        this.post_paid_id = post_paid_id;
        this.user_id = user_id;
        this.account_number = account_number;
        this.customer_name = customer_name;
        this.customer_balance = customer_balance;
    }

    public String getPost_paid_id() {
        return post_paid_id;
    }

    public void setPost_paid_id(String post_paid_id) {
        this.post_paid_id = post_paid_id;
    }

    public String getUser_id() {
        return user_id;
    }

    public void setUser_id(String user_id) {
        this.user_id = user_id;
    }

    public String getAccount_number() {
        return account_number;
    }

    public void setAccount_number(String account_number) {
        this.account_number = account_number;
    }

    public String getCustomer_name() {
        return customer_name;
    }

    public void setCustomer_name(String customer_name) {
        this.customer_name = customer_name;
    }

    public String getCustomer_balance() {
        return customer_balance;
    }

    public void setCustomer_balance(String customer_balance) {
        this.customer_balance = customer_balance;
    }
}


