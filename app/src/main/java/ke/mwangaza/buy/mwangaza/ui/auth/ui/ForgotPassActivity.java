package ke.mwangaza.buy.mwangaza.ui.auth.ui;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import butterknife.BindView;
import butterknife.ButterKnife;
import ke.mwangaza.buy.mwangaza.Mwangaza;
import ke.mwangaza.buy.mwangaza.R;
import ke.mwangaza.buy.mwangaza.data.retrofit.user.SetPin;
import ke.mwangaza.buy.mwangaza.data.retrofit.user.UserResponse;
import ke.mwangaza.buy.mwangaza.network.APIClient;
import ke.mwangaza.buy.mwangaza.network.AuthInterface;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ForgotPassActivity extends AppCompatActivity {

    @BindView(R.id.setPin)
    EditText setPin;
    @BindView(R.id.pinCheck)
    EditText pinCheck;
    @BindView(R.id.buttonReset)
    Button buttonReset;

    Mwangaza app;
    AuthInterface authInterface;
    ProgressDialog progressDialog;

    String pin, confirm_pin, user_id;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_forgotpass);
        ButterKnife.bind(this);

        app = (Mwangaza) getApplicationContext();
        authInterface = APIClient.getClient(app.settings.getBearerToken()).create(AuthInterface.class);

        Intent intent = getIntent();
        Bundle bundle = intent.getExtras();

        assert bundle != null;
        user_id = bundle.getString("user_id");

        buttonReset.setOnClickListener(v -> verifyPIN());

    }

    private void verifyPIN() {
        pin = setPin.getText().toString().trim();
        confirm_pin = pinCheck.getText().toString().trim();

        if (TextUtils.isEmpty(pin)) {
            setPin.setError("Kindly Enter Your PIN");
            return;
        }

        if (TextUtils.isEmpty(confirm_pin)) {
            setPin.setError("Kindly Confirm Your PIN");
            return;
        }

        if (!pin.equals(confirm_pin)) {
            setPin.setError("PIN does not match");
            return;
        }

        updatePIN();
    }

    private void updatePIN() {

        progressDialog = new ProgressDialog(this);
        progressDialog.setIndeterminate(true);
        progressDialog.setMessage("Setting New PIN...");
        progressDialog.setCancelable(false);
        progressDialog.show();

        SetPin setPin = new SetPin(user_id, pin);

        Call<UserResponse> call = authInterface.setNewPin(setPin);
        call.enqueue(new Callback<UserResponse>() {
            @Override
            public void onResponse(@NonNull Call<UserResponse> call, @NonNull Response<UserResponse> response) {
                progressDialog.dismiss();
                if (response.isSuccessful()) {
                    Toast.makeText(ForgotPassActivity.this, "PIN set successfully", Toast.LENGTH_SHORT).show();

                    Intent intent = new Intent(getApplicationContext(), LoginActivity.class);
                    startActivity(intent);
                } else {
                    Toast.makeText(ForgotPassActivity.this, "Failed to Update PIN", Toast.LENGTH_SHORT).show();

                    Intent intent = new Intent(getApplicationContext(), LoginActivity.class);
                    startActivity(intent);
                }
            }

            @Override
            public void onFailure(@NonNull Call<UserResponse> call, @NonNull Throwable t) {
                progressDialog.dismiss();

                Toast.makeText(ForgotPassActivity.this, "Ooops...an error occured", Toast.LENGTH_SHORT).show();

                Intent intent = new Intent(getApplicationContext(), LoginActivity.class);
                startActivity(intent);
            }
        });

    }

}
