package ke.mwangaza.buy.mwangaza.ui.tv;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.widget.Button;
import android.content.Intent;
import android.widget.TextView;

import butterknife.BindView;
import butterknife.ButterKnife;
import ke.mwangaza.buy.mwangaza.R;
import ke.mwangaza.buy.mwangaza.ui.main.ui.DashboardActivity;

public class TvSuccessActivity extends AppCompatActivity {

    @BindView(R.id.toolbar)
    Toolbar toolbar;
    @BindView(R.id.cancel)
    Button cancel;
    @BindView(R.id.mobile)
    TextView mobile;
    @BindView(R.id.amount)
    TextView amount;
    @BindView(R.id.share)
    Button share;

    String message, trans_ref;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_tv_success);
        ButterKnife.bind(this);

        toolbar.setTitle(R.string.payment_success);
        setSupportActionBar(toolbar);
        if(getSupportActionBar() != null){
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setDisplayShowHomeEnabled(true);
        }

        Intent intent = getIntent();
        Bundle bundle = intent.getExtras();

        assert bundle != null;
        message = bundle.getString("message");
        trans_ref = bundle.getString("trans_ref");

        mobile.setText(String.format("Message : %s", message));
        amount.setText(String.format("Transaction Reference : %s", trans_ref));

        share.setOnClickListener(v -> {
            Intent myIntent = new Intent(Intent.ACTION_SEND);
            myIntent.setType("text/plain");
            String shareBody = "MWANGAZA PAYMENT”+”\n”+”==========================“+”\n”+”Message: "+ message
                    +"\n"+"Transaction Reference: "+ trans_ref;
            myIntent.putExtra(Intent.EXTRA_TEXT, shareBody);
            startActivity(Intent.createChooser(myIntent, "Share via..."));
        });

        cancel.setOnClickListener(v -> {
            Intent intents = new Intent(getApplicationContext(), DashboardActivity.class);
            startActivity(intents);
        });
    }
    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }
}
