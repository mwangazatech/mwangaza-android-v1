package ke.mwangaza.buy.mwangaza.ui.wallet.ui;

import android.annotation.SuppressLint;
import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.Toast;

import java.util.List;

import butterknife.BindView;
import ke.mwangaza.buy.mwangaza.Mwangaza;
import ke.mwangaza.buy.mwangaza.R;
import ke.mwangaza.buy.mwangaza.data.retrofit.wallet.MyTransactions;
import ke.mwangaza.buy.mwangaza.network.APIClient;
import ke.mwangaza.buy.mwangaza.network.WalletInterface;
import ke.mwangaza.buy.mwangaza.ui.main.ui.DashboardActivity;
import ke.mwangaza.buy.mwangaza.ui.wallet.adapter.TransactionsAdapter;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

@SuppressLint("ValidFragment")
public class RecentFragment extends Fragment {

    View view;
    Mwangaza app;
    WalletInterface walletInterface;
    ProgressDialog progressDialog;

    ImageView brokenImageLink;

    List<MyTransactions> myTransactions;
    RecyclerView recyclerView;

    public RecentFragment(Mwangaza app) {
        this.app = app;
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_recent, container, false);

        walletInterface = APIClient.getClient(app.settings.getBearerToken()).create(WalletInterface.class);

        recyclerView = view.findViewById(R.id.recyclerView);
        brokenImageLink = view.findViewById(R.id.brokenImageLink);

        loadPreviousTransactions();

        return view;
    }

    private void loadPreviousTransactions() {

        progressDialog = new ProgressDialog(getActivity());
        progressDialog.setIndeterminate(true);
        progressDialog.setMessage("Fetching Recent Transactions...");
        progressDialog.setCancelable(false);
        progressDialog.show();

        Call<List<MyTransactions>> call = walletInterface.getRecentTransactions();
        call.enqueue(new Callback<List<MyTransactions>>() {
            @Override
            public void onResponse(@NonNull Call<List<MyTransactions>> call, @NonNull Response<List<MyTransactions>> response) {
                progressDialog.dismiss();
                if (response.isSuccessful()) {
                    myTransactions = response.body();
                    assert myTransactions != null;

                    showTransactions(myTransactions);
                }
            }

            @Override
            public void onFailure(@NonNull Call<List<MyTransactions>> call, @NonNull Throwable t) {
                progressDialog.dismiss();
                Toast.makeText(getActivity(), "Failed to load recent transactions...", Toast.LENGTH_SHORT).show();
                Intent intent = new Intent(getActivity(), DashboardActivity.class);
                startActivity(intent);
            }
        });

    }

    private void showTransactions(List<MyTransactions> myTransactions) {

        if (myTransactions == null || myTransactions.isEmpty()) {
            brokenImageLink.setVisibility(View.VISIBLE);
            recyclerView.setVisibility(View.INVISIBLE);
        } else {

            TransactionsAdapter transactionsAdapter = new TransactionsAdapter(getActivity(), myTransactions);

            LinearLayoutManager mLayoutManager =
                    new LinearLayoutManager(getActivity(), LinearLayoutManager.VERTICAL, false);
            recyclerView.setLayoutManager(mLayoutManager);
            recyclerView.setAdapter(transactionsAdapter);
        }
    }

}
