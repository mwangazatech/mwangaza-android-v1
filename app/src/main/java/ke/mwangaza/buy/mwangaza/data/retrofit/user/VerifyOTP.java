package ke.mwangaza.buy.mwangaza.data.retrofit.user;

public class VerifyOTP {

    private String user_id;
    private String otp;

    public VerifyOTP(String user_id, String otp) {
        this.user_id = user_id;
        this.otp = otp;
    }
}
