package ke.mwangaza.buy.mwangaza.ui.power;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.widget.Button;
import android.widget.TextView;

import butterknife.BindView;
import butterknife.ButterKnife;
import ke.mwangaza.buy.mwangaza.R;
import ke.mwangaza.buy.mwangaza.ui.main.ui.DashboardActivity;

public class PrepaidSuccessActivity extends AppCompatActivity {

    @BindView(R.id.toolbar)
    Toolbar toolbar;
    @BindView(R.id.customer)
    TextView customer;
    @BindView(R.id.meter)
    TextView meter;
    @BindView(R.id.paid)
    TextView paid;
    @BindView(R.id.tokens)
    TextView tokens;
    @BindView(R.id.unit)
    TextView unit;
    @BindView(R.id.cancel)
    Button cancel;
    @BindView(R.id.share)
    Button share;

    String customer_name, meter_number, amount, units, token;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_success);
        ButterKnife.bind(this);
        toolbar.setTitle(R.string.tokens_success);
        setSupportActionBar(toolbar);
        if(getSupportActionBar() != null){
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setDisplayShowHomeEnabled(true);
        }

        Intent intent = getIntent();
        Bundle bundle = intent.getExtras();

        assert bundle != null;
        customer_name = bundle.getString("customer_name");
        meter_number = bundle.getString("meter_number");
        amount = bundle.getString("amount");
        units = bundle.getString("units");
        token = bundle.getString("token");

        customer.setText(String.format("Customer Name : %s", customer_name));
        meter.setText(String.format("Meter Number : %s", meter_number));
        paid.setText(String.format("Amount Paid : %s", amount));
        tokens.setText(String.format("Token Number : %s", token));
        unit.setText(String.format("Number of Units : %s", units));

        share.setOnClickListener(v -> {
            Intent myIntent = new Intent(Intent.ACTION_SEND);
            myIntent.setType("text/plain");
            String shareBody = "MWANGAZA POWER TOKENS"+"\n"+"=========================="+"\n"+"Customer Name: "+ customer_name+"\n"+"Meter Number: "+ meter_number+"\n"+"Amount Paid: "+ amount +"\n"+"Token Number: "+ token+"\n"+"Number of units: "+ units;
            myIntent.putExtra(Intent.EXTRA_TEXT, shareBody);
            startActivity(Intent.createChooser(myIntent, "Share via..."));
        });

        cancel.setOnClickListener(v -> {
            Intent dashIntent = new Intent(getApplicationContext(), DashboardActivity.class);
            startActivity(dashIntent);
        });
    }
    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }
}
