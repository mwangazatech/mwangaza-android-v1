package ke.mwangaza.buy.mwangaza.ui.info.adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import ke.mwangaza.buy.mwangaza.R;
import ke.mwangaza.buy.mwangaza.data.retrofit.airtime.AirtimePhoneNumbers;

public class AirtimeNumbersAdapter extends RecyclerView.Adapter<AirtimeNumbersAdapter.MyTransactionsViewHolder> {

    Context context;
    private List<AirtimePhoneNumbers> transactionsList;
    CustomItemClickListener listener;

    public AirtimeNumbersAdapter(Context context, List<AirtimePhoneNumbers> transactionsList, CustomItemClickListener listener) {
        this.context = context;
        this.transactionsList = transactionsList;
        this.listener = listener;
    }

    @NonNull
    @Override
    public AirtimeNumbersAdapter.MyTransactionsViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.recycler_view_airtimenumbers, parent, false);
        final AirtimeNumbersAdapter.MyTransactionsViewHolder mViewHolder = new AirtimeNumbersAdapter.MyTransactionsViewHolder(itemView);
        return mViewHolder;
    }

    @Override
    public void onBindViewHolder(@NonNull final AirtimeNumbersAdapter.MyTransactionsViewHolder holder, final int position) {
        final AirtimePhoneNumbers transactions = transactionsList.get(position);

        holder.textTelephoneNumber.setText(transactions.getPhone_number());
        holder.buy.setOnClickListener(v -> listener.onItemClick(v, position));

    }

    @Override
    public int getItemCount() {
        return transactionsList.size();
    }

    class MyTransactionsViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.textTelephoneNumber)
        TextView textTelephoneNumber;
        @BindView(R.id.buy)
        Button buy;

        MyTransactionsViewHolder(View view) {
            super(view);
            ButterKnife.bind(this, view);
        }
    }

}