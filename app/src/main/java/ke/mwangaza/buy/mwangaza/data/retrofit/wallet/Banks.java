package ke.mwangaza.buy.mwangaza.data.retrofit.wallet;

public class Banks {
    private String bank_id;
    private String bank_name;
    private String paybill;
    private String created_at;
    private String created_by;
    private boolean active;

    public Banks(String bank_id, String bank_name, String paybill, String created_at, String created_by, boolean active) {
        this.bank_id = bank_id;
        this.bank_name = bank_name;
        this.paybill = paybill;
        this.created_at = created_at;
        this.created_by = created_by;
        this.active = active;
    }

    public String getBank_id() {
        return bank_id;
    }

    public void setBank_id(String bank_id) {
        this.bank_id = bank_id;
    }

    public String getBank_name() {
        return bank_name;
    }

    public void setBank_name(String bank_name) {
        this.bank_name = bank_name;
    }

    public String getPaybill() {
        return paybill;
    }

    public void setPaybill(String paybill) {
        this.paybill = paybill;
    }

    public String getCreated_at() {
        return created_at;
    }

    public void setCreated_at(String created_at) {
        this.created_at = created_at;
    }

    public String getCreated_by() {
        return created_by;
    }

    public void setCreated_by(String created_by) {
        this.created_by = created_by;
    }

    public boolean isActive() {
        return active;
    }

    public void setActive(boolean active) {
        this.active = active;
    }
}
