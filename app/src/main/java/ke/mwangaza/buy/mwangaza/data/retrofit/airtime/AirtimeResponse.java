package ke.mwangaza.buy.mwangaza.data.retrofit.airtime;

public class AirtimeResponse {

    private String airtime_id;
    private String user_id;
    private String phone_number;
    private int amount;
    private String receipt_number;
    private boolean transaction_status;

    public AirtimeResponse(String airtime_id, String user_id, String phone_number, int amount, String receipt_number, boolean transaction_status) {
        this.airtime_id = airtime_id;
        this.user_id = user_id;
        this.phone_number = phone_number;
        this.amount = amount;
        this.receipt_number = receipt_number;
        this.transaction_status = transaction_status;
    }

    public String getAirtime_id() {
        return airtime_id;
    }

    public void setAirtime_id(String airtime_id) {
        this.airtime_id = airtime_id;
    }

    public String getUser_id() {
        return user_id;
    }

    public void setUser_id(String user_id) {
        this.user_id = user_id;
    }

    public String getPhone_number() {
        return phone_number;
    }

    public void setPhone_number(String phone_number) {
        this.phone_number = phone_number;
    }

    public int getAmount() {
        return amount;
    }

    public void setAmount(int amount) {
        this.amount = amount;
    }

    public String getReceipt_number() {
        return receipt_number;
    }

    public void setReceipt_number(String receipt_number) {
        this.receipt_number = receipt_number;
    }

    public boolean isTransaction_status() {
        return transaction_status;
    }

    public void setTransaction_status(boolean transaction_status) {
        this.transaction_status = transaction_status;
    }
}