package ke.mwangaza.buy.mwangaza.data.retrofit.wallet;

public class Utilities {

    private String account;
    private int amount;
    private String biller_name;
    private String merchant_reference;
    private String phone;

    public Utilities(String account, int amount, String biller_name, String merchant_reference, String phone) {
        this.account = account;
        this.amount = amount;
        this.biller_name = biller_name;
        this.merchant_reference = merchant_reference;
        this.phone = phone;
    }
}


