package ke.mwangaza.buy.mwangaza.data.retrofit.airtime;

public class AirtimePhoneNumbers {

    private String phone_number;

    public AirtimePhoneNumbers(String phone_number) {
        this.phone_number = phone_number;
    }

    public String getPhone_number() {
        return phone_number;
    }

    public void setPhone_number(String phone_number) {
        this.phone_number = phone_number;
    }
}