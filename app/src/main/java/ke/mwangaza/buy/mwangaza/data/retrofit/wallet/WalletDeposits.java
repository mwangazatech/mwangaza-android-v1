package ke.mwangaza.buy.mwangaza.data.retrofit.wallet;

public class WalletDeposits {

    private String mpesa_transaction_id;
    private String trans_id;
    private String trans_time;
    private String trans_amount;

    public WalletDeposits(String mpesa_transaction_id, String trans_id, String trans_time, String trans_amount) {
        this.mpesa_transaction_id = mpesa_transaction_id;
        this.trans_id = trans_id;
        this.trans_time = trans_time;
        this.trans_amount = trans_amount;
    }

    public String getMpesa_transaction_id() {
        return mpesa_transaction_id;
    }

    public void setMpesa_transaction_id(String mpesa_transaction_id) {
        this.mpesa_transaction_id = mpesa_transaction_id;
    }

    public String getTrans_id() {
        return trans_id;
    }

    public void setTrans_id(String trans_id) {
        this.trans_id = trans_id;
    }

    public String getTrans_time() {
        return trans_time;
    }

    public void setTrans_time(String trans_time) {
        this.trans_time = trans_time;
    }

    public String getTrans_amount() {
        return trans_amount;
    }

    public void setTrans_amount(String trans_amount) {
        this.trans_amount = trans_amount;
    }
}