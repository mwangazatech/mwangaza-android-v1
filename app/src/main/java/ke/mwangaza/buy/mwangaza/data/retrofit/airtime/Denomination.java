package ke.mwangaza.buy.mwangaza.data.retrofit.airtime;

public class Denomination {

    private int amount;

    public Denomination(int amount) {
        this.amount = amount;
    }

    public int getAmount() {
        return amount;
    }

    public void setAmount(int amount) {
        this.amount = amount;
    }
}
