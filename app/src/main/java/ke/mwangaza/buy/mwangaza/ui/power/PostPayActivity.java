package ke.mwangaza.buy.mwangaza.ui.power;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;

import butterknife.BindView;
import butterknife.ButterKnife;
import ke.mwangaza.buy.mwangaza.Mwangaza;
import ke.mwangaza.buy.mwangaza.R;
import ke.mwangaza.buy.mwangaza.data.retrofit.power.PostPaidVend;
import ke.mwangaza.buy.mwangaza.data.retrofit.power.PostPaidVendResponse;
import ke.mwangaza.buy.mwangaza.network.APIClient;
import ke.mwangaza.buy.mwangaza.network.PowerInterface;
import ke.mwangaza.buy.mwangaza.ui.main.ui.DashboardActivity;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class PostPayActivity extends AppCompatActivity {

    @BindView(R.id.toolbar)
    Toolbar toolbar;
    @BindView(R.id.cancel)
    Button cancel;
    @BindView(R.id.payPostPayBill)
    Button payPostPayBill;

    @BindView(R.id.customer)
    TextView customer;
    @BindView(R.id.meter)
    TextView meter;
    @BindView(R.id.payable)
    TextView payable;
    @BindView(R.id.postPaidAmount)
    EditText postPaidAmount;

    Mwangaza app;
    PowerInterface powerInterface;
    ProgressDialog progressDialog;


    String customer_name, account_number, amount, post_paid_id;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_post_pay);
        ButterKnife.bind(this);

        toolbar.setTitle(R.string.button_postpaid);
        setSupportActionBar(toolbar);
        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setDisplayShowHomeEnabled(true);
        }

        app = (Mwangaza) getApplicationContext();
        powerInterface = APIClient.getClient(app.settings.getBearerToken()).create(PowerInterface.class);

        Intent intent = getIntent();
        Bundle bundle = intent.getExtras();

        assert bundle != null;
        customer_name = bundle.getString("customer_name");
        account_number = bundle.getString("account_number");
        post_paid_id = bundle.getString("post_paid_id");
        amount = bundle.getString("customer_balance");

        customer.setText(String.format("Customer Name : %s", customer_name));
        meter.setText(String.format("Account Number : %s", account_number));
        payable.setText(String.format("Amount Due : %s", amount));

        cancel.setOnClickListener(v -> {
            Intent dashboard = new Intent(getApplicationContext(), DashboardActivity.class);
            startActivity(dashboard);
        });

        payPostPayBill.setOnClickListener(v -> verifyAmount());
    }

    private void verifyAmount() {

        amount = postPaidAmount.getText().toString().trim();

        if (TextUtils.isEmpty(amount)) {
            postPaidAmount.setError("Kindly Enter Amount you wish to pay");
            return;
        }

        makePostPayBillPayment();
    }

    private void makePostPayBillPayment() {

        progressDialog = new ProgressDialog(this);
        progressDialog.setIndeterminate(true);
        progressDialog.setMessage("Vending Postpay Meter...");
        progressDialog.setCancelable(false);
        progressDialog.show();

        PostPaidVend postPaidVend = new PostPaidVend(post_paid_id, account_number, customer_name, Integer.parseInt(amount));

        Call<PostPaidVendResponse> call = powerInterface.vendPostpaid(postPaidVend);
        call.enqueue(new Callback<PostPaidVendResponse>() {
            @Override
            public void onResponse(@NonNull Call<PostPaidVendResponse> call, @NonNull Response<PostPaidVendResponse> response) {
                if (response.isSuccessful()) {
                    progressDialog.dismiss();
                    PostPaidVendResponse postPaidVendResponse = response.body();
                    assert postPaidVendResponse != null;

                    Intent intent = new Intent(getApplicationContext(), PostSuccessActivity.class);
                    Bundle bundle = new Bundle();
                    bundle.putString("customer_name", postPaidVendResponse.getCustomer_name());
                    bundle.putString("account_number", postPaidVendResponse.getAccount_number());
                    bundle.putString("paid_amount", postPaidVendResponse.getAmount());
                    bundle.putString("rct_num", postPaidVendResponse.getRct_num());
                    bundle.putString("ref", postPaidVendResponse.getRef());
                    intent.putExtras(bundle);
                    startActivity(intent);

                } else {
                    progressDialog.dismiss();
                    assert response.body() != null;
                    JSONObject jsonObject = null;
                    try {
                        assert response.errorBody() != null;
                        jsonObject = new JSONObject(response.errorBody().string());

                        Intent intent = new Intent(getApplicationContext(), PostFailureActivity.class);
                        Bundle bundle = new Bundle();
                        bundle.putString("message", jsonObject.getString("message"));
                        intent.putExtras(bundle);
                        startActivity(intent);

                    } catch (JSONException e) {
                        e.printStackTrace();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
            }

            @Override
            public void onFailure(@NonNull Call<PostPaidVendResponse> call, @NonNull Throwable t) {
                progressDialog.dismiss();
                Intent intent = new Intent(getApplicationContext(), PostFailureActivity.class);
                Bundle bundle = new Bundle();
                bundle.putString("message", "Please try your request again...");
                intent.putExtras(intent);
                startActivity(intent);
            }
        });

    }

    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }
}
