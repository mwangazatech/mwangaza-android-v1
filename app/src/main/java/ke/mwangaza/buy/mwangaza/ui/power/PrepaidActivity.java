package ke.mwangaza.buy.mwangaza.ui.power;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.widget.Button;
import android.widget.EditText;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;

import butterknife.BindView;
import butterknife.ButterKnife;
import ke.mwangaza.buy.mwangaza.Mwangaza;
import ke.mwangaza.buy.mwangaza.R;
import ke.mwangaza.buy.mwangaza.data.retrofit.power.PrePaidDetails;
import ke.mwangaza.buy.mwangaza.data.retrofit.power.PrePaidRequest;
import ke.mwangaza.buy.mwangaza.network.APIClient;
import ke.mwangaza.buy.mwangaza.network.PowerInterface;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class PrepaidActivity extends AppCompatActivity {

    @BindView(R.id.toolbar)
    Toolbar toolbar;
    @BindView(R.id.kplcVerifyPrePaidMeter)
    Button kplcVerifyPrePaidMeter;
    @BindView(R.id.kplcPrePaidCancel)
    Button kplcPrePaidCancel;
    @BindView(R.id.kplcPrePaidMeterNumber)
    EditText kplcPrePaidMeterNumber;

    Mwangaza app;
    PowerInterface powerInterface;
    ProgressDialog progressDialog;

    String meter_number, user_id, fetched_meter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_prepaid);
        ButterKnife.bind(this);

        toolbar.setTitle(R.string.kp_token);
        setSupportActionBar(toolbar);
        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setDisplayShowHomeEnabled(true);
        }

        app = (Mwangaza) getApplicationContext();

        powerInterface = APIClient.getClient(app.settings.getBearerToken()).create(PowerInterface.class);

        Intent intent = getIntent();
        Bundle bundle = intent.getExtras();

        if (bundle != null) {
            fetched_meter = bundle.getString("meter_number");
            kplcPrePaidMeterNumber.setText(fetched_meter);
        }

        kplcVerifyPrePaidMeter.setOnClickListener(v -> verifyMeterNumber());
        kplcPrePaidCancel.setOnClickListener(v -> {
            Intent cancel = new Intent(getApplicationContext(), KenyaPowerActivity.class);
            startActivity(cancel);
        });

    }

    private void verifyMeterNumber() {

        meter_number = kplcPrePaidMeterNumber.getText().toString().trim();
        user_id = app.settings.getUserId();

        if (TextUtils.isEmpty(meter_number)) {
            kplcPrePaidMeterNumber.setError("Kindly Enter Your Meter Number");
            return;
        }

        confirmMeterDetails();
    }

    private void confirmMeterDetails() {

        progressDialog = new ProgressDialog(this);
        progressDialog.setIndeterminate(true);
        progressDialog.setMessage("Verifying Meter Number...");
        progressDialog.setCancelable(false);
        progressDialog.show();

        PrePaidRequest prePaidRequest = new PrePaidRequest(user_id, meter_number);

        Call<PrePaidDetails> call = powerInterface.verifyPrepaid(prePaidRequest);
        call.enqueue(new Callback<PrePaidDetails>() {
            @Override
            public void onResponse(@NonNull Call<PrePaidDetails> call, @NonNull Response<PrePaidDetails> response) {
                if (response.isSuccessful()) {
                    progressDialog.dismiss();

                    PrePaidDetails prePaidDetails = response.body();
                    assert prePaidDetails != null;

                    Intent intent = new Intent(getApplicationContext(), TokenAmountActivity.class);
                    Bundle bundle = new Bundle();
                    bundle.putString("pre_paid_id", prePaidDetails.getPre_paid_id());
                    bundle.putString("meter_number", prePaidDetails.getMeter_number());
                    bundle.putString("customer_name", prePaidDetails.getCustomer_name());
                    intent.putExtras(bundle);
                    startActivity(intent);

                } else {
                    progressDialog.dismiss();
                    assert response.body() != null;
                    JSONObject jsonObject = null;
                    try {
                        assert response.errorBody() != null;
                        jsonObject = new JSONObject(response.errorBody().string());

                        Intent intent = new Intent(getApplicationContext(), PrepaidFailureActivity.class);
                        Bundle bundle = new Bundle();
                        bundle.putString("message", jsonObject.getString("message"));
                        intent.putExtras(bundle);
                        startActivity(intent);

                    } catch (JSONException e) {
                        e.printStackTrace();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
            }

            @Override
            public void onFailure(@NonNull Call<PrePaidDetails> call, @NonNull Throwable t) {
                progressDialog.dismiss();
                Intent intent = new Intent(getApplicationContext(), PrepaidFailureActivity.class);
                Bundle bundle = new Bundle();
                bundle.putString("message", "Please try your request again...");
                intent.putExtras(intent);
                startActivity(intent);
            }
        });

    }

    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }
}
