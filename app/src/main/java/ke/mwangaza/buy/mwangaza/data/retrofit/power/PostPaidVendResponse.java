package ke.mwangaza.buy.mwangaza.data.retrofit.power;

public class PostPaidVendResponse {

    private String post_paid_id;
    private String user_id;
    private String account_number;
    private String customer_name;
    private String rct_num;
    private String amount;
    private int trans_id;
    private String ref;

    public PostPaidVendResponse(String post_paid_id, String user_id, String account_number, String customer_name, String rct_num, String amount, int trans_id, String ref) {
        this.post_paid_id = post_paid_id;
        this.user_id = user_id;
        this.account_number = account_number;
        this.customer_name = customer_name;
        this.rct_num = rct_num;
        this.amount = amount;
        this.trans_id = trans_id;
        this.ref = ref;
    }

    public String getPost_paid_id() {
        return post_paid_id;
    }

    public void setPost_paid_id(String post_paid_id) {
        this.post_paid_id = post_paid_id;
    }

    public String getUser_id() {
        return user_id;
    }

    public void setUser_id(String user_id) {
        this.user_id = user_id;
    }

    public String getAccount_number() {
        return account_number;
    }

    public void setAccount_number(String account_number) {
        this.account_number = account_number;
    }

    public String getCustomer_name() {
        return customer_name;
    }

    public void setCustomer_name(String customer_name) {
        this.customer_name = customer_name;
    }

    public String getRct_num() {
        return rct_num;
    }

    public void setRct_num(String rct_num) {
        this.rct_num = rct_num;
    }

    public String getAmount() {
        return amount;
    }

    public void setAmount(String amount) {
        this.amount = amount;
    }

    public int getTrans_id() {
        return trans_id;
    }

    public void setTrans_id(int trans_id) {
        this.trans_id = trans_id;
    }

    public String getRef() {
        return ref;
    }

    public void setRef(String ref) {
        this.ref = ref;
    }
}
