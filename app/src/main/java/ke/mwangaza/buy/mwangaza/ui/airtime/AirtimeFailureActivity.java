package ke.mwangaza.buy.mwangaza.ui.airtime;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.AppCompatButton;
import android.support.v7.widget.Toolbar;
import android.widget.Button;
import android.widget.TextView;

import butterknife.BindView;
import butterknife.ButterKnife;
import ke.mwangaza.buy.mwangaza.PinAirtime;
import ke.mwangaza.buy.mwangaza.R;
import ke.mwangaza.buy.mwangaza.ui.main.ui.DashboardActivity;

public class AirtimeFailureActivity extends AppCompatActivity {

    @BindView(R.id.cancel)
    Button cancel;
    @BindView(R.id.details)
    TextView details;
    @BindView(R.id.toolbar)
    Toolbar toolbar;
    @BindView(R.id.buyAirtime)
    AppCompatButton buyAirtime;

    String message;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_airtime_fail);
        ButterKnife.bind(this);

        toolbar.setTitle(R.string.failure);
        setSupportActionBar(toolbar);
        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setDisplayShowHomeEnabled(true);
        }

        Intent intent = getIntent();
        Bundle bundle = intent.getExtras();

        assert bundle != null;
        message = bundle.getString("message");

        buyAirtime.setOnClickListener(v -> {
            Intent dashboard = new Intent(getApplicationContext(), PinAirtime.class);
            startActivity(dashboard);
        });

        details.setText(String.format("Reason : %s", message));

        cancel.setOnClickListener(v -> {
            Intent dashboard = new Intent(getApplicationContext(), DashboardActivity.class);
            startActivity(dashboard);
        });
    }

    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }
}
