package ke.mwangaza.buy.mwangaza.ui.auth.ui;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.Toast;

import com.facebook.accountkit.AccessToken;
import com.facebook.accountkit.AccountKit;
import com.facebook.accountkit.AccountKitLoginResult;
import com.facebook.accountkit.ui.AccountKitActivity;
import com.facebook.accountkit.ui.AccountKitConfiguration;
import com.facebook.accountkit.ui.LoginType;

import butterknife.BindView;
import butterknife.ButterKnife;
import ke.mwangaza.buy.mwangaza.Mwangaza;
import ke.mwangaza.buy.mwangaza.R;

public class WelcomeActivity extends AppCompatActivity {

    public static int REQUEST_CODE = 999;


    @BindView(R.id.phoneLogin)
    Button phoneLogin;
    @BindView(R.id.checkBox)
    CheckBox checkBox;

    Mwangaza app;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_welcome);
        ButterKnife.bind(this);
        app = (Mwangaza) getApplication();

        AccessToken accessToken = AccountKit.getCurrentAccessToken();
        if (accessToken != null) {
            goToProfileInActivity();
        }

        checkBox.setOnCheckedChangeListener((buttonView, isChecked) -> {

            if (isChecked) {
                phoneLogin.setVisibility(View.VISIBLE);
            } else {
                phoneLogin.setVisibility(View.INVISIBLE);

            }
        });


        phoneLogin.setOnClickListener(view -> startLoginPage());

    }

    private void goToProfileInActivity() {
        Intent intent = new Intent(this, LoginActivity.class);
        startActivity(intent);
        finish();
    }

    private void startLoginPage() {

        Intent intent = new Intent(this, AccountKitActivity.class);
        AccountKitConfiguration.AccountKitConfigurationBuilder configurationBuilder =
                new AccountKitConfiguration.AccountKitConfigurationBuilder(LoginType.PHONE,
                        AccountKitActivity.ResponseType.TOKEN);
        configurationBuilder.setDefaultCountryCode("KE");
        configurationBuilder.setReadPhoneStateEnabled(true);
        configurationBuilder.setReceiveSMS(true);
        intent.putExtra(AccountKitActivity.ACCOUNT_KIT_ACTIVITY_CONFIGURATION, configurationBuilder.build());
        startActivityForResult(intent, REQUEST_CODE);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        {
            super.onActivityResult(requestCode, resultCode, data);
            if (requestCode == REQUEST_CODE) {
                AccountKitLoginResult myresult = data.getParcelableExtra(AccountKitLoginResult.RESULT_KEY);
                if (myresult.getError() != null) {
                    Toast.makeText(this, "" + myresult.getError().getErrorType().getMessage(), Toast.LENGTH_SHORT).show();
                    //return;
                } else if (myresult.wasCancelled()) {
                    Toast.makeText(this, "Login Cancelled", Toast.LENGTH_SHORT).show();
                    //eturn;
                } else {
                    if (myresult.getAccessToken() != null)
                        Toast.makeText(this, "Success:" + myresult.getAccessToken().getAccountId(), Toast.LENGTH_SHORT).show();
                    else {
                        Toast.makeText(this, "Success:" + myresult.getAuthorizationCode(), Toast.LENGTH_SHORT).show();
                    }
                    startActivity(new Intent(WelcomeActivity.this, RegistrationActivity.class));
                    finish();
                }

            }
        }
    }
}
