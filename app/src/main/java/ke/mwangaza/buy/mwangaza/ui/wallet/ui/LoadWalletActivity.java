package ke.mwangaza.buy.mwangaza.ui.wallet.ui;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.util.Log;
import android.widget.Button;
import android.widget.EditText;

import com.androidstudy.daraja.Daraja;
import com.androidstudy.daraja.DarajaListener;
import com.androidstudy.daraja.model.AccessToken;
import com.androidstudy.daraja.model.LNMExpress;
import com.androidstudy.daraja.model.LNMResult;
import com.androidstudy.daraja.util.Env;
import com.androidstudy.daraja.util.TransactionType;

import butterknife.BindView;
import butterknife.ButterKnife;
import ke.mwangaza.buy.mwangaza.Mwangaza;
import ke.mwangaza.buy.mwangaza.R;
import ke.mwangaza.buy.mwangaza.ui.funds.LoadFailure;
import ke.mwangaza.buy.mwangaza.ui.main.ui.DashboardActivity;


public class LoadWalletActivity extends AppCompatActivity {

    @BindView(R.id.toolbar)
    Toolbar toolbar;
    @BindView(R.id.cancel)
    Button Cancel;
    @BindView(R.id.loadWallet)
    Button loadWallet;

    @BindView(R.id.amount)
    EditText amount;

    String topupAmount;

    Daraja daraja;
    Mwangaza app;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_load_wallet);
        ButterKnife.bind(this);

        toolbar.setTitle(R.string.title_wallet);
        setSupportActionBar(toolbar);
        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setDisplayShowHomeEnabled(true);
        }

        app = (Mwangaza) getApplication();

        daraja = Daraja.with("A3GHd0I73mJQDfkwaWu07X8KENIrZIWt", "BTi15A6cMYA1n98y", Env.PRODUCTION, new DarajaListener<AccessToken>() {
            @Override
            public void onResult(@NonNull AccessToken accessToken) {
                Log.i(LoadWalletActivity.this.getClass().getSimpleName(), accessToken.getAccess_token());
            }

            @Override
            public void onError(String error) {
                Log.e(LoadWalletActivity.this.getClass().getSimpleName(), error);
            }
        });


        loadWallet.setOnClickListener(v -> checkWallet());


        Cancel.setOnClickListener(v -> {
            Intent intent = new Intent(getApplicationContext(), DashboardActivity.class);
            startActivity(intent);
        });

    }

    private void checkWallet() {

        topupAmount = amount.getText().toString().trim();

        if (TextUtils.isEmpty(topupAmount)) {
            amount.setError("Kindly Enter Amount you wish to topup");
        }

        triggerSTKPush();
    }

    private void triggerSTKPush() {

        LNMExpress lnmExpress = new LNMExpress(
                "156194",
                "e1186a4acf4eaa669f5382562c2b3e1d4d7847663b7d25cc8d39c86dfbc60029",
                TransactionType.CustomerBuyGoodsOnline,
                topupAmount,
                app.settings.getPhoneNumber(),
                "325386",
                app.settings.getPhoneNumber(),
                "https://api.mwangaza.ke/api/v1/mpesa/xpress",
                app.settings.getPhoneNumber(),
                "Wallet Top Up"
        );

        daraja.requestMPESAExpress(lnmExpress, new DarajaListener<LNMResult>() {
            @Override
            public void onResult(@NonNull LNMResult lnmResult) {
                Log.i(LoadWalletActivity.this.getClass().getSimpleName(), lnmResult.ResponseDescription);
                Intent intents = new Intent(getApplicationContext(), DashboardActivity.class);
                startActivity(intents);
            }

            @Override
            public void onError(String error) {
                Log.i(LoadWalletActivity.this.getClass().getSimpleName(), error);
                Intent myintent = new Intent(getApplicationContext(), LoadFailure.class);
                myintent.putExtra("message", "STKPush is currently not working, please load your wallet via Buy Goods (Till) 325386");
                startActivity(myintent);
            }
        });
    }

    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }
}
