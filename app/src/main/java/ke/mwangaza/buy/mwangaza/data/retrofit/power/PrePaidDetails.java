package ke.mwangaza.buy.mwangaza.data.retrofit.power;

public class PrePaidDetails {

    private String pre_paid_id;
    private String meter_number;
    private String customer_name;

    public PrePaidDetails(String pre_paid_id, String meter_number, String customer_name) {
        this.pre_paid_id = pre_paid_id;
        this.meter_number = meter_number;
        this.customer_name = customer_name;
    }

    public String getPre_paid_id() {
        return pre_paid_id;
    }

    public void setPre_paid_id(String pre_paid_id) {
        this.pre_paid_id = pre_paid_id;
    }

    public String getMeter_number() {
        return meter_number;
    }

    public void setMeter_number(String meter_number) {
        this.meter_number = meter_number;
    }

    public String getCustomer_name() {
        return customer_name;
    }

    public void setCustomer_name(String customer_name) {
        this.customer_name = customer_name;
    }
}
