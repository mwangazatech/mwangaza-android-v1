package ke.mwangaza.buy.mwangaza.network;

import java.util.List;

import ke.mwangaza.buy.mwangaza.data.retrofit.common.ErrorMessage;
import ke.mwangaza.buy.mwangaza.data.retrofit.power.PrePaidVendResponse;
import ke.mwangaza.buy.mwangaza.data.retrofit.wallet.Banks;
import ke.mwangaza.buy.mwangaza.data.retrofit.wallet.MyTransactions;
import ke.mwangaza.buy.mwangaza.data.retrofit.wallet.PayBill;
import ke.mwangaza.buy.mwangaza.data.retrofit.wallet.SendMoney;
import ke.mwangaza.buy.mwangaza.data.retrofit.wallet.ShareFunds;
import ke.mwangaza.buy.mwangaza.data.retrofit.wallet.ShareFundsRes;
import ke.mwangaza.buy.mwangaza.data.retrofit.wallet.Utilities;
import ke.mwangaza.buy.mwangaza.data.retrofit.wallet.UtilitiesResponse;
import ke.mwangaza.buy.mwangaza.data.retrofit.wallet.WalletBalance;
import ke.mwangaza.buy.mwangaza.data.retrofit.wallet.WalletDeposits;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.POST;

public interface WalletInterface {

    @GET("wallet/balance")
    Call<WalletBalance> getWalletBalance();

    @GET("wallet/deposits")
    Call<List<WalletDeposits>> getWalletDeposits();

    @POST("wallet/share/funds")
    Call<ShareFundsRes> shareFunds(@Body ShareFunds shareFunds);

    @POST("wallet/send/money")
    Call<ErrorMessage> sendMoney(@Body SendMoney sendMoney);

    @POST("wallet/pay/bill")
    Call<ErrorMessage> payBill(@Body PayBill payBill);

    @POST("utilities/pay")
    Call<UtilitiesResponse> payUtility(@Body Utilities utilities);

    @GET("wallet/my/transactions")
    Call<List<MyTransactions>> getRecentTransactions();

    @GET("list/banks")
    Call<List<Banks>> getBanksFromServer();

    @GET("list/token/history")
    Call<List<PrePaidVendResponse>> getTokenHistory();

}
