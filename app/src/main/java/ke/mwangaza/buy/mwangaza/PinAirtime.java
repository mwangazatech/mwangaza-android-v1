package ke.mwangaza.buy.mwangaza;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.CardView;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.toptoche.searchablespinnerlibrary.SearchableSpinner;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import ke.mwangaza.buy.mwangaza.data.retrofit.airtime.AirtimeVoucherRequest;
import ke.mwangaza.buy.mwangaza.data.retrofit.airtime.AirtimeVoucherResponse;
import ke.mwangaza.buy.mwangaza.data.retrofit.airtime.Denomination;
import ke.mwangaza.buy.mwangaza.network.APIClient;
import ke.mwangaza.buy.mwangaza.network.AirtimeInterface;
import ke.mwangaza.buy.mwangaza.ui.main.ui.DashboardActivity;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class PinAirtime extends AppCompatActivity {

    @BindView(R.id.toolbar)
    Toolbar toolbar;
    @BindView(R.id.cancel)
    Button cancel;
    @BindView(R.id.safaricom)
    CardView safaricom;
    @BindView(R.id.airtel)
    CardView airtel;
    @BindView(R.id.telkom)
    CardView telkom;
    @BindView(R.id.pay)
    TextView pay;
    @BindView(R.id.spinner)
    SearchableSpinner spinner;
    @BindView(R.id.buyAirtime)
    Button buyAirtime;

    AirtimeInterface airtimeInterface;
    ProgressDialog progressDialog;

    Mwangaza app;

    String operator, amount;
    List<Denomination> denominations;
    private ArrayList<String> airtimeList;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_pin_airtime);
        ButterKnife.bind(this);

        toolbar.setTitle(R.string.titleTV);
        setSupportActionBar(toolbar);
        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setDisplayShowHomeEnabled(true);
        }

        app = (Mwangaza) getApplicationContext();
        airtimeInterface = APIClient.getClient(app.settings.getBearerToken()).create(AirtimeInterface.class);

        setDenominations(getDenominations());

        safaricom.setOnClickListener(v -> {
            pay.setText("Safaricom");
            operator = "safaricom";

        });

        airtel.setOnClickListener(v -> {
            pay.setText("Airtel");
            operator = "airtel";
        });

        telkom.setOnClickListener(v -> {
            pay.setText("Telkom");
            operator = "telcom";
        });

        spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> arg0, View arg1, int position, long id) {
                amount = String.valueOf(denominations.get(position).getAmount());
            }

            @Override
            public void onNothingSelected(AdapterView<?> arg0) {
                // TODO Auto-generated method stub
            }
        });

        buyAirtime.setOnClickListener(v -> validatePurchase());

        cancel.setOnClickListener(v -> {
            Intent intents = new Intent(getApplicationContext(), DashboardActivity.class);
            startActivity(intents);
        });
    }

    private void validatePurchase() {

        if (TextUtils.isEmpty(operator)) {
            Toast.makeText(this, "Please select Operator", Toast.LENGTH_SHORT).show();
            return;
        }

        if (TextUtils.isEmpty(amount)) {
            Toast.makeText(this, "Please select amount", Toast.LENGTH_SHORT).show();
            return;
        }

        purchaseAirtimeVoucher();

    }

    private void purchaseAirtimeVoucher() {

        progressDialog = new ProgressDialog(this);
        progressDialog.setIndeterminate(true);
        progressDialog.setMessage("Purchasing Airtime Voucher...");
        progressDialog.setCancelable(false);
        progressDialog.show();

        AirtimeVoucherRequest airtimeVoucherRequest = new AirtimeVoucherRequest(Integer.parseInt(amount), operator);

        Call<AirtimeVoucherResponse> call = airtimeInterface.buyAirtimeVoucher(airtimeVoucherRequest);
        call.enqueue(new Callback<AirtimeVoucherResponse>() {
            @Override
            public void onResponse(Call<AirtimeVoucherResponse> call, Response<AirtimeVoucherResponse> response) {
                if (response.isSuccessful()) {
                    progressDialog.dismiss();
                    AirtimeVoucherResponse airtimeVoucherResponse = response.body();
                    assert airtimeVoucherResponse != null;

                    Intent intent = new Intent(getApplicationContext(), PinAirtimeSuccess.class);
                    Bundle bundle = new Bundle();
                    bundle.putString("pin", airtimeVoucherResponse.getPincode());
                    bundle.putString("amount", String.valueOf(airtimeVoucherResponse.getAmount()));
                    bundle.putString("serial_number", String.valueOf(airtimeVoucherResponse.getRef()));
                    intent.putExtras(bundle);
                    startActivity(intent);

                } else {
                    progressDialog.dismiss();
                    assert response.body() != null;
                    JSONObject jsonObject = null;
                    try {
                        assert response.errorBody() != null;
                        jsonObject = new JSONObject(response.errorBody().string());

                        Intent intent = new Intent(getApplicationContext(), PinAirtimeFailure.class);
                        Bundle bundle = new Bundle();
                        bundle.putString("message", jsonObject.getString("message"));
                        intent.putExtras(bundle);
                        startActivity(intent);

                    } catch (JSONException e) {
                        e.printStackTrace();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
            }

            @Override
            public void onFailure(Call<AirtimeVoucherResponse> call, Throwable t) {
                progressDialog.dismiss();
                Intent intent = new Intent(getApplicationContext(), PinAirtimeFailure.class);
                Bundle bundle = new Bundle();
                bundle.putString("message", "Please try your request again...");
                intent.putExtras(intent);
                startActivity(intent);
            }
        });
    }

    private List<Denomination> getDenominations() {
        List<Denomination> listViewItems = new ArrayList<>();
        listViewItems.add(new Denomination(20));
        listViewItems.add(new Denomination(50));
        listViewItems.add(new Denomination(100));
        listViewItems.add(new Denomination(250));
        listViewItems.add(new Denomination(500));
        listViewItems.add(new Denomination(1000));
        return listViewItems;
    }

    public void setDenominations(final List<Denomination> denominationList) {
        denominations = denominationList;
        airtimeList = new ArrayList<>();
        for (Denomination denomination : denominationList) {
            airtimeList.add(String.valueOf(denomination.getAmount()));
        }

        ArrayAdapter<String> airtimeAdapter = new ArrayAdapter<String>(PinAirtime.this,
                R.layout.spinner_item_rg, airtimeList);

        airtimeAdapter.setDropDownViewResource(R.layout.spinner_item_rg2);
        spinner.setAdapter(airtimeAdapter);
    }

    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }
}
