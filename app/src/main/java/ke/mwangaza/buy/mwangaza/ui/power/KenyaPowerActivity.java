package ke.mwangaza.buy.mwangaza.ui.power;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.widget.Button;

import butterknife.BindView;
import butterknife.ButterKnife;
import ke.mwangaza.buy.mwangaza.R;
import ke.mwangaza.buy.mwangaza.ui.main.ui.DashboardActivity;

public class KenyaPowerActivity extends AppCompatActivity {

    @BindView(R.id.toolbar)
    Toolbar toolbar;
    @BindView(R.id.kplcPrePaid)
    Button kplcPrePaid;
    @BindView(R.id.kplcPostPaid)
    Button kplcPostPaid;
    @BindView(R.id.kplcCancel)
    Button kplcCancel;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_kenyapower);
        ButterKnife.bind(this);

        toolbar.setTitle(R.string.kp_title);
        setSupportActionBar(toolbar);
        if(getSupportActionBar() != null){
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setDisplayShowHomeEnabled(true);
        }

        kplcPrePaid.setOnClickListener(v -> {
            Intent intent = new Intent(getApplicationContext(), PrepaidActivity.class);
            startActivity(intent);
        });

        kplcPostPaid.setOnClickListener(v -> {
            Intent intent = new Intent(getApplicationContext(), PostPaidMeterActivity.class);
            startActivity(intent);
        });

        kplcCancel.setOnClickListener(v -> {
            Intent intent = new Intent(getApplicationContext(), DashboardActivity.class);
            startActivity(intent);
        });

    }
    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }
}
