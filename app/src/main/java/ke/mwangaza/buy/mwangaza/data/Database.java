package ke.mwangaza.buy.mwangaza.data;

import android.arch.persistence.room.Room;
import android.arch.persistence.room.RoomDatabase;
import android.arch.persistence.room.TypeConverters;
import android.content.Context;

import ke.mwangaza.buy.mwangaza.data.dao.UserDao;
import ke.mwangaza.buy.mwangaza.data.model.User;

@android.arch.persistence.room.Database(entities = {User.class}, version = 1)
@TypeConverters(DateConverter.class)
public abstract class Database extends RoomDatabase {

    private static Database INSTANCE;

    public static Database getDatabase(Context context) {
        if (INSTANCE == null) {
            INSTANCE =
                    Room.databaseBuilder(context.getApplicationContext(), Database.class, "mwangaza_db")
                            .allowMainThreadQueries()
                            .build();
        }
        return INSTANCE;
    }

    public abstract UserDao userDao();
}