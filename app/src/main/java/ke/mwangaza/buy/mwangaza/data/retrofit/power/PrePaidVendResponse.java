package ke.mwangaza.buy.mwangaza.data.retrofit.power;

public class PrePaidVendResponse {

    private String pre_paid_id;
    private String user_id;
    private String meter_number;
    private String customer_name;
    private String units;
    private String total_amount;
    private String token;

    public PrePaidVendResponse(String pre_paid_id, String user_id, String meter_number, String customer_name, String units, String total_amount, String token) {
        this.pre_paid_id = pre_paid_id;
        this.user_id = user_id;
        this.meter_number = meter_number;
        this.customer_name = customer_name;
        this.units = units;
        this.total_amount = total_amount;
        this.token = token;
    }

    public String getPre_paid_id() {
        return pre_paid_id;
    }

    public void setPre_paid_id(String pre_paid_id) {
        this.pre_paid_id = pre_paid_id;
    }

    public String getUser_id() {
        return user_id;
    }

    public void setUser_id(String user_id) {
        this.user_id = user_id;
    }

    public String getMeter_number() {
        return meter_number;
    }

    public void setMeter_number(String meter_number) {
        this.meter_number = meter_number;
    }

    public String getCustomer_name() {
        return customer_name;
    }

    public void setCustomer_name(String customer_name) {
        this.customer_name = customer_name;
    }

    public String getUnits() {
        return units;
    }

    public void setUnits(String units) {
        this.units = units;
    }

    public String getTotal_amount() {
        return total_amount;
    }

    public void setTotal_amount(String total_amount) {
        this.total_amount = total_amount;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }
}
