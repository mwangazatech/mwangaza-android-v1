package ke.mwangaza.buy.mwangaza.data.retrofit.airtime;

public class AirtimeRequest {

    private String phone_number;
    private int amount;
    private String operator;

    public AirtimeRequest(String phone_number, int amount, String operator) {
        this.phone_number = phone_number;
        this.amount = amount;
        this.operator = operator;
    }
}
