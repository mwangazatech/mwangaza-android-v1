package ke.mwangaza.buy.mwangaza.ui.power;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;

import butterknife.BindView;
import butterknife.ButterKnife;
import ke.mwangaza.buy.mwangaza.Mwangaza;
import ke.mwangaza.buy.mwangaza.R;
import ke.mwangaza.buy.mwangaza.data.retrofit.power.PrePaidVend;
import ke.mwangaza.buy.mwangaza.data.retrofit.power.PrePaidVendResponse;
import ke.mwangaza.buy.mwangaza.network.APIClient;
import ke.mwangaza.buy.mwangaza.network.PowerInterface;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class TokenAmountActivity extends AppCompatActivity {

    @BindView(R.id.toolbar)
    Toolbar toolbar;
    @BindView(R.id.buytokens)
    Button buyTokens;
    @BindView(R.id.customer)
    TextView customer;
    @BindView(R.id.meter)
    TextView meter;
    @BindView(R.id.kplcPrePaidTokenAmount)
    EditText kplcPrePaidTokenAmount;

    Mwangaza app;
    PowerInterface powerInterface;
    ProgressDialog progressDialog;

    String pre_paid_id, meter_number, customer_name, amount;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_tokenamount);
        ButterKnife.bind(this);

        toolbar.setTitle(R.string.buy_token);
        setSupportActionBar(toolbar);
        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setDisplayShowHomeEnabled(true);
        }

        app = (Mwangaza) getApplicationContext();
        powerInterface = APIClient.getClient(app.settings.getBearerToken()).create(PowerInterface.class);

        Intent intent = getIntent();
        Bundle bundle = intent.getExtras();

        assert bundle != null;
        pre_paid_id = bundle.getString("pre_paid_id");
        meter_number = bundle.getString("meter_number");
        customer_name = bundle.getString("customer_name");

        customer.setText(String.format("Customer Name: %s", customer_name));
        meter.setText(String.format("Meter Number: %s", meter_number));

        powerInterface = APIClient.getClient(app.settings.getBearerToken()).create(PowerInterface.class);

        buyTokens.setOnClickListener(v -> verifyMeterDetails());

    }

    private void verifyMeterDetails() {

        amount = kplcPrePaidTokenAmount.getText().toString().trim();

        if (TextUtils.isEmpty(amount)) {
            kplcPrePaidTokenAmount.setError("Kindly Enter Token Amount");
            return;
        }

        vendMeterDetails();
    }

    private void vendMeterDetails() {

        progressDialog = new ProgressDialog(this);
        progressDialog.setIndeterminate(true);
        progressDialog.setMessage("Purchasing Prepaid Tokens...");
        progressDialog.setCancelable(false);
        progressDialog.show();

        PrePaidVend prePaidVend = new PrePaidVend(pre_paid_id, meter_number, customer_name, Integer.parseInt(amount));

        Call<PrePaidVendResponse> call = powerInterface.vendPrepaid(prePaidVend);
        call.enqueue(new Callback<PrePaidVendResponse>() {
            @Override
            public void onResponse(@NonNull Call<PrePaidVendResponse> call, @NonNull Response<PrePaidVendResponse> response) {
                if (response.isSuccessful()) {
                    progressDialog.dismiss();
                    PrePaidVendResponse prePaidVendResponse = response.body();
                    assert prePaidVendResponse != null;

                    Intent intent = new Intent(getApplicationContext(), PrepaidSuccessActivity.class);
                    Bundle bundle = new Bundle();
                    bundle.putString("customer_name", prePaidVendResponse.getCustomer_name());
                    bundle.putString("meter_number", prePaidVendResponse.getMeter_number());
                    bundle.putString("amount", prePaidVendResponse.getTotal_amount());
                    bundle.putString("units", prePaidVendResponse.getUnits());
                    bundle.putString("token", prePaidVendResponse.getToken());
                    intent.putExtras(bundle);
                    startActivity(intent);

                } else {
                    progressDialog.dismiss();
                    assert response.body() != null;
                    JSONObject jsonObject = null;
                    try {
                        assert response.errorBody() != null;
                        jsonObject = new JSONObject(response.errorBody().string());

                        Intent intent = new Intent(getApplicationContext(), PostFailureActivity.class);
                        Bundle bundle = new Bundle();
                        bundle.putString("message", jsonObject.getString("message"));
                        intent.putExtras(bundle);
                        startActivity(intent);

                    } catch (JSONException e) {
                        e.printStackTrace();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
            }

            @Override
            public void onFailure(@NonNull Call<PrePaidVendResponse> call, @NonNull Throwable t) {
                progressDialog.dismiss();
                Intent intent = new Intent(getApplicationContext(), PostFailureActivity.class);
                Bundle bundle = new Bundle();
                bundle.putString("message", "Please try your request again...");
                intent.putExtras(intent);
                startActivity(intent);
            }
        });
    }

    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }
}
