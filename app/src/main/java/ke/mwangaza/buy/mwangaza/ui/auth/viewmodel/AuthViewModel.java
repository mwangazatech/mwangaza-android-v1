package ke.mwangaza.buy.mwangaza.ui.auth.viewmodel;

import android.app.Application;
import android.arch.lifecycle.AndroidViewModel;
import android.os.AsyncTask;

import ke.mwangaza.buy.mwangaza.data.Database;
import ke.mwangaza.buy.mwangaza.data.model.User;

public class AuthViewModel extends AndroidViewModel {

    private Database database;

    public AuthViewModel(Application application) {
        super(application);

        database = Database.getDatabase(this.getApplication());

    }

    public void addUser(final User user) {
        new AuthViewModel.addAsyncTask(database).execute(user);
    }

    private static class addAsyncTask extends AsyncTask<User, Void, Void> {

        private Database db;

        addAsyncTask(Database database) {
            db = database;
        }

        @Override
        protected Void doInBackground(final User... params) {
            db.userDao().addUser(params[0]);
            return null;
        }

    }
}
