package ke.mwangaza.buy.mwangaza.ui.funds;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.widget.Button;
import android.widget.TextView;

import butterknife.BindView;
import butterknife.ButterKnife;
import ke.mwangaza.buy.mwangaza.R;
import ke.mwangaza.buy.mwangaza.ui.main.ui.DashboardActivity;

public class SuccessShareFundsActivity extends AppCompatActivity {

    @BindView(R.id.toolbar)
    Toolbar toolbar;
    @BindView(R.id.cancel)
    Button Cancel;
    @BindView(R.id.mobile)
    TextView mobile;
    @BindView(R.id.mwaname)
    TextView mwaname;
    @BindView(R.id.paid)
    TextView paid;
    @BindView(R.id.transaction)
    TextView transaction;
    @BindView(R.id.share)
    Button share;

    String name, phone_number, amount, balance;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_success_share_funds);
        ButterKnife.bind(this);
        toolbar.setTitle(R.string.share_funds);
        setSupportActionBar(toolbar);
        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setDisplayShowHomeEnabled(true);
        }

        Intent intent = getIntent();
        Bundle bundle = intent.getExtras();

        assert bundle != null;
        name = bundle.getString("name");
        amount = bundle.getString("amount");
        phone_number = bundle.getString("phone_number");
        balance = bundle.getString("balance");

        mwaname.setText(String.format("Mwangaza Name : %s", name));
        mobile.setText(String.format("Mobile Number : %s", phone_number));
        paid.setText(String.format("Shared Amount : %s", amount));
        transaction.setText(String.format("Wallet Balance : %s", balance));

        share.setOnClickListener(v -> {
            Intent myIntent = new Intent(Intent.ACTION_SEND);
            myIntent.setType("text/plain");
            String shareBody = "MWANGAZA SHARE FUNDS"+"\n"+"=========================="+"\n"+"Mwangaza Name: "+ name+"\n"+"Mobile Number: "+ phone_number+"\n"+"Amount Paid: "+ amount +"\n"+"Wallet Balance: "+ balance;
            myIntent.putExtra(Intent.EXTRA_TEXT, shareBody);
            startActivity(Intent.createChooser(myIntent, "Share via..."));
        });


        Cancel.setOnClickListener(v -> {
            Intent cancel = new Intent(getApplicationContext(), DashboardActivity.class);
            startActivity(cancel);
        });
    }

    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }


}
