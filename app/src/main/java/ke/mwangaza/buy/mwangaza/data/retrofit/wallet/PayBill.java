package ke.mwangaza.buy.mwangaza.data.retrofit.wallet;

public class PayBill {
    private String user_id;
    private int amount;
    private String account_number;
    private String paybill;

    public PayBill(String user_id, int amount, String account_number, String paybill) {
        this.user_id = user_id;
        this.amount = amount;
        this.account_number = account_number;
        this.paybill = paybill;
    }
}
