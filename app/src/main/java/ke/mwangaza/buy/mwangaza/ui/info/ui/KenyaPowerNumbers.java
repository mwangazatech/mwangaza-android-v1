package ke.mwangaza.buy.mwangaza.ui.info.ui;

import android.annotation.SuppressLint;
import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.Toast;

import java.util.List;

import ke.mwangaza.buy.mwangaza.Mwangaza;
import ke.mwangaza.buy.mwangaza.R;
import ke.mwangaza.buy.mwangaza.data.retrofit.power.PrePaidMeters;
import ke.mwangaza.buy.mwangaza.network.APIClient;
import ke.mwangaza.buy.mwangaza.network.PowerInterface;
import ke.mwangaza.buy.mwangaza.ui.info.adapter.CustomItemClickListener;
import ke.mwangaza.buy.mwangaza.ui.info.adapter.KenyaPowerNumbersAdapter;
import ke.mwangaza.buy.mwangaza.ui.main.ui.DashboardActivity;
import ke.mwangaza.buy.mwangaza.ui.power.PrepaidActivity;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

@SuppressLint("ValidFragment")
public class KenyaPowerNumbers extends Fragment {

    View view;
    Mwangaza app;
    PowerInterface powerInterface;

    ProgressDialog progressDialog;
    List<PrePaidMeters> prePaidMetersList;
    RecyclerView recyclerView;
    ImageView brokenImageLink;

    KenyaPowerNumbersAdapter kenyaPowerNumbersAdapter;

    public KenyaPowerNumbers(Mwangaza app) {
        this.app = app;
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        view = inflater.inflate(R.layout.fragment_kenya_power_numbers, container, false);

        powerInterface = APIClient.getClient(app.settings.getBearerToken()).create(PowerInterface.class);

        recyclerView = view.findViewById(R.id.recyclerView);
        brokenImageLink = view.findViewById(R.id.brokenImageLink);

        loadPreviousNumbers();

        return view;
    }

    private void loadPreviousNumbers() {

        progressDialog = new ProgressDialog(getActivity());
        progressDialog.setIndeterminate(true);
        progressDialog.setMessage("Fetching Recent Meter Numbers...");
        progressDialog.setCancelable(false);
        progressDialog.show();

        Call<List<PrePaidMeters>> call = powerInterface.getRecentPrePaidMeters();
        call.enqueue(new Callback<List<PrePaidMeters>>() {
            @Override
            public void onResponse(@NonNull Call<List<PrePaidMeters>> call, @NonNull Response<List<PrePaidMeters>> response) {
                progressDialog.dismiss();
                if (response.isSuccessful()) {
                    prePaidMetersList = response.body();
                    assert prePaidMetersList != null;

                    showTransactions(prePaidMetersList);
                }
            }

            @Override
            public void onFailure(@NonNull Call<List<PrePaidMeters>> call, @NonNull Throwable t) {
                progressDialog.dismiss();
                Toast.makeText(getActivity(), "Failed to load recent meters...", Toast.LENGTH_SHORT).show();
                Intent intent = new Intent(getActivity(), DashboardActivity.class);
                startActivity(intent);
            }
        });

    }

    private void showTransactions(List<PrePaidMeters> myTransactions) {

        if (myTransactions == null || myTransactions.isEmpty()) {
            brokenImageLink.setVisibility(View.VISIBLE);
            recyclerView.setVisibility(View.INVISIBLE);
        } else {

            kenyaPowerNumbersAdapter = new KenyaPowerNumbersAdapter(getActivity(), myTransactions, new CustomItemClickListener() {
                @Override
                public void onItemClick(View v, int position) {
                    Intent intent = new Intent(getActivity(), PrepaidActivity.class);
                    Bundle bundle = new Bundle();
                    bundle.putString("meter_number", myTransactions.get(position).getAccount_number());
                    intent.putExtras(bundle);
                    startActivity(intent);
                }
            });

            LinearLayoutManager mLayoutManager =
                    new LinearLayoutManager(getActivity(), LinearLayoutManager.VERTICAL, false);
            recyclerView.setLayoutManager(mLayoutManager);
            recyclerView.setAdapter(kenyaPowerNumbersAdapter);
        }
    }
}
