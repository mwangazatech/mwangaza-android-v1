package ke.mwangaza.buy.mwangaza.data.retrofit.wallet;

public class UtilitiesResponse {

    private String utility_id;
    private String user_id;
    private String account;
    private int amount;
    private String biller_name;
    private String merchant_reference;
    private String phone;
    private boolean status;
    private String created_at;
    private String created_by;
    private boolean active;

    public UtilitiesResponse(String utility_id, String user_id, String account, int amount, String biller_name, String merchant_reference, String phone, boolean status, String created_at, String created_by, boolean active) {
        this.utility_id = utility_id;
        this.user_id = user_id;
        this.account = account;
        this.amount = amount;
        this.biller_name = biller_name;
        this.merchant_reference = merchant_reference;
        this.phone = phone;
        this.status = status;
        this.created_at = created_at;
        this.created_by = created_by;
        this.active = active;
    }

    public String getUtility_id() {
        return utility_id;
    }

    public void setUtility_id(String utility_id) {
        this.utility_id = utility_id;
    }

    public String getUser_id() {
        return user_id;
    }

    public void setUser_id(String user_id) {
        this.user_id = user_id;
    }

    public String getAccount() {
        return account;
    }

    public void setAccount(String account) {
        this.account = account;
    }

    public int getAmount() {
        return amount;
    }

    public void setAmount(int amount) {
        this.amount = amount;
    }

    public String getBiller_name() {
        return biller_name;
    }

    public void setBiller_name(String biller_name) {
        this.biller_name = biller_name;
    }

    public String getMerchant_reference() {
        return merchant_reference;
    }

    public void setMerchant_reference(String merchant_reference) {
        this.merchant_reference = merchant_reference;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public boolean isStatus() {
        return status;
    }

    public void setStatus(boolean status) {
        this.status = status;
    }

    public String getCreated_at() {
        return created_at;
    }

    public void setCreated_at(String created_at) {
        this.created_at = created_at;
    }

    public String getCreated_by() {
        return created_by;
    }

    public void setCreated_by(String created_by) {
        this.created_by = created_by;
    }

    public boolean isActive() {
        return active;
    }

    public void setActive(boolean active) {
        this.active = active;
    }
}

