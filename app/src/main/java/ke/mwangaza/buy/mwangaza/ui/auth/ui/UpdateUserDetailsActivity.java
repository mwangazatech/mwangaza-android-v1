package ke.mwangaza.buy.mwangaza.ui.auth.ui;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import butterknife.BindView;
import butterknife.ButterKnife;
import ke.mwangaza.buy.mwangaza.Mwangaza;
import ke.mwangaza.buy.mwangaza.R;
import ke.mwangaza.buy.mwangaza.data.retrofit.common.ErrorMessage;
import ke.mwangaza.buy.mwangaza.data.retrofit.user.UpdateUser;
import ke.mwangaza.buy.mwangaza.network.APIClient;
import ke.mwangaza.buy.mwangaza.network.AuthInterface;
import ke.mwangaza.buy.mwangaza.ui.main.ui.DashboardActivity;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class UpdateUserDetailsActivity extends AppCompatActivity {

    @BindView(R.id.updateUserDetails)
    Button updateUserDetails;
    @BindView(R.id.updateUserFullName)
    EditText updateUserFullName;
    @BindView(R.id.currentUserFullName)
    EditText currentUserFullName;

    Mwangaza app;
    AuthInterface authInterface;
    ProgressDialog progressDialog;

    String name;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_update_details);
        ButterKnife.bind(this);

        app = (Mwangaza) getApplicationContext();
        authInterface = APIClient.getClient(app.settings.getBearerToken()).create(AuthInterface.class);
        currentUserFullName.setText(app.settings.getName());

        updateUserDetails.setOnClickListener(v -> validateUserInfo());
    }

    private void validateUserInfo() {

        name = updateUserFullName.getText().toString().trim();

        if (TextUtils.isEmpty(name)) {
            updateUserFullName.setError("Kindly Enter Full Name");
        }

        updateUserInfo();
    }

    private void updateUserInfo() {

        progressDialog = new ProgressDialog(this);
        progressDialog.setIndeterminate(true);
        progressDialog.setMessage("Updating User Info...");
        progressDialog.setCancelable(false);
        progressDialog.show();

        UpdateUser updateUser = new UpdateUser(name);

        Call<ErrorMessage> call = authInterface.updateUserInfo(updateUser);
        call.enqueue(new Callback<ErrorMessage>() {
            @Override
            public void onResponse(@NonNull Call<ErrorMessage> call, @NonNull Response<ErrorMessage> response) {
                progressDialog.dismiss();
                //Success
                if (response.isSuccessful()) {
                    app.settings.setName(name);
                    Toast.makeText(UpdateUserDetailsActivity.this, "User Details Update Successful...", Toast.LENGTH_SHORT).show();
                    Intent intent = new Intent(getApplicationContext(), DashboardActivity.class);
                    startActivity(intent);
                }
            }

            @Override
            public void onFailure(@NonNull Call<ErrorMessage> call, @NonNull Throwable t) {
                progressDialog.dismiss();
                //Failure
                Toast.makeText(UpdateUserDetailsActivity.this, "An error occured during update...", Toast.LENGTH_SHORT).show();
                Intent intent = new Intent(getApplicationContext(), DashboardActivity.class);
                startActivity(intent);
            }
        });

    }
}
