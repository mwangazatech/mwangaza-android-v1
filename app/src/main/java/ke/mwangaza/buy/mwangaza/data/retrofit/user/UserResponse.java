package ke.mwangaza.buy.mwangaza.data.retrofit.user;

public class UserResponse {
    private String user_id;
    private String name;
    private String phone_number;
    private String device_id;
    private String facebook_token;
    private boolean active;
    private String token;

    public UserResponse(String user_id, String name, String phone_number, String device_id, boolean active, String token, String facebook_token) {
        this.user_id = user_id;
        this.name = name;
        this.phone_number = phone_number;
        this.device_id = device_id;
        this.facebook_token = facebook_token;
        this.active = active;
        this.token = token;
    }

    public String getUser_id() {
        return user_id;
    }

    public void setUser_id(String user_id) {
        this.user_id = user_id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPhone_number() {
        return phone_number;
    }

    public void setPhone_number(String phone_number) {
        this.phone_number = phone_number;
    }

    public String getDevice_id() {
        return device_id;
    }

    public void setDevice_id(String device_id) {
        this.device_id = device_id;
    }

    public boolean isActive() {
        return active;
    }

    public void setActive(boolean active) {
        this.active = active;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public String getFacebook_token() {
        return facebook_token;
    }

    public void setFacebook_token(String facebook_token) {
        this.facebook_token = facebook_token;
    }
}

