package ke.mwangaza.buy.mwangaza.ui.funds;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.widget.Button;
import android.content.Intent;

import butterknife.BindView;
import butterknife.ButterKnife;
import ke.mwangaza.buy.mwangaza.R;
import ke.mwangaza.buy.mwangaza.ui.main.ui.DashboardActivity;

public class SendMoneyActivity extends AppCompatActivity {

    @BindView(R.id.toolbar)
    Toolbar toolbar;
    @BindView(R.id.cancel)
    Button cancel;
    @BindView(R.id.mpesa)
    Button mpesa;
    @BindView(R.id.bank)
    Button bank;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_send_money);
        ButterKnife.bind(this);

        toolbar.setTitle(R.string.send);
        setSupportActionBar(toolbar);
        if(getSupportActionBar() != null){
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setDisplayShowHomeEnabled(true);
        }

        mpesa.setOnClickListener(v -> {
            Intent intent = new Intent(getApplicationContext(), MpesaActivity.class);
            startActivity(intent);
        });

        bank.setOnClickListener(v -> {
            Intent intent = new Intent(getApplicationContext(), BankPaymentActivity.class);
            startActivity(intent);
        });


        cancel.setOnClickListener(v -> {
            Intent intent = new Intent(getApplicationContext(), DashboardActivity.class);
            startActivity(intent);
        });
    }
    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }
}
