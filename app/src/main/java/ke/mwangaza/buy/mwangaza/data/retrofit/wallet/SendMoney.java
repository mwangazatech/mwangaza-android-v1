package ke.mwangaza.buy.mwangaza.data.retrofit.wallet;

public class SendMoney {

    private String user_id;
    private int amount;
    private String account_number;

    public SendMoney(String user_id, int amount, String account_number) {
        this.user_id = user_id;
        this.amount = amount;
        this.account_number = account_number;
    }
}
