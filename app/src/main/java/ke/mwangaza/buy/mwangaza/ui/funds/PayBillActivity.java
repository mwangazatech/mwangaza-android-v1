package ke.mwangaza.buy.mwangaza.ui.funds;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;

import butterknife.BindView;
import butterknife.ButterKnife;
import ke.mwangaza.buy.mwangaza.Mwangaza;
import ke.mwangaza.buy.mwangaza.R;
import ke.mwangaza.buy.mwangaza.data.retrofit.common.ErrorMessage;
import ke.mwangaza.buy.mwangaza.data.retrofit.wallet.PayBill;
import ke.mwangaza.buy.mwangaza.network.APIClient;
import ke.mwangaza.buy.mwangaza.network.WalletInterface;
import ke.mwangaza.buy.mwangaza.ui.main.ui.DashboardActivity;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class PayBillActivity extends AppCompatActivity {

    @BindView(R.id.toolbar)
    Toolbar toolbar;
    @BindView(R.id.cancel)
    Button cancel;

    @BindView(R.id.paybilltext)
    EditText paybilltext;
    @BindView(R.id.accountreference)
    EditText accountreference;
    @BindView(R.id.amountext)
    EditText amountext;
    @BindView(R.id.choice_tokens)
    Button choice_tokens;
    @BindView(R.id.cost)
    TextView cost;

    String paybill, account, amount;

    Mwangaza app;
    WalletInterface walletInterface;
    ProgressDialog progressDialog;
    int calculatableAmount;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_pay_bill);
        ButterKnife.bind(this);

        toolbar.setTitle(R.string.paybill);
        setSupportActionBar(toolbar);
        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setDisplayShowHomeEnabled(true);
        }

        amountext.addTextChangedListener(new TextWatcher() {
            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
            }

            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

                // TODO Auto-generated method stub
            }

            @Override
            public void afterTextChanged(Editable s) {
                if (!TextUtils.isEmpty(s)) {
                    calculatableAmount = Integer.valueOf(amountext.getText().toString().trim());
                    int i = calculateB2BTransactionCost(calculatableAmount);
                    cost.setText(String.format("Transaction Cost is: %s", i));
                }
            }
        });

        app = (Mwangaza) getApplicationContext();
        walletInterface = APIClient.getClient(app.settings.getBearerToken()).create(WalletInterface.class);

        cancel.setOnClickListener(v -> {
            Intent intent = new Intent(getApplicationContext(), DashboardActivity.class);
            startActivity(intent);
        });

        choice_tokens.setOnClickListener(v -> verifyDetails());
    }

    private void verifyDetails() {
        paybill = paybilltext.getText().toString().trim();
        account = accountreference.getText().toString().trim();
        amount = amountext.getText().toString().trim();

        if (TextUtils.isEmpty(paybill)) {
            paybilltext.setError("Kindly Enter Paybill Number");
            return;
        }

        if (TextUtils.isEmpty(amount)) {
            accountreference.setError("Kindly Enter Account Reference");
            return;
        }

        if (TextUtils.isEmpty(account)) {
            amountext.setError("Kindly Enter Amount");
            return;
        }

        if (paybill.equals("955100") || paybill.equals("955700") || paybill.equals("290020") || paybill.equals("997270") || paybill.equals("880185") || paybill.equals("290290") || paybill.equals("290027") || paybill.equals("202202") || paybill.equals("290059") || paybill.equals("290028") || paybill.equals("290067") || paybill.equals("290030") || paybill.equals("850700") || paybill.equals("295525") || paybill.equals("850704") || paybill.equals("29007") || paybill.equals("775678")) {
            Toast.makeText(this, "Betting Paybills are not supported!", Toast.LENGTH_SHORT).show();
            return;
        }

        sendMoney();
    }

    private void sendMoney() {

        progressDialog = new ProgressDialog(this);
        progressDialog.setIndeterminate(true);
        progressDialog.setMessage("Sending Money...");
        progressDialog.setCancelable(false);
        progressDialog.show();

        PayBill payBill = new PayBill(app.settings.getUserId(), Integer.parseInt(amount), account, paybill);

        Call<ErrorMessage> call = walletInterface.payBill(payBill);
        call.enqueue(new Callback<ErrorMessage>() {
            @Override
            public void onResponse(@NonNull Call<ErrorMessage> call, @NonNull Response<ErrorMessage> response) {
                if (response.isSuccessful()) {
                    progressDialog.dismiss();

                    ErrorMessage errorMessage = response.body();
                    assert errorMessage != null;

                    Intent intent = new Intent(getApplicationContext(), PaybillSuccessActivity.class);
                    Bundle bundle = new Bundle();
                    bundle.putString("message", errorMessage.getMessage());
                    bundle.putString("trans_ref", errorMessage.getTrans_ref());
                    intent.putExtras(bundle);
                    startActivity(intent);

                } else {
                    progressDialog.dismiss();
                    assert response.body() != null;
                    JSONObject jsonObject = null;
                    try {
                        assert response.errorBody() != null;
                        jsonObject = new JSONObject(response.errorBody().string());

                        Intent intent = new Intent(getApplicationContext(), PaybillFailureActivity.class);
                        Bundle bundle = new Bundle();
                        bundle.putString("message", jsonObject.getString("message"));
                        intent.putExtras(bundle);
                        startActivity(intent);

                    } catch (JSONException e) {
                        e.printStackTrace();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
            }

            @Override
            public void onFailure(@NonNull Call<ErrorMessage> call, @NonNull Throwable t) {
                progressDialog.dismiss();
                Intent intent = new Intent(getApplicationContext(), PaybillFailureActivity.class);
                Bundle bundle = new Bundle();
                bundle.putString("message", "Please try your request again...");
                intent.putExtras(intent);
                startActivity(intent);
            }
        });
    }

    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }

    private int calculateB2BTransactionCost(int amount) {
        if (amount <= 100) {
            return 10;
        } else if (amount >= 101 && amount <= 500) {
            return 15;
        } else if (amount >= 501 && amount <= 1000) {
            return 20;
        } else if (amount >= 1001 && amount <= 1500) {
            return 25;
        } else if (amount >= 1501 && amount <= 2500) {
            return 30;
        } else if (amount >= 2501 && amount <= 3500) {
            return 35;
        } else if (amount >= 3501 && amount <= 5000) {
            return 50;
        } else if (amount >= 5001 && amount <= 7500) {
            return 60;
        } else if (amount >= 7501 && amount <= 10000) {
            return 60;
        } else if (amount >= 10001 && amount <= 15000) {
            return 85;
        } else if (amount >= 15001 && amount <= 35000) {
            return 100;
        } else if (amount >= 35001 && amount <= 50000) {
            return 190;
        }
        return 190;
    }
}
