package ke.mwangaza.buy.mwangaza.network;

import ke.mwangaza.buy.mwangaza.data.retrofit.common.ErrorMessage;
import ke.mwangaza.buy.mwangaza.data.retrofit.user.Firebase;
import ke.mwangaza.buy.mwangaza.data.retrofit.user.LoginUser;
import ke.mwangaza.buy.mwangaza.data.retrofit.user.RegisterUser;
import ke.mwangaza.buy.mwangaza.data.retrofit.user.ResetOTP;
import ke.mwangaza.buy.mwangaza.data.retrofit.user.ResetOTPRes;
import ke.mwangaza.buy.mwangaza.data.retrofit.user.SetPin;
import ke.mwangaza.buy.mwangaza.data.retrofit.user.UpdateUser;
import ke.mwangaza.buy.mwangaza.data.retrofit.user.UserResponse;
import ke.mwangaza.buy.mwangaza.data.retrofit.user.VerifyOTP;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.POST;

public interface AuthInterface {

    @POST("user/login")
    Call<UserResponse> loginUser(@Body LoginUser loginUser);

    @POST("user/register")
    Call<UserResponse> registerUser(@Body RegisterUser registerUser);

    @POST("user/fcm/token")
    Call<ErrorMessage> updateFCMToken(@Body Firebase firebase);

    @POST("user/update/info")
    Call<ErrorMessage> updateUserInfo(@Body UpdateUser updateUser);

    @POST("user/pin/reset")
    Call<ResetOTPRes> requestForOTP(@Body ResetOTP resetOTP);

    @POST("user/otp/verify")
    Call<ErrorMessage> verifyOTP(@Body VerifyOTP verifyOtp);

    @POST("user/pin/update")
    Call<UserResponse> setNewPin(@Body SetPin setPin);

}
