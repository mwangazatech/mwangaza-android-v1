package ke.mwangaza.buy.mwangaza.ui.auth.ui;

import android.Manifest;
import android.annotation.TargetApi;
import android.app.ProgressDialog;
import android.arch.lifecycle.ViewModelProviders;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.telephony.TelephonyManager;
import android.text.InputType;
import android.text.TextUtils;
import android.text.method.PasswordTransformationMethod;
import android.util.Log;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.facebook.accountkit.Account;
import com.facebook.accountkit.AccountKit;
import com.facebook.accountkit.AccountKitCallback;
import com.facebook.accountkit.AccountKitError;
import com.facebook.accountkit.PhoneNumber;

import butterknife.BindView;
import butterknife.ButterKnife;
import ke.mwangaza.buy.mwangaza.Mwangaza;
import ke.mwangaza.buy.mwangaza.R;
import ke.mwangaza.buy.mwangaza.data.model.User;
import ke.mwangaza.buy.mwangaza.data.retrofit.user.RegisterUser;
import ke.mwangaza.buy.mwangaza.data.retrofit.user.UserResponse;
import ke.mwangaza.buy.mwangaza.network.APIClient;
import ke.mwangaza.buy.mwangaza.network.AuthInterface;
import ke.mwangaza.buy.mwangaza.ui.auth.viewmodel.AuthViewModel;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class RegistrationActivity extends AppCompatActivity {

    @BindView(R.id.registerName)
    EditText registerName;
    @BindView(R.id.registerPhone)
    EditText registerPhone;
    @BindView(R.id.accountID)
    EditText accountID;
    @BindView(R.id.subscriberID)
    EditText subscriberID;
    @BindView(R.id.imeiID)
    EditText imeiID;
    @BindView(R.id.registerPin)
    EditText registerPin;
    @BindView(R.id.buttonRegister)
    Button buttonRegister;

    AuthInterface authInterface;
    ProgressDialog progressDialog;
    String name, phone_number, facebook_token, pin;

    AuthViewModel authViewModel;
    Mwangaza app;

    private static final int PERMISSION_REQUEST_CODE = 1;
    String TAG = "PhoneActivityTAG";
    String wantPermission = Manifest.permission.READ_PHONE_STATE;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_registration);
        ButterKnife.bind(this);
        app = (Mwangaza) getApplication();

        registerPin.setTransformationMethod(new PasswordTransformationMethod());
        registerPin.setInputType(InputType.TYPE_NUMBER_VARIATION_PASSWORD);

        authViewModel = ViewModelProviders.of(this).get(AuthViewModel.class);

        authInterface = APIClient.getClient("").create(AuthInterface.class);

        buttonRegister.setOnClickListener(v -> verifyUserDetails());

        if (!checkPermission(wantPermission)) {
            requestPermission(wantPermission);
        } else {
            Log.d(TAG, "Phone number: " + getPhone());

            subscriberID.setText(getPhone());

        }

        showAccountInfo();
    }

    @TargetApi(Build.VERSION_CODES.O)
    private String getPhone() {
        TelephonyManager phoneMgr = (TelephonyManager) getSystemService(Context.TELEPHONY_SERVICE);
        if (ActivityCompat.checkSelfPermission(RegistrationActivity.this, wantPermission) != PackageManager.PERMISSION_GRANTED) {
            return "";

        }
        return "";
//        String getSimSerialNumber = phoneMgr.getSimSerialNumber();
//        String getSimNumber = phoneMgr.getLine1Number();
//        String getSubscriberID = phoneMgr.getSubscriberId();
        //return phoneMgr.getSimSerialNumber();
        // return phoneMgr.getImei();
        //return phoneMgr.getSubscriberId();
    }


    private void requestPermission(String permission) {
        if (ActivityCompat.shouldShowRequestPermissionRationale(RegistrationActivity.this, permission)) {
            Toast.makeText(RegistrationActivity.this, "Please allow for additional functionality with Mwangaza.", Toast.LENGTH_LONG).show();
        }
        ActivityCompat.requestPermissions(RegistrationActivity.this, new String[]{permission}, PERMISSION_REQUEST_CODE);
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        switch (requestCode) {
            case PERMISSION_REQUEST_CODE:
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    Log.d(TAG, "Phone number: " + getPhone());
                } else {
                    Toast.makeText(RegistrationActivity.this, "Permission Denied. We can't continue.", Toast.LENGTH_LONG).show();
                }
                break;
        }
    }

    private boolean checkPermission(String permission) {
        if (Build.VERSION.SDK_INT >= 23) {
            int result = ContextCompat.checkSelfPermission(RegistrationActivity.this, permission);
            return result == PackageManager.PERMISSION_GRANTED;
        } else {
            return true;
        }
    }



    private void showAccountInfo() {
        AccountKit.getCurrentAccount(new AccountKitCallback<Account>() {
            @Override
            public void onSuccess(final Account account) {
                String accountId = account.getId();
                PhoneNumber phoneNumber = account.getPhoneNumber();
                String phoneNumberString;
                if (phoneNumber != null) {
                    phoneNumberString = phoneNumber.toString();

                    accountID.setText(accountId);
                    registerPhone.setText(phoneNumberString);
                    registerPhone.setEnabled(false);

                    phone_number = phoneNumberString;
                    facebook_token = accountId;

                }
            }

            @Override
            public void onError(final AccountKitError error) {
                Toast.makeText(RegistrationActivity.this, error.toString(), Toast.LENGTH_LONG).show();
            }
        });
    }


    private void verifyUserDetails() {
        name = registerName.getText().toString().trim();
        pin = registerPin.getText().toString().trim();


        if (TextUtils.isEmpty(name)) {
            registerName.setError("Enter Your Full Names");
            return;
        }

        //if (TextUtils.isEmpty(phone_number)) {
        //Toast.makeText(getApplicationContext(), "Enter Your Phone Number", Toast.LENGTH_SHORT).show();
        // return;
        //}

        if (TextUtils.isEmpty(pin)) {
            registerPin.setError("Enter Your Mwangaza Pin");
            return;
        }

        registerUser();
    }

    private void registerUser() {

        progressDialog = new ProgressDialog(this);
        progressDialog.setIndeterminate(true);
        progressDialog.setMessage("Signing Up...");
        progressDialog.setCancelable(false);
        progressDialog.show();

        RegisterUser registerUser = new RegisterUser(name, phone_number, pin, facebook_token);

        Call<UserResponse> call = authInterface.registerUser(registerUser);
        call.enqueue(new Callback<UserResponse>() {
            @Override
            public void onResponse(@NonNull Call<UserResponse> call, @NonNull Response<UserResponse> response) {
                if (response.isSuccessful()) {
                    progressDialog.dismiss();

                    UserResponse userResponse = response.body();
                    assert userResponse != null;
                    authViewModel.addUser(new User(
                            userResponse.getUser_id(),
                            userResponse.getName(),
                            userResponse.getPhone_number(),
                            userResponse.getFacebook_token(),
                            userResponse.getDevice_id(),
                            userResponse.isActive(),
                            userResponse.getToken()
                    ));

                    app.settings.SetIsloggedIn(true);
                    app.settings.setUserId(userResponse.getUser_id());
                    app.settings.setBearerToken(userResponse.getToken());
                    app.settings.setPhoneNumber(userResponse.getPhone_number());
                    app.settings.setName(userResponse.getName());

                    Intent dashboard = new Intent(getApplicationContext(), LoginActivity.class);
                    startActivity(dashboard);
                    finish();

                } else {
                    progressDialog.dismiss();
                    Toast.makeText(RegistrationActivity.this, "User Already Exists " + response.message(), Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(@NonNull Call<UserResponse> call, @NonNull Throwable t) {
                progressDialog.dismiss();
            }
        });
    }
}

