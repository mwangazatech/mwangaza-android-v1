package ke.mwangaza.buy.mwangaza.ui.auth.ui;

import android.app.ProgressDialog;
import android.arch.lifecycle.ViewModelProviders;
import android.content.Intent;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;
import android.text.method.PasswordTransformationMethod;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.facebook.accountkit.Account;
import com.facebook.accountkit.AccountKit;
import com.facebook.accountkit.AccountKitCallback;
import com.facebook.accountkit.AccountKitError;
import com.facebook.accountkit.PhoneNumber;

import butterknife.BindView;
import butterknife.ButterKnife;
import ke.mwangaza.buy.mwangaza.Mwangaza;
import ke.mwangaza.buy.mwangaza.R;
import ke.mwangaza.buy.mwangaza.data.model.User;
import ke.mwangaza.buy.mwangaza.data.retrofit.user.LoginUser;
import ke.mwangaza.buy.mwangaza.data.retrofit.user.UserResponse;
import ke.mwangaza.buy.mwangaza.network.APIClient;
import ke.mwangaza.buy.mwangaza.network.AuthInterface;
import ke.mwangaza.buy.mwangaza.ui.auth.viewmodel.AuthViewModel;
import ke.mwangaza.buy.mwangaza.ui.main.ui.DashboardActivity;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class LoginActivity extends AppCompatActivity {

    @BindView(R.id.buttonLogin)
    Button buttonLogin;
    @BindView(R.id.forgot_password)
    TextView forgot_password;
    @BindView(R.id.loginPhoneNumber)
    EditText loginPhoneNumber;
    @BindView(R.id.loginPin)
    EditText loginPin;

    AuthInterface authInterface;
    ProgressDialog progressDialog;
    String phone_number, pin, facebook_token;

    AuthViewModel authViewModel;
    Mwangaza app;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        ButterKnife.bind(this);
        app = (Mwangaza) getApplication();
        authInterface = APIClient.getClient(app.settings.getBearerToken()).create(AuthInterface.class);

        loginPin.setTypeface(Typeface.DEFAULT);
        loginPin.setTransformationMethod(new PasswordTransformationMethod());

        authViewModel = ViewModelProviders.of(this).get(AuthViewModel.class);

        forgot_password.setOnClickListener(v -> {
            Intent forgotPass = new Intent(getApplicationContext(), RecoveryNumber.class);
            startActivity(forgotPass);
        });

        buttonLogin.setOnClickListener(v -> verifyUserDetails());
    }

    @Override
    protected void onResume() {
        super.onResume();

        if (!app.settings.IsloggedIn()){
            app.settings.SetIsloggedIn(false);
            app.settings.setUserId(null);
            app.settings.setBearerToken(null);
            app.settings.setPhoneNumber(null);
            app.settings.setName(null);
        }

        AccountKit.getCurrentAccount(new AccountKitCallback<Account>() {
            @Override
            public void onSuccess(final Account account) {
                String accountId = account.getId();
                PhoneNumber phoneNumber = account.getPhoneNumber();
                String phoneNumberString;
                if (phoneNumber != null) {
                    phoneNumberString = phoneNumber.toString();
                    String strNew = phoneNumberString.replace("+", "");
                    loginPhoneNumber.setText(strNew);
                    loginPhoneNumber.setEnabled(false);
                    phone_number = phoneNumberString;
                    facebook_token = accountId;
                }
            }

            @Override
            public void onError(final AccountKitError error) {
                Toast.makeText(LoginActivity.this, error.toString(), Toast.LENGTH_LONG).show();
            }
        });
    }

    private void verifyUserDetails() {
        phone_number = loginPhoneNumber.getText().toString().trim();
        pin = loginPin.getText().toString().trim();

        if (TextUtils.isEmpty(pin)) {
            loginPin.setError("Enter Your Mwangaza Pin");
            return;
        }

        loginUser(phone_number, pin);
    }

    private void loginUser(String phone_number, String pin) {

        progressDialog = new ProgressDialog(this);
        progressDialog.setIndeterminate(true);
        progressDialog.setMessage("Signing In...");
        progressDialog.setCancelable(false);
        progressDialog.show();

        LoginUser loginUser = new LoginUser(phone_number, pin);

        Call<UserResponse> call = authInterface.loginUser(loginUser);
        call.enqueue(new Callback<UserResponse>() {
            @Override
            public void onResponse(@NonNull Call<UserResponse> call, @NonNull Response<UserResponse> response) {
                if (response.isSuccessful()) {
                    progressDialog.dismiss();

                    UserResponse userResponse = response.body();
                    assert userResponse != null;
                    authViewModel.addUser(new User(
                            userResponse.getUser_id(),
                            userResponse.getName(),
                            userResponse.getPhone_number(),
                            userResponse.getFacebook_token(),
                            userResponse.getDevice_id(),
                            userResponse.isActive(),
                            userResponse.getToken()
                    ));

                    app.settings.SetIsloggedIn(true);
                    app.settings.setUserId(userResponse.getUser_id());
                    app.settings.setBearerToken(userResponse.getToken());
                    app.settings.setPhoneNumber(userResponse.getPhone_number());
                    app.settings.setName(userResponse.getName());

                    Intent dashboard = new Intent(getApplicationContext(), DashboardActivity.class);
                    startActivity(dashboard);
                    finish();

                } else {
                    progressDialog.dismiss();
                    Toast.makeText(LoginActivity.this, "Failed " + response.message(), Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(@NonNull Call<UserResponse> call, @NonNull Throwable t) {
                progressDialog.dismiss();
            }
        });
    }
}
