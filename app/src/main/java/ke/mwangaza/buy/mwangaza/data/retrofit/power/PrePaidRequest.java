package ke.mwangaza.buy.mwangaza.data.retrofit.power;

public class PrePaidRequest {

    private String user_id;
    private String meter_number;

    public PrePaidRequest(String user_id, String meter_number) {
        this.user_id = user_id;
        this.meter_number = meter_number;
    }
}
