package ke.mwangaza.buy.mwangaza.ui.main.ui;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.CardView;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.TextView;

import com.facebook.accountkit.AccountKit;

import butterknife.BindView;
import butterknife.ButterKnife;
import ke.mwangaza.buy.mwangaza.Mwangaza;
import ke.mwangaza.buy.mwangaza.R;
import ke.mwangaza.buy.mwangaza.data.retrofit.common.ErrorMessage;
import ke.mwangaza.buy.mwangaza.data.retrofit.user.Firebase;
import ke.mwangaza.buy.mwangaza.data.retrofit.wallet.WalletBalance;
import ke.mwangaza.buy.mwangaza.network.APIClient;
import ke.mwangaza.buy.mwangaza.network.AuthInterface;
import ke.mwangaza.buy.mwangaza.network.WalletInterface;
import ke.mwangaza.buy.mwangaza.ui.airtime.AirtimeActivity;
import ke.mwangaza.buy.mwangaza.ui.auth.ui.UpdateUserDetailsActivity;
import ke.mwangaza.buy.mwangaza.ui.common.SplashActivity;
import ke.mwangaza.buy.mwangaza.ui.funds.PayBillActivity;
import ke.mwangaza.buy.mwangaza.ui.funds.SendMoneyActivity;
import ke.mwangaza.buy.mwangaza.ui.funds.ShareFundsActivity;
import ke.mwangaza.buy.mwangaza.ui.info.ui.ContactUsActivity;
import ke.mwangaza.buy.mwangaza.ui.info.ui.MyNumbers;
import ke.mwangaza.buy.mwangaza.ui.power.KenyaPowerActivity;
import ke.mwangaza.buy.mwangaza.ui.tv.TvInternetActivity;
import ke.mwangaza.buy.mwangaza.ui.wallet.ui.LoadWalletActivity;
import ke.mwangaza.buy.mwangaza.ui.wallet.ui.TransactionsActivity;
import ke.mwangaza.buy.mwangaza.ui.water.WaterBillsActivity;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import ru.dimorinny.floatingtextbutton.FloatingTextButton;
import uk.co.samuelwall.materialtaptargetprompt.MaterialTapTargetPrompt;

public class DashboardActivity extends AppCompatActivity {

    public static boolean isAppRunning;

    @BindView(R.id.toolbar)
    Toolbar toolbar;
    @BindView(R.id.walletbalance)
    TextView walletbalance;

    @BindView(R.id.cardViewSendMoney)
    CardView cardViewSendMoney;
    @BindView(R.id.cardViewLoadWallet)
    CardView cardViewLoadWallet;
    @BindView(R.id.cardViewShareFunds)
    CardView cardViewShareFunds;

    @BindView(R.id.cardViewKenyaPower)
    CardView cardViewKenyaPower;
    @BindView(R.id.cardViewTv)
    CardView cardViewTv;
    @BindView(R.id.cardViewWaterBills)
    CardView cardViewWaterBills;
    @BindView(R.id.cardViewAirtime)
    CardView cardViewAirtime;
    @BindView(R.id.cardViewPayBill)
    CardView cardViewPayBill;
    @BindView(R.id.cardViewTransactions)
    CardView cardViewTransactions;

    @BindView(R.id.mainfloatingActionButton)
    FloatingActionButton mainfloatingActionButton;
    @BindView(R.id.action_button)
    FloatingTextButton action_button;

    WalletInterface walletInterface;
    AuthInterface authInterface;
    Mwangaza app;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_dashboard);
        ButterKnife.bind(this);
        app = (Mwangaza) getApplication();

        toolbar.setTitle(R.string.dash_title);
        setSupportActionBar(toolbar);

        walletInterface = APIClient.getClient(app.settings.getBearerToken()).create(WalletInterface.class);
        authInterface = APIClient.getClient(app.settings.getBearerToken()).create(AuthInterface.class);

        refreshBalance();
        updateFirebase();

        mainfloatingActionButton.setOnClickListener(v -> share());

        cardViewSendMoney.setOnClickListener(v -> {
            Intent intent = new Intent(getApplicationContext(), SendMoneyActivity.class);
            startActivity(intent);
        });

        cardViewLoadWallet.setOnClickListener(v -> {
            Intent intent = new Intent(getApplicationContext(), LoadWalletActivity.class);
            startActivity(intent);
        });

        cardViewShareFunds.setOnClickListener(v -> {
            Intent intent = new Intent(getApplicationContext(), ShareFundsActivity.class);
            startActivity(intent);
        });

        cardViewKenyaPower.setOnClickListener(v -> {
            Intent intent = new Intent(getApplicationContext(), KenyaPowerActivity.class);
            startActivity(intent);
        });

        cardViewTv.setOnClickListener(v -> {
            Intent intent = new Intent(getApplicationContext(), TvInternetActivity.class);
            startActivity(intent);
        });

        cardViewWaterBills.setOnClickListener(v -> {
            Intent intent = new Intent(getApplicationContext(), WaterBillsActivity.class);
            startActivity(intent);
        });

        cardViewAirtime.setOnClickListener(v -> {
            Intent intent = new Intent(getApplicationContext(), AirtimeActivity.class);
            startActivity(intent);
        });

        cardViewPayBill.setOnClickListener(v -> {
            Intent intent = new Intent(getApplicationContext(), PayBillActivity.class);
            startActivity(intent);
        });

        cardViewTransactions.setOnClickListener(v -> {
            Intent intent = new Intent(getApplicationContext(), TransactionsActivity.class);
            startActivity(intent);
        });

        action_button.setOnClickListener(v -> {
            Intent intent = new Intent(getApplicationContext(), MyNumbers.class);
            startActivity(intent);
        });

    }

    private void share() {
        new MaterialTapTargetPrompt.Builder(DashboardActivity.this)
                .setTarget(R.id.mainfloatingActionButton)
                .setPrimaryText("Share Mwangaza")
                .setSecondaryText("Share with family and friends")
                .setPromptStateChangeListener((prompt, state) -> {
                    if (state == MaterialTapTargetPrompt.STATE_FOCAL_PRESSED) {
                        shareApp();
                    }
                })
                .show();
    }

    private void shareApp() {
        Intent sendIntent = new Intent();
        sendIntent.setAction(Intent.ACTION_SEND);
        sendIntent.putExtra(Intent.EXTRA_TEXT,
                "Download the Mwangaza app at: https://play.google.com/store/apps/details?id=ke.mwangaza.buy.mwangaza, enjoy ZERO transaction fees on airtime and Kenya Power and the best paybill pricing in town");
        sendIntent.setType("text/plain");
        startActivity(sendIntent);
    }

    private void updateFirebase() {

        Firebase firebase = new Firebase(app.settings.getFirebaseToken());

        Call<ErrorMessage> call = authInterface.updateFCMToken(firebase);
        call.enqueue(new Callback<ErrorMessage>() {
            @Override
            public void onResponse(@NonNull Call<ErrorMessage> call, @NonNull Response<ErrorMessage> response) {

            }

            @Override
            public void onFailure(@NonNull Call<ErrorMessage> call, @NonNull Throwable t) {

            }
        });
    }

    private void refreshBalance() {
        Call<WalletBalance> call = walletInterface.getWalletBalance();
        call.enqueue(new Callback<WalletBalance>() {
            @Override
            public void onResponse(@NonNull Call<WalletBalance> call, @NonNull Response<WalletBalance> response) {
                if (response.isSuccessful()) {
                    WalletBalance walletBalance = response.body();
                    assert walletBalance != null;

                    walletbalance.setText(String.format("KES %s", walletBalance.getBalance()));
                }
            }

            @Override
            public void onFailure(@NonNull Call<WalletBalance> call, @NonNull Throwable t) {

            }
        });
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu, menu);
        return true;
    }

    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.details:
                startActivity(new Intent(this, UpdateUserDetailsActivity.class));
                return true;
            case R.id.contactus:
                startActivity(new Intent(this, ContactUsActivity.class));
                return true;
            case R.id.logout:
                AccountKit.logOut();
                app.settings.SetIsloggedIn(false);
                app.settings.setUserId(null);
                app.settings.setBearerToken(null);
                app.settings.setPhoneNumber(null);
                app.settings.setName(null);
                launchLoginActivity();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    private void launchLoginActivity() {
        Intent intent = new Intent(this, SplashActivity.class);
        startActivity(intent);
        finish();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        isAppRunning = false;
    }

}

