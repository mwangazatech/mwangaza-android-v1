package ke.mwangaza.buy.mwangaza.network;

import java.util.List;

import ke.mwangaza.buy.mwangaza.data.retrofit.water.PayWaterRequest;
import ke.mwangaza.buy.mwangaza.data.retrofit.water.WaterDetails;
import ke.mwangaza.buy.mwangaza.data.retrofit.water.WaterMeters;
import ke.mwangaza.buy.mwangaza.data.retrofit.water.WaterPaymentDetails;
import ke.mwangaza.buy.mwangaza.data.retrofit.water.WaterRequest;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.POST;

public interface WaterInterface {

    @POST("water/nairobi/fetch")
    Call<WaterDetails> fetchNairobiWaterBill(@Body WaterRequest prePaidRequest);

    @POST("water/kisumu/fetch")
    Call<WaterDetails> fetchKisumuWaterBill(@Body WaterRequest prePaidRequest);

    @POST("water/bill/pay")
    Call<WaterPaymentDetails> payWaterBill(@Body PayWaterRequest payWaterRequest);

    @GET("config/water/meters")
    Call<List<WaterMeters>> getRecentWaterMeters();
}
