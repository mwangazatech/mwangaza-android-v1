package ke.mwangaza.buy.mwangaza.data.retrofit.water;

public class WaterMeters {

    private String account_number;
    private String customer_name;

    public WaterMeters(String account_number, String customer_name) {
        this.account_number = account_number;
        this.customer_name = customer_name;
    }

    public String getAccount_number() {
        return account_number;
    }

    public void setAccount_number(String account_number) {
        this.account_number = account_number;
    }

    public String getCustomer_name() {
        return customer_name;
    }

    public void setCustomer_name(String customer_name) {
        this.customer_name = customer_name;
    }
}