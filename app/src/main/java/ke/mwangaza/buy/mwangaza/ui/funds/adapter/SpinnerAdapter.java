package ke.mwangaza.buy.mwangaza.ui.funds.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import java.util.List;

import ke.mwangaza.buy.mwangaza.R;
import ke.mwangaza.buy.mwangaza.data.retrofit.wallet.Banks;

public class SpinnerAdapter extends BaseAdapter {
    private LayoutInflater layoutInflater;
    private List<Banks> listData;
    private Context context;

    public SpinnerAdapter(Context context, List<Banks> listData) {
        this.context = context;
        layoutInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        this.listData = listData;
    }

    @Override
    public int getCount() {
        return listData.size();
    }

    @Override
    public Object getItem(int position) {
        return listData.get(position);
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder spinnerHolder;
        if (convertView == null) {
            spinnerHolder = new ViewHolder();
            convertView = layoutInflater.inflate(R.layout.spinner_list, parent, false);
            spinnerHolder.spinnerItemList = convertView.findViewById(R.id.spinner_list_item);
            convertView.setTag(spinnerHolder);
        } else {
            spinnerHolder = (ViewHolder) convertView.getTag();
        }
        spinnerHolder.spinnerItemList.setText(listData.get(position).getBank_name());
        return convertView;
    }

    class ViewHolder {
        TextView spinnerItemList;
    }
}