package ke.mwangaza.buy.mwangaza.ui.water;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.widget.Button;
import android.widget.EditText;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;

import butterknife.BindView;
import butterknife.ButterKnife;
import ke.mwangaza.buy.mwangaza.Mwangaza;
import ke.mwangaza.buy.mwangaza.R;
import ke.mwangaza.buy.mwangaza.data.retrofit.water.WaterDetails;
import ke.mwangaza.buy.mwangaza.data.retrofit.water.WaterRequest;
import ke.mwangaza.buy.mwangaza.network.APIClient;
import ke.mwangaza.buy.mwangaza.network.WaterInterface;
import ke.mwangaza.buy.mwangaza.ui.main.ui.DashboardActivity;
import ke.mwangaza.buy.mwangaza.ui.power.PrepaidFailureActivity;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class KisumuMeterActivity extends AppCompatActivity {

    @BindView(R.id.toolbar)
    Toolbar toolbar;
    @BindView(R.id.cancel)
    Button cancel;

    @BindView(R.id.editTextMeterNumber)
    EditText editTextMeterNumber;
    @BindView(R.id.verifymeter)
    Button verifymeter;

    Mwangaza app;
    WaterInterface waterInterface;
    ProgressDialog progressDialog;

    String meter_number, user_id, fetched_meter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_kisumu_meter);
        ButterKnife.bind(this);

        toolbar.setTitle(R.string.kisumu_pay);
        setSupportActionBar(toolbar);
        if(getSupportActionBar() != null){
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setDisplayShowHomeEnabled(true);
        }


        app = (Mwangaza) getApplicationContext();
        waterInterface = APIClient.getClient(app.settings.getBearerToken()).create(WaterInterface.class);

        Intent intent = getIntent();
        Bundle bundle = intent.getExtras();

        if (bundle != null) {
            fetched_meter = bundle.getString("meter_number");
            editTextMeterNumber.setText(fetched_meter);
        }

        cancel.setOnClickListener(v -> {
            Intent cancel = new Intent(getApplicationContext(), DashboardActivity.class);
            startActivity(cancel);
        });

        verifymeter.setOnClickListener(v -> verifyDetails());
    }

    private void verifyDetails() {

        meter_number = editTextMeterNumber.getText().toString().trim();
        user_id = app.settings.getUserId();

        if (TextUtils.isEmpty(meter_number)) {
            editTextMeterNumber.setError("Kindly Enter Your Meter Number");
            return;
        }

        confirmMeterDetails();
    }

    private void confirmMeterDetails() {

        progressDialog = new ProgressDialog(this);
        progressDialog.setIndeterminate(true);
        progressDialog.setMessage("Verifying Meter Number...");
        progressDialog.setCancelable(false);
        progressDialog.show();

        WaterRequest waterRequest = new WaterRequest(user_id, meter_number);

        Call<WaterDetails> call = waterInterface.fetchKisumuWaterBill(waterRequest);
        call.enqueue(new Callback<WaterDetails>() {
            @Override
            public void onResponse(Call<WaterDetails> call, Response<WaterDetails> response) {
                if (response.isSuccessful()) {
                    progressDialog.dismiss();

                    WaterDetails waterDetails = response.body();
                    assert waterDetails != null;

                    Intent intent = new Intent(getApplicationContext(), KisumuAmountActivity.class);
                    Bundle bundle = new Bundle();
                    bundle.putString("water_id", waterDetails.getWater_id());
                    bundle.putString("user_id", waterDetails.getUser_id());
                    bundle.putString("account_number", waterDetails.getAccount_number());
                    bundle.putString("customer_name", waterDetails.getCustomer_name());
                    bundle.putString("amount", waterDetails.getAmount());
                    bundle.putString("due_date", waterDetails.getDue_date());
                    intent.putExtras(bundle);
                    startActivity(intent);

                } else {
                    progressDialog.dismiss();
                    assert response.body() != null;
                    JSONObject jsonObject = null;
                    try {
                        assert response.errorBody() != null;
                        jsonObject = new JSONObject(response.errorBody().string());

                        Intent intent = new Intent(getApplicationContext(), KisumuFailActivity.class);
                        Bundle bundle = new Bundle();
                        bundle.putString("message", jsonObject.getString("message"));
                        intent.putExtras(bundle);
                        startActivity(intent);

                    } catch (JSONException e) {
                        e.printStackTrace();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
            }

            @Override
            public void onFailure(Call<WaterDetails> call, Throwable t) {
                progressDialog.dismiss();
                Intent intent = new Intent(getApplicationContext(), PrepaidFailureActivity.class);
                Bundle bundle = new Bundle();
                bundle.putString("message", "Please try your request again...");
                intent.putExtras(intent);
                startActivity(intent);
            }
        });
    }

    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }
}
