package ke.mwangaza.buy.mwangaza.data.retrofit.wallet;

public class WalletBalance {

    private String user_id;
    private String balance;

    public WalletBalance(String user_id, String balance) {
        this.user_id = user_id;
        this.balance = balance;
    }

    public String getUser_id() {
        return user_id;
    }

    public void setUser_id(String user_id) {
        this.user_id = user_id;
    }

    public String getBalance() {
        return balance;
    }

    public void setBalance(String balance) {
        this.balance = balance;
    }
}